import { ModalProvider } from '../../src/context/ModalContext';
import Modal from '../../src/components/Modal/Modal';
import { Decorator } from '@storybook/react';

const ModalContextDecorator: Decorator = (Story) => {
  return (
    <ModalProvider>
      <div
        style={{
          width: '100%',
          height: '100vh',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          background: '#757575',
        }}
      >
        <Story />
        <Modal />
      </div>
    </ModalProvider>
  );
};

export default ModalContextDecorator;
