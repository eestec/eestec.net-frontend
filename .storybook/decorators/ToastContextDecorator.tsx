import { Decorator } from '@storybook/react';

import ToastContext, { Toast } from '../../src/context/ToastContext';

const ToastContextDecorator: Decorator = (Story, context) => {
  return (
    <ToastContext.Provider
      value={{
        toast: {
          ...(context.args as unknown as Toast),
        },
        setToast: () => {},
        setToastContent: () => {},
      }}
    >
      <Story />
    </ToastContext.Provider>
  );
};

export default ToastContextDecorator;
