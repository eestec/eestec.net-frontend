import { create } from '@storybook/theming';
import logoEESTEC from '../../src/assets/img/eestec-logo_white.svg';

export default create({
  // Base info
  base: 'dark',
  brandTitle: 'EESTECnet',
  brandUrl: 'https://eestec.net',
  brandImage: logoEESTEC,
  brandTarget: '_self',

  // Colors
  colorPrimary: '#e52a30',
  colorSecondary: '#e52a30',
  textColor: '#fafafa',
  barTextColor: '#fafafa',
  barSelectedColor: '#e52a30',

  // Typography
  fontBase: 'Roboto, "Open Sans", sans-serif',
  fontCode: 'Consolas, "Courier New", monospace',
});
