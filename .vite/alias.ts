import * as path from 'path';

export default {
  '~': path.resolve(__dirname, '..', 'src'),
  api: path.resolve(__dirname, '..', 'src', 'api'),
  assets: path.resolve(__dirname, '..', 'src', 'assets'),
  components: path.resolve(__dirname, '..', 'src', 'components'),
  context: path.resolve(__dirname, '..', 'src', 'context'),
  hooks: path.resolve(__dirname, '..', 'src', 'hooks'),
  layouts: path.resolve(__dirname, '..', 'src', 'layouts'),
  routes: path.resolve(__dirname, '..', 'src', 'routes'),
  utils: path.resolve(__dirname, '..', 'src', 'utils'),
  views: path.resolve(__dirname, '..', 'src', 'views'),
};
