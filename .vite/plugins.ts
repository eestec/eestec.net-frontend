import react from '@vitejs/plugin-react';
import { checker } from 'vite-plugin-checker';

export default [
  checker({
    typescript: true,
    eslint: {
      lintCommand: 'eslint --ext .cjs,.mjs,.js,.jsx,ts,tsx .',
    },
  }),
  react(),
];
