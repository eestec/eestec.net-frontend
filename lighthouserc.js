/*eslint-env node*/
module.exports = {
  ci: {
    collect: {
      settings: {
        chromeFlags: '--no-sandbox --disable-dev-shm-usage --headless',
      },
    },
    upload: {
      target: 'filesystem',
      outputDir: './lhci',
    },
  },
};
