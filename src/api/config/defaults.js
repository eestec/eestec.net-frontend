import axios from 'axios';
import { stringify } from 'qs';
import { envOrDefault } from 'utils/misc';

// Back-end url
const BASE_URL = envOrDefault('VITE_BASE_API_URL', 'http://localhost:8000');

// Defaults required for Laravel auth
axios.defaults.withCredentials = true;
axios.defaults.paramsSerializer = (params) =>
  stringify(params, { arrayFormat: 'brackets' });

const apiClient = axios.create({
  baseURL: `${BASE_URL}/api/`,
});

const authClient = axios.create({
  baseURL: `${BASE_URL}/auth/`,
});

export { BASE_URL, apiClient, authClient };
