export function processParameters(params, { sortingMap } = {}) {
  if (!params) {
    return undefined;
  }
  const { page, filters, sorting } = params;

  return {
    page: page != null ? page + 1 : undefined,
    filter: processFilters(filters),
    sort: processSorting(sorting, sortingMap),
  };
}

function processFilters(filters) {
  if (!filters) {
    return undefined;
  }

  const { quickFilterValues, items } = filters;
  const fieldFilter = items.at(0);
  const search = quickFilterValues.join(' ');

  if (!search && !fieldFilter) {
    return undefined;
  }

  const result = {};

  if (search) {
    result.search = search;
  }

  if (fieldFilter) {
    const { field, value, operator } = fieldFilter;
    result[field] = {
      type: operator ? operatorMap[operator] ?? operator : '',
      value,
    };
  }

  return result;
}

function processSorting(sorting, sortingMap) {
  const _sorting = sorting?.at(0);
  if (!_sorting) {
    return undefined;
  }

  const { field: sortField, sort: sortType } = _sorting;
  let sort = sortField ? sortingMap[sortField] || sortField : undefined;
  if (sort && sortType === 'desc') {
    sort = `-${sort}`;
  }

  return sort;
}

const operatorMap = {
  not: 'isNot',
  after: 'isAfter',
  onOrAfter: 'isOnOrAfter',
  before: 'isBefore',
  onOrBefore: 'isOnOrBefore',
};
