import { apiRequest } from 'api/config/request';

export class AdminService {
  static getAdminData() {
    return apiRequest({
      url: '/admin',
      method: 'GET',
    });
  }
}
