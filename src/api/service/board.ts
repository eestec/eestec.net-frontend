import { apiRequest } from 'api/config/request';
import type { BoardMember, BoardPosition, Mandate } from 'utils/types';

export default class BoardService {
  static async getBoard(
    params: { mandate?: Mandate },
    abortSignal?: AbortSignal,
  ) {
    const response = await apiRequest({
      url: '/board',
      method: 'GET',
      signal: abortSignal,
      params,
    });

    return response.data as BoardMember[];
  }

  static async addBoardMember(
    {
      mandate,
      boardPositionId,
      order,
      userId,
    }: {
      userId: number;
      mandate: Mandate;
      boardPositionId: number;
      order?: number;
    },
    abortSignal?: AbortSignal,
  ) {
    const response = await apiRequest({
      url: '/board',
      method: 'POST',
      signal: abortSignal,
      data: {
        mandate,
        board_position_id: boardPositionId,
        order: order ?? boardPositionId,
        user_id: userId,
      },
    });

    return response.data as BoardPosition[];
  }

  static async updateBoardMember(
    boardMemberId: number,
    data: { userId?: number; order?: number; boardPositionId?: number },
    abortSignal?: AbortSignal,
  ) {
    const response = await apiRequest({
      url: `/board/${boardMemberId}`,
      method: 'POST',
      signal: abortSignal,
      data: {
        user_id: data.userId,
        order: data.order,
        board_position_id: data.boardPositionId,
        _method: 'PUT',
      },
    });

    return response.data as BoardPosition[];
  }

  static async removeBoardMember(boardMemberId: number) {
    return apiRequest({
      url: `/board/${boardMemberId}`,
      method: 'DELETE',
    });
  }

  static async getBoardPositions(abortSignal?: AbortSignal) {
    const response = await apiRequest({
      url: '/board-positions',
      method: 'GET',
      signal: abortSignal,
    });

    return response.data as BoardPosition[];
  }

  static async updateBoardPosition(
    positionId: number,
    data: { description: string },
    abortSignal?: AbortSignal,
  ) {
    const response = await apiRequest({
      url: `/board-positions/${positionId}`,
      method: 'POST',
      signal: abortSignal,
      data: {
        ...data,
        _method: 'PUT',
      },
    });

    return response.data as BoardPosition[];
  }
}
