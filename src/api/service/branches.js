import EventService from 'api/service/events';
import { apiRequest } from 'api/config/request';
import { getFormData } from 'utils/api';
import { removeNbsp } from 'utils/misc';
import { isBranchTerminated } from 'utils/branches';
import { processParameters } from './_filterHelpers';

export default class BranchService {
  static async getBranches(params, abortSignal) {
    const _params = {
      ...processParameters(params, { sortingMap }),
      all: params?.all,
    };

    const response = await apiRequest({
      url: '/branches',
      method: 'GET',
      signal: abortSignal,
      params: _params,
    });

    if (_params?.all) {
      return mapBranchesList(response.data);
    }

    return {
      ...response,
      data: mapBranchesList(response?.data),
    };
  }

  static async getBranch(slug, abortSignal) {
    const response = await apiRequest({
      url: `/branches/${slug}`,
      method: 'GET',
      signal: abortSignal,
    });

    return {
      ...response,
      branch: {
        ...response.branch,
        description: removeNbsp(response.branch.description),
        isTerminated: isBranchTerminated(response.branch.branch_type),
      },
    };
  }

  static getBranchEvents(slug, abortSignal, options = {}) {
    if (options.customGetEventsLink) {
      return EventService.getEvents(abortSignal, options);
    }
    return apiRequest({
      url: `/branches/${slug}/events`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static editData(slug, data, { presentImageKeys = [] } = {}) {
    const { profile_picture_path, ...requestData } = data;
    presentImageKeys.forEach((key) => {
      requestData[key] = data[key];
    });

    return apiRequest({
      url: `/branches/${slug}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData({
        ...requestData,
        _method: 'PUT',
      }),
    });
  }

  static changeBranchType(slug, branchTypeName) {
    return apiRequest({
      url: `/branches/${slug}/type`,
      method: 'POST',
      data: {
        branch_type: branchTypeName,
      },
    });
  }

  static manageMembership(branchSlug, applicationSlug, acceptance) {
    return apiRequest({
      url: `/branches/${branchSlug}/applications/${applicationSlug}/approval`,
      method: 'POST',
      data: {
        acceptance,
      },
    });
  }

  static manageMemberships(branchSlug, applications, acceptance) {
    return apiRequest({
      url: `/branches/${branchSlug}/applications/approval`,
      method: 'POST',
      data: {
        acceptance,
        applications,
      },
    });
  }

  static removeMember(branchSlug, userSlug) {
    return apiRequest({
      url: `/branches/${branchSlug}/remove/${userSlug}`,
      method: 'DELETE',
    });
  }

  static removeMembers(branchSlug, userSlugs) {
    return apiRequest({
      url: `/branches/${branchSlug}/remove`,
      method: 'DELETE',
      params: {
        slugs: userSlugs.join(','),
      },
    });
  }

  static apply(branchSlug) {
    return apiRequest({
      url: `/branches/${branchSlug}/apply`,
      method: 'POST',
    });
  }

  static changeMemberRole(branchSlug, userSlug, newRole) {
    return apiRequest({
      url: `/branches/${branchSlug}/change-role/${userSlug}`,
      method: 'POST',
      data: {
        branch_role_id: newRole,
      },
    });
  }

  static changeMembersRoles(branchSlug, slugs, newRole) {
    return apiRequest({
      url: `/branches/${branchSlug}/change-role`,
      method: 'POST',
      data: {
        users: slugs,
        branch_role_id: newRole,
      },
    });
  }

  static addBranch(data) {
    const _data = {
      ...data,
      region: parseInt(data.region, 10),
      branch_type_id: 1,
    };

    return apiRequest({
      url: '/branches',
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData(_data),
    });
  }
}

const mapBranchesList = (branches) =>
  branches?.map(
    ({ name, slug, profile_picture_path, branch_type, country, region }) => ({
      name,
      slug,
      backgroundImage: profile_picture_path,
      country,
      branch_type,
      region,
    }),
  );

const sortingMap = {
  branch_type: 'branch_type_id',
};
