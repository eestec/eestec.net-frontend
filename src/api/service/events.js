/* eslint-disable camelcase,prefer-const */

import { apiRequest, request } from 'api/config/request';
import axios from 'axios';
import { getFormData } from 'utils/api';
import compareAsc from 'date-fns/compareAsc';

export default class EventService {
  static async getEventsNew({
    pageParam,
    filters,
    kind,
    entityKey,
    entityValue,
  }) {
    const searchParams = new URLSearchParams(filters);
    searchParams.set('kind', kind);

    let data;

    if (pageParam) {
      data = await request(axios, {
        url: `${pageParam}&${searchParams}`,
        method: 'GET',
      });
    } else {
      const urlBase = entityKeyMapper[entityKey]?.(entityValue) ?? '/events';

      data = await apiRequest({
        url: `${urlBase}?${searchParams}`,
        method: 'GET',
      });
    }

    if (kind !== 'active') {
      return data;
    }

    const today = new Date();
    data.data = data.data.reduce(
      (acc, eventInstance) => {
        const startDate = new Date(eventInstance.start_date);
        const applicationDeadline = new Date(
          eventInstance.application_deadline,
        );
        // Add to in progress events if an active event start date is older than the current date
        if (compareAsc(startDate, today) < 1) {
          acc.inProgress.push(eventInstance);
          return acc;
        }
        // Add to upcoming if an active event's application deadline is older than the current date
        if (compareAsc(applicationDeadline, today) < 0) {
          acc.upcoming.push(eventInstance);
          return acc;
        }
        // Otherwise add to open for applications
        acc.applications.push(eventInstance);

        return acc;
      },
      {
        inProgress: [],
        upcoming: [],
        applications: [],
      },
    );

    return data;
  }

  static getEvents(abortSignal, { customGetEventsLink, filters, kind }) {
    const searchParams = new URLSearchParams();
    if (filters) {
      searchParams.set('filters', filters);
    }
    if (kind) {
      searchParams.set('kind', kind);
    }

    const searchParamsString = searchParams.toString();

    if (customGetEventsLink) {
      const includesSearchParams = customGetEventsLink.includes('?');
      const concatChar = includesSearchParams ? '&' : '?';
      const customSearchParams = searchParamsString
        ? `${concatChar}${searchParamsString}`
        : '';
      return request(axios, {
        url: `${customGetEventsLink}${customSearchParams}`,
        method: 'GET',
        signal: abortSignal,
      });
    }

    return apiRequest({
      url: `/events?${searchParamsString}`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static getEvent(slug) {
    return apiRequest({
      url: `/events/${slug}`,
      method: 'GET',
    });
  }

  static editData(slug, data, { presentImageKeys = [] } = {}) {
    const { profile_picture, banner_path, ...requestData } = data;
    presentImageKeys.forEach((key) => {
      requestData[key] = data[key];
    });
    return apiRequest({
      url: `/events/${slug}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData({
        ...requestData,
        _method: 'PUT',
      }),
    });
  }

  static getEventTypes() {
    return apiRequest({
      url: '/events/types',
      method: 'GET',
    });
  }
}

const entityKeyMapper = {
  branch: (val) => `/branches/${val}/events`,
  userAttended: (val) => `/users/${val}/events-attended`,
  userOrganized: (val) => `/users/${val}/events-organized`,
};
