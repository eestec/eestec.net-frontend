import { apiRequest } from 'api/config/request';
import { processParameters } from './_filterHelpers';
import type {
  EntityParams,
  ApiQueryParams,
  BranchMemberKind,
  PaginatedResponse,
  BranchMember,
  BranchMemberApplication,
  User,
  DetailedUser,
} from 'utils/types';

const entityMap = {
  branchMembers: (branch: string) => `/branches/${branch}/members`,
  branchApplications: (branch: string) => `/branches/${branch}/applications`,
  eventParticipants: (event: string) => `/events/${event}/participants`,
  eventOrganizers: (event: string) => `/events/${event}/organizers`,
  eventHelpers: (event: string) => `/events/${event}/helpers`,
  all: () => '/users',
};

const getBoardEnpoint = (branch: string) => `/branches/${branch}/board`;

export default class MemberService {
  static getMembers<K extends MemberEntityParams['key']>(
    {
      key: entityKey,
      value: entityValue,
    }: {
      key: K;
      value?: MemberEntityParams['value'];
    },
    requestParams?: ApiQueryParams<{ kind?: BranchMemberKind }>,
    abortSignal?: AbortSignal,
  ) {
    let endpoint = entityMap[entityKey](entityValue!);
    let kind = requestParams?.kind;
    if (entityKey === 'branchMembers' && kind === 'board') {
      endpoint = getBoardEnpoint(entityValue!);
      kind = undefined;
    }

    const params = {
      ...processParameters(requestParams, { sortingMap: {} }),
      kind,
    };

    return apiRequest({
      url: endpoint,
      method: 'GET',
      signal: abortSignal,
      params,
    }) as Promise<PaginatedResponse<MemberType<K>[]>>;
  }
}

export type MemberEntityParams = EntityParams<keyof typeof entityMap>;

type EntityTypeMap = {
  branchMembers: BranchMember;
  branchApplications: BranchMemberApplication;
  eventParticipants: User;
  eventOrganizers: User;
  eventHelpers: User;
  all: DetailedUser;
};

// Define the MemberType using the EntityTypeMap
type MemberType<K extends keyof EntityTypeMap> = EntityTypeMap[K];
