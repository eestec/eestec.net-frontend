import { apiRequest } from 'api/config/request';

export default class PartnerService {
  static getPartners() {
    return apiRequest({
      url: '/partners',
      method: 'GET',
    });
  }
}
