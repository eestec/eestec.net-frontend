/* eslint-disable camelcase,prefer-const */

import { apiRequest } from 'api/config/request';
import { getFormData } from 'utils/api';
import EventService from 'api/service/events';

export default class UserService {
  static getUserData(abortSignal = null) {
    return apiRequest({
      url: '/user',
      method: 'GET',
      signal: abortSignal,
    });
  }

  static getUserProfile(slug, abortSignal) {
    return apiRequest({
      url: `/users/${slug}`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static getUserAttendedEvents(slug, abortSignal, options = {}) {
    if (options.customGetEventsLink) {
      return EventService.getEvents(abortSignal, options);
    }
    return apiRequest({
      url: `/users/${slug}/events-attended`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static getUserOrganizedEvents(slug, abortSignal, options = {}) {
    if (options.customGetEventsLink) {
      return EventService.getEvents(abortSignal, options);
    }
    return apiRequest({
      url: `/users/${slug}/events-organized`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static editData(slug, data, { presentImageKeys = [] } = {}) {
    const { profile_photo_path, banner_path, ...requestData } = data;
    presentImageKeys.forEach((key) => {
      requestData[key] = data[key];
    });
    return apiRequest({
      url: `/users/${slug}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData({
        ...requestData,
        _method: 'PUT',
      }),
    });
  }
}
