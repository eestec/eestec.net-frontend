/* eslint-disable no-alert */
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import SplitButtonComponent from 'components/SplitButton/SplitButton';
import LinkButtonComponent from 'components/LinkButton/LinkButton';
import Button from './Button';
import { splitButtonMockedProps } from '../../../.storybook/helpers';

export default {
  title: 'Components/Button',
  component: Button,
  tags: ['autodocs'],
  subcomponents: {
    SplitButton: SplitButtonComponent,
    LinkButton: LinkButtonComponent,
  },
  argTypes: {
    icon: {
      control: false,
    },
  },
};

export const Primary = {
  args: {
    className: 'primary-button',
    onClick: () => alert('I am a primary button!'),
    children: 'Button',
  },
};

export const Secondary = {
  args: {
    className: 'secondary-button',
    onClick: () => alert('I am a secondary button!'),
    children: 'Button',
  },
};

export const WithIcon = {
  args: {
    className: 'primary-button',
    onClick: () => alert('I am a button with an icon!'),
    children: 'Button',
    icon: ArrowForwardIosIcon,
  },
};

export const Inverted = {
  args: {
    className: 'inverted',
    onClick: () => alert('I am an inverted button!'),
    children: 'Button',
  },
};

export const Round = {
  args: {
    className: 'primary-button round',
    onClick: () => alert('I am a round button!'),
    children: 'Button',
  },
};

function SplitButtonRender(args) {
  return (
    <div style={{ minHeight: '150px' }}>
      <SplitButtonComponent {...args} />
    </div>
  );
}

export const SplitButton = {
  render: SplitButtonRender,
  args: splitButtonMockedProps.props,
  paramseters: {
    controls: {
      exclude: /.*/g,
    },
  },
};

export const LinkButton = {
  render: (args) => <LinkButtonComponent {...args} />,
  args: {
    onClick: () =>
      alert('Look! You clicked a link that got executed like a button!'),
    children: 'I may look like a link but I am actually a button!',
    title: 'ButtonLink',
  },
  parameters: {
    controls: {
      exclude: /.*/g,
    },
  },
};
