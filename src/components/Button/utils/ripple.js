export function createRipple(event) {
  const button = event.currentTarget;
  const rect = button.getBoundingClientRect();

  const circle = document.createElement('span');
  circle.classList.add('ripple');
  const diameter = Math.max(button.clientWidth, button.clientHeight);
  const radius = diameter / 2;

  circle.style.width = `${diameter}px`;
  circle.style.height = `${diameter}px`;
  circle.style.left = `${event.clientX - rect.left - radius}px`;
  circle.style.top = `${event.clientY - rect.top - radius}px`;

  button.appendChild(circle);
  setTimeout(() => {
    circle.remove();
  }, 400);
}
