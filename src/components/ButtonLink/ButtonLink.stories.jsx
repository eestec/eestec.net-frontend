import { withRouter } from 'storybook-addon-react-router-v6';
import ButtonLink from './ButtonLink';

export default {
  title: 'Components/Link variants/ButtonLink',
  component: ButtonLink,
  tags: ['autodocs'],
  decorators: [withRouter],
  argTypes: {
    icon: {
      control: false,
    },
    to: {
      control: false,
    },
  },
};

export const Default = {
  args: {
    to: '/?path=/story/components-linkvariants-buttonlink',
    children: 'Button link',
    className: 'primary-button',
  },
};
