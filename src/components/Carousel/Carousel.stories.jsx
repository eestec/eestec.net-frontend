import { Carousel } from 'components/Carousel/Carousel';
import CarouselButtonBase from 'components/Carousel/components/CarouselButtonBase';
import { generateExampleItems } from '../../../.storybook/helpers';

export default {
  title: 'Components/Carousel',
  component: Carousel,
  tags: ['autodocs'],
  subcomponents: { CarouselButtonBase },
  argTypes: {
    children: { control: false },
  },
  parameters: {
    layout: 'fullscreen',
  },
  args: {
    nativeSnapping: false,
  },
};

export const Default = {
  args: {
    children: generateExampleItems(10, 'carousel-item'),
  },

  argTypes: {
    arrows: { control: false },
  },
};

export const WithoutAutoscroll = {
  args: {
    ...Default.args,
    autoscroll: false,
  },

  argTypes: {
    autoscroll: { control: false },
  },
};

export const WithArrows = {
  args: {
    ...WithoutAutoscroll.args,
    arrows: true,
  },

  argTypes: {
    autoscroll: { control: false },
  },
};
