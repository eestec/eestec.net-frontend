import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { animateDraggable } from 'utils/scroll/animateScroll';
import Draggable from 'components/Draggable/Draggable';
import Div from 'utils/componentsWrappers/Div';
import { SvgIcon, IconButton, CircularProgress } from '@mui/material';
import { ArrowForwardIos, ArrowBackIosNew } from '@mui/icons-material';

import './Carousel.css';

const SpacingClasses = {
  LARGE: 'spacing-large',
  NONE: 'spacing-none',
  MEDIUM: 'spacing-medium',
  SMALL: 'spacing-small',
};

type ArrowProps = {
  Icon: typeof SvgIcon;
  onClick: React.MouseEventHandler;
  ariaLabel: string;
  className: string;
};

function Arrow({
  Icon,
  onClick,
  ariaLabel = 'Carousel Arrow',
  className = '',
}: ArrowProps) {
  let classes = 'carousel-arrow';
  classes += className ? ` ${className}` : '';
  return (
    <IconButton className={classes} aria-label={ariaLabel} onClick={onClick}>
      <Icon />
    </IconButton>
  );
}

type ClickableArrowProps = {
  onClick: React.MouseEventHandler;
};

function NextArrow({ onClick }: ClickableArrowProps) {
  return (
    <Arrow
      ariaLabel="next slide"
      onClick={onClick}
      Icon={ArrowForwardIos}
      className="next"
    />
  );
}

function PrevArrow({ onClick }: ClickableArrowProps) {
  return (
    <Arrow
      ariaLabel="previous slide"
      onClick={onClick}
      Icon={ArrowBackIosNew}
      className="prev"
    />
  );
}

type CarouselProps = {
  children: Array<React.ReactNode>;
  /**
   The spacing between the elements of the carousel.
  */
  spacing?: keyof typeof SpacingClasses;
  /**
   A boolean value that indicates whether the snapping should be handled by the
   underlying container for `true` (e.g. [`Draggable`](./?path=/docs/components-draggable)
   or by carousel's scrolling behaviours `false`.

   __NOTE:__ This value can be considered `false` if `autoscroll` is set to `true`.
  */
  nativeSnapping: boolean;
  /**
   A boolean value that indicates whether the carousel should be scrolling automatically.
  */
  autoscroll: boolean;
  /**
   The duration of a single automatic scroll if `autoscroll` is set to `true`. _(unit: ms)_
  */
  scrollDuration: number;
  /**
   The delay between a single automatic scroll if `autoscroll` is set to `true`. _(unit: ms)_
  */
  scrollDelay: number;
  /**
   A boolean value that indicates whether the carousel should be draggable with a mouse.
  */
  draggable: boolean;
  containerClassName: string;
  /**
   A boolean value that indicates whether the carousel should be scrollable by clicking arrows.
  */
  arrows: boolean;
};

/**
 __Carousel__ serves as a slider for the elements that can but do not need to be:
 - draggable,
 - automatically scrolled within a given interval,
 - scrollable by clicking on arrows.

 All the options are customizable. Under the hood `Carousel` implements
 ['Draggable'](./?path=/docs/components-draggable) and consists of a bunch of pure JS
 functions that handle the interval of scrolling.

 __NOTE:__ If you want for the items of the carousel to be clickable and used like buttons
 wrap them in `CarouselButtonBase`.

 __NOTE 2:__ Always import this element using the default-exported `CarouselWrapper`,
 not by importing `Carousel` directly. Cases of loading where there are no items yet
 are handled well by the `CarouselWrapper`.
 */
export function Carousel({
  children,
  spacing = 'LARGE',
  nativeSnapping = true,
  autoscroll = true,
  scrollDuration = 2500,
  scrollDelay = 1000,
  draggable = true,
  containerClassName = '',
  arrows = false,
}: CarouselProps) {
  // Use refs instead of normal React useState as the calculations have to be synchronous
  const scrollInterval = useRef<NodeJS.Timeout>();
  const containerRef = useRef<HTMLDivElement>(null);
  const timeoutRef = useRef<NodeJS.Timeout>();
  const infiniteTimeoutRef = useRef<NodeJS.Timeout>();
  const arrowReadinessTimeout = useRef<NodeJS.Timeout>();
  const mouseDownStateRef = useRef<boolean>(false);

  const [isArrowReady, setIsArrowReady] = useState(true);

  // Imitate normal setState behavior function for the ref
  const setIsMouseDown = useCallback(
    (newState: boolean) => {
      mouseDownStateRef.current = newState;
    },
    [mouseDownStateRef],
  );

  /*
    addInfiniteBlock handles the impression if infinity of the carousel elements
    (once the carousel is over, the first elements appears as the last one and so on).

    __NOTE:__ This approach with cloning children two times and just moving
    scrollLeft by the whole containerLength is not expensive it terms of calculations
    but it bloats the DOM. Maybe there is a better way to implement the infinite scrolling.
  */
  const addInfiniteBlock = useCallback(() => {
    if (containerRef.current === null) return;
    const { scrollLeft } = containerRef.current;
    // all the children have the same width in the Carousel
    const childrenWidth = (containerRef.current.children[0] as HTMLElement)
      .offsetWidth;
    const containerLength = children.length * childrenWidth;

    if (scrollLeft < containerLength) {
      // scroll the container to the same child but in the "right length" of the container
      containerRef.current.scrollLeft += containerLength;
    } else if (scrollLeft >= containerLength * 2) {
      // scroll the container to the same child but in the "left" length of the container
      containerRef.current.scrollLeft -= containerLength;
    }
  }, [containerRef, children.length]);

  const autoScroll = useCallback(
    (targetPosition?: number) => {
      // only execute the autoScroll if the containerRef already has a value
      if (containerRef.current) {
        let scrollTargetPosition = targetPosition;

        // if targetPosition is undefined, set the width of the following single scroll to childrenWidth
        if (scrollTargetPosition === undefined) {
          scrollTargetPosition =
            containerRef.current.scrollLeft +
            (containerRef.current.children[0] as HTMLElement).offsetWidth;
        }

        // animate the scroll
        animateDraggable(
          scrollTargetPosition,
          scrollDuration,
          containerRef,
          mouseDownStateRef,
        );

        /*
        A new scroll was executed so clear the current timeout for adding the infinity impression
        and set a new timeout for this.
        only do that for non-draggable carousels as others are handled by `Draggable`'s onScrollEnd
       */
        if (!draggable) {
          if (infiniteTimeoutRef.current) {
            clearTimeout(infiniteTimeoutRef.current);
          }
          infiniteTimeoutRef.current = setTimeout(
            addInfiniteBlock,
            scrollDuration,
          );
        }
      }
    },
    // change the behavior of autoScroll on window size change
    // TODO: there's an antipattern here - refs and elements of window shouldn't be in a dep array
    // (they probably can be removed but check thoroughly if nothing breaks)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      containerRef,
      window.innerWidth,
      addInfiniteBlock,
      draggable,
      scrollDuration,
    ],
  );

  const onMouseDown: React.EventHandler<
    React.MouseEvent | React.TouchEvent
  > = () => {
    /*
      If autoscroll is enabled and a user decides to scroll by themselves, clear
      all intervals and timeouts associated with automatic scrolling.
    */
    if (autoscroll) {
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
      clearInterval(scrollInterval.current);
    }
    setIsMouseDown(true);
  };

  const containerSettings = useMemo(() => {
    const classes = `${SpacingClasses[spacing]} ${containerClassName}`;
    return draggable
      ? {
          Container: Draggable,
          containerProps: {
            hideScrollbars: true,
            className: classes,
            innerRef: containerRef,
            snap: !(autoscroll || !nativeSnapping),
            onScrollEnd: addInfiniteBlock,
          },
        }
      : {
          Container: Div,
          containerProps: {
            className: `no-touchy-touchy ${classes}`,
            ref: containerRef,
          },
        };
  }, [
    autoscroll,
    draggable,
    spacing,
    nativeSnapping,
    containerClassName,
    addInfiniteBlock,
  ]);

  useLayoutEffect(() => {
    /*
      Once the container reference is loaded, set the initial scroll position
      to the middle one of the three children containers
      (so that the infinity impressions algorithm starts from the middle).
    */
    if (containerRef.current) {
      const childrenWidth = (containerRef.current.children[0] as HTMLElement)
        .offsetWidth;
      containerRef.current.scrollLeft = children.length * childrenWidth;
    }
  }, [containerRef, children.length]);

  useEffect(() => {
    // onMouseUp fixes the position of scrollLeft after a user leaves it in the middle of a child
    const onMouseUp = () => {
      /*
        Make sure that everything was loaded and the mouseUp behavior is executed only
        for a preceding mouseDown behavior.
      */
      if (mouseDownStateRef.current) {
        setIsMouseDown(false);

        // TODO: Should we do something differnet?
        if (!containerRef.current) return;
        // First scroll until the end of the current child
        const { scrollLeft } = containerRef.current;
        const childrenWidth = (containerRef.current.children[0] as HTMLElement)
          .offsetWidth;

        // Get the number of children which scrollLeft already went through
        const scrollDivision = Math.floor(scrollLeft / childrenWidth);

        // Checks whether the scroll fix should go to the beginning or the end of the current child
        const targetPosition =
          (scrollDivision + 1) * childrenWidth - scrollLeft < childrenWidth / 2
            ? (scrollDivision + 1) * childrenWidth
            : scrollDivision * childrenWidth;

        // Set a reasonable scroll duration, taking into account
        // that it is not the usual scroll width
        const mouseUpScrollDuration =
          scrollDuration / 2 <= 100 ? scrollDuration : scrollDuration / 2;

        // Animate the scroll fix
        animateDraggable(
          targetPosition,
          mouseUpScrollDuration,
          containerRef,
          mouseDownStateRef,
        );

        /*
          A new scroll was executed so clear the current timeout for adding the infinity
          impression and set a new timeout for this.
          only do that for non-draggable carousels as others are handled
          by `Draggable`'s onScrollEnd
         */
        if (!draggable) {
          if (infiniteTimeoutRef.current) {
            clearTimeout(infiniteTimeoutRef.current);
          }
          infiniteTimeoutRef.current = setTimeout(
            addInfiniteBlock,
            mouseUpScrollDuration,
          );
        }

        // Resume interval of scrolling when the scrolling to the end of current child is completed
        if (autoscroll) {
          timeoutRef.current = setTimeout(() => {
            autoScroll();
            scrollInterval.current = setInterval(
              autoScroll,
              scrollDelay + scrollDuration, // resume after the current delay and duration passes
            );
          }, mouseUpScrollDuration);
        }
      }
    };

    if ((autoscroll || !nativeSnapping) && children && children.length > 0) {
      document.addEventListener('mouseup', onMouseUp);
      document.addEventListener('touchend', onMouseUp);
      if (autoscroll) {
        // set the autoscroll interval on component's mount and in case of props' changes.
        if (scrollInterval.current) {
          clearInterval(scrollInterval.current);
        }
        autoScroll();
        scrollInterval.current = setInterval(
          autoScroll,
          scrollDelay + scrollDuration,
        );
      }
    }

    // Clear every timeout, interval and eventListener on component's unmount
    return () => {
      document.removeEventListener('mouseup', onMouseUp);
      document.removeEventListener('touchend', onMouseUp);
      if (infiniteTimeoutRef.current) {
        clearTimeout(infiniteTimeoutRef.current);
      }
      if (arrowReadinessTimeout.current) {
        clearTimeout(arrowReadinessTimeout.current);
      }
      if (autoscroll) {
        clearInterval(scrollInterval.current);
        if (timeoutRef.current) {
          clearTimeout(timeoutRef.current);
        }
      }
    };
  }, [
    infiniteTimeoutRef,
    addInfiniteBlock,
    draggable,
    arrowReadinessTimeout,
    scrollInterval,
    autoscroll,
    nativeSnapping,
    children,
    setIsMouseDown,
    autoScroll,
    scrollDelay,
    scrollDuration,
  ]);

  const onArrowMove = (next = true) => {
    // If the scrolling behavior of the previous arrow click is still ongoing, do not do anything
    if (!isArrowReady || containerRef.current === null) {
      return;
    }
    const childrenWidth = (containerRef.current.children[0] as HTMLElement)
      .offsetWidth;

    // Set arrow readiness to false to prevent double clicks
    setIsArrowReady(false);

    // Animate a scroll on the arrow click
    animateDraggable(
      containerRef.current.scrollLeft + (next ? childrenWidth : -childrenWidth),
      scrollDuration,
      containerRef,
      mouseDownStateRef,
    );

    // Enable the arrow behavior again after the scroll animation is over
    arrowReadinessTimeout.current = setTimeout(
      () => setIsArrowReady(true),
      scrollDuration,
    );
  };

  // Handles rendering of a single container length
  const renderBlocks = (suffix = '') => {
    const keyPattern = (child: React.ReactElement) =>
      child.key
        ? `${child.key}-carousel-block${suffix}`
        : `${JSON.stringify(child)}${suffix}`;
    return children.map((child) => (
      <div
        key={keyPattern(child as React.ReactElement)}
        className="carousel-block"
      >
        {child}
      </div>
    ));
  };

  const { Container, containerProps } = containerSettings;

  const innerCarousel = (
    <div
      role="presentation"
      tabIndex={-1}
      className="carousel-wrapper"
      onMouseDown={autoscroll || !nativeSnapping ? onMouseDown : undefined}
      onTouchStart={autoscroll || !nativeSnapping ? onMouseDown : undefined}
    >
      <Container
        {...containerProps}
        className={`carousel ${containerProps.className}`}
      >
        {renderBlocks('-pre')}
        {renderBlocks()}
        {renderBlocks('-post')}
      </Container>
    </div>
  );

  return !arrows ? (
    innerCarousel
  ) : (
    <div className="carousel-with-arrows">
      <PrevArrow onClick={() => onArrowMove(false)} />
      {innerCarousel}
      <NextArrow onClick={() => onArrowMove(true)} />
    </div>
  );
}

function CarouselWrapper(props: CarouselProps) {
  // eslint-disable-next-line
  return props.children.length > 0 ? (
    // eslint-disable-next-line
    <Carousel {...props}>{props.children}</Carousel>
  ) : (
    <div className="carousel-loader">
      <CircularProgress size={70} />
    </div>
  );
}

export default CarouselWrapper;
