import CheckboxInput from 'components/CheckboxInput/CheckboxInput';
import SkeletonCheckboxes from 'components/CheckboxInput/skeleton/SkeletonCheckboxes';
import { useArgs } from '@storybook/addons';

export default {
  title: 'Components/CheckboxInput',
  component: CheckboxInput,
  tags: ['autodocs'],
  subcomponents: { SkeletonCheckboxes },
  decorators: [(Story) => <div style={{ width: '200px' }}>{Story()}</div>],
};

function Template({ ...args }) {
  const [{ checked, disabled }, updateArgs] = useArgs();
  const handleClick = () => {
    if (!disabled) {
      updateArgs({ checked: !checked });
    }
  };
  return <CheckboxInput {...args} onChange={handleClick} />;
}

export const Default = {
  render: Template,

  args: {
    checked: true,
    label: 'Checkbox Input',
  },
};

export const Skeleton = {
  render: Template,

  args: {
    checked: true,
    disabled: true,
    label: '',
    isSkeleton: true,
  },
};

export const SkeletonGroup = {
  render: ({ length }) => <SkeletonCheckboxes length={length} />,
  args: {
    length: 8,
  },
  parameters: {
    controls: {
      exclude: /[^length]/g,
    },
  },
};
