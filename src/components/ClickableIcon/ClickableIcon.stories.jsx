import ClickableIcon from 'components/ClickableIcon/ClickableIcon';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

export default {
  title: 'Components/ClickableIcon',
  component: ClickableIcon,
  tags: ['autodocs'],
  args: {
    onClick: () => alert('You cliked a ClickableIcon!'),
    icon: ArrowForwardIosIcon,
  },
  argTypes: {
    icon: {
      controls: false,
    },
  },
};

export const Default = {};
