import ConditionalInput from 'components/ConditionalInput/ConditionalInput';
import EditIcons from 'components/EditIcons/EditIcons';
import { useArgs } from '@storybook/addons';
import Paragraph from 'utils/componentsWrappers/Paragraph';
import TextInput from 'components/TextInput/TextInput';

export default {
  title: 'Components/ConditionalInput',
  component: ConditionalInput,
  tags: ['autodocs'],
  argTypes: {
    staticComponent: { control: false },
    inputComponent: { control: false },
  },
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
};

function TextConditionalInputFunc(args) {
  const [{ isAllowedToEdit, content, editableContent, condition }, updateArgs] =
    useArgs();

  return (
    <div style={{ minWidth: '200px' }}>
      <EditIcons
        onEditSubmit={() =>
          updateArgs({
            content: editableContent,
            condition: false,
          })
        }
        onEditCancel={() =>
          updateArgs({
            condition: false,
            editableContent: content,
          })
        }
        onEditStart={() => updateArgs({ condition: true })}
        isBeingEdited={condition}
        shouldDisplay={isAllowedToEdit}
      />
      {!content && !condition && (
        <p style={{ opacity: 0.4 }}>Nothing is here :(</p>
      )}
      <ConditionalInput
        {...args}
        isAllowedToEdit={isAllowedToEdit}
        editableContent={editableContent}
        condition={condition}
        onChange={(e) => updateArgs({ editableContent: e.target.value })}
        content={content}
      />
    </div>
  );
}
export const TextConditionalInput = {
  render: TextConditionalInputFunc,
  args: {
    content: 'Text',
    editableContent: 'Text',
    condition: false,
    staticComponent: {
      element: Paragraph,
    },
    inputComponent: {
      element: TextInput,
      props: {
        placeholder: 'Write here',
      },
    },
    isAllowedToEdit: true,
  },
};
