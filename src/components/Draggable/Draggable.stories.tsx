import Draggable from 'components/Draggable/Draggable';
import { generateExampleItems } from '../../../.storybook/helpers';
import { Meta, StoryObj } from '@storybook/react';

const meta = {
  title: 'Components/Draggable',
  component: Draggable,
  tags: ['autodocs'],
  decorators: [
    (Story, context) => {
      let classes = '';
      classes += context.args.horizontal ? ' draggable-sb-helper' : '';
      classes += context.args.vertical ? ' draggable-sb-helper-vertical' : '';
      return (
        <div className={classes}>
          <Draggable
            {...context.args}
            horizontal={context.args.horizontal}
            vertical={context.args.vertical}
          />
        </div>
      );
    },
  ],
  args: {
    children: generateExampleItems(10),
    onScrollStart: undefined,
    onScrollEnd: undefined,
  },
  argTypes: {
    children: { control: false },
    onScrollEnd: { control: false },
    onScrollStart: { control: false },
    innerRef: { control: false },
  },
} satisfies Meta<typeof Draggable>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Horizontal: Story = {
  args: {
    vertical: false,
    horizontal: true,
  },
};

export const Vertical: Story = {
  args: {
    vertical: true,
    horizontal: false,
  },
};

export const Both: Story = {
  args: {
    vertical: true,
    horizontal: true,
  },
};

export const NoSnap: Story = {
  args: {
    vertical: false,
    horizontal: true,
    snap: false,
  },
};
