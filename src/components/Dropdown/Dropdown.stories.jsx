import Dropdown from 'components/Dropdown/Dropdown';
import SkeletonDropdown from 'components/Dropdown/skeleton/SkeletonDropdown';
import { useArgs } from '@storybook/addons';

export default {
  title: 'Components/Dropdown',
  component: Dropdown,
  tags: ['autodocs'],
  subcomponents: { SkeletonDropdown },
  decorators: [
    (Story, context) => {
      let minHeight;
      if (!context.args.options) {
        minHeight = 'unset';
      } else {
        minHeight = (1 + context.args.options.length) * 38;
        minHeight = `${minHeight > 540 ? 540 : minHeight}px`;
      }
      return <div style={{ width: '200px', minHeight }}>{Story()}</div>;
    },
  ],
};

function Template(args) {
  // eslint-disable-next-line no-unused-vars
  const [, updateArgs] = useArgs();
  return (
    <Dropdown
      {...args}
      onSelectClick={(option) =>
        /* eslint-disable-next-line react/destructuring-assignment */
        updateArgs({ label: option[args.optionLabel] })
      }
    />
  );
}

export const Default = {
  render: Template,

  args: {
    label: 'Choose an option',
    optionLabel: 'name',
    optionKey: 'slug',
    options: [
      {
        slug: 'option-1',
        name: 'Option 1',
      },
      {
        slug: 'option-2',
        name: 'Option 2',
      },
      {
        slug: 'option-3',
        name: 'Option 3',
      },
      {
        slug: 'option-4',
        name: 'Option 4',
      },
      {
        slug: 'option-5',
        name: 'Option 5',
      },
    ],
  },
};

export function Skeleton() {
  return <SkeletonDropdown />;
}
