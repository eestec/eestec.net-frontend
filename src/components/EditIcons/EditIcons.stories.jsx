import EditIcons from 'components/EditIcons/EditIcons';
import { useArgs } from '@storybook/addons';

export default {
  title: 'Components/ConditionalInput/EditIcons',
  component: EditIcons,
  tags: ['autodocs'],
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
};

export function Default(args) {
  // eslint-disable-next-line no-unused-vars
  const [, updateArgs] = useArgs();

  return (
    <EditIcons
      {...args}
      onEditStart={() => updateArgs({ isBeingEdited: true })}
      onEditCancel={() => updateArgs({ isBeingEdited: false })}
      onEditEnd={() => updateArgs({ isBeingEdited: false })}
    />
  );
}
