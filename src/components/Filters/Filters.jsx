import { useEffect, useId, useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'components/Dropdown/Dropdown';
import useDebouncedSearch from 'hooks/useDebouncedSearch';
import FilteringTextInput from 'components/FilteringTextInput/FilteringTextInput';
import CheckboxInput from 'components/CheckboxInput/CheckboxInput';
import SkeletonCheckboxes from 'components/CheckboxInput/skeleton/SkeletonCheckboxes';
import SkeletonDropdown from 'components/Dropdown/skeleton/SkeletonDropdown';
import IconButton from '@mui/material/IconButton';
import FilterListIcon from '@mui/icons-material/FilterList';
import CloseIcon from '@mui/icons-material/Close';

import './Filters.css';

function renderCheckboxSection(
  { loading, options, title, onClick, skeletonLength },
  sectionId,
  disabled,
  className,
) {
  return (
    <div className="filter-section checkboxes" key={sectionId}>
      <p className="filter-subtitle">{title}</p>
      <div className={className}>
        {loading ? (
          <SkeletonCheckboxes length={skeletonLength ?? 10} />
        ) : (
          options.map(({ name, id, enabled }, optionIdx) => (
            <CheckboxInput
              label={name}
              key={`${sectionId}-${id}`}
              id={`${sectionId}-${id}`}
              checked={enabled}
              disabled={disabled}
              onChange={() => onClick(optionIdx)}
            />
          ))
        )}
      </div>
    </div>
  );
}

function renderDropdownSection(
  { loading, options, optionLabel, optionKey, label, title, onClick },
  sectionId,
  disabled,
  className,
) {
  return (
    <Fragment key={sectionId}>
      <p className="filter-subtitle">{title}</p>
      <div className={className}>
        {loading ? (
          <SkeletonDropdown />
        ) : (
          <Dropdown
            options={options}
            optionLabel={optionLabel}
            optionKey={optionKey}
            id={sectionId}
            label={label}
            onSelectClick={onClick}
            disabled={disabled}
          />
        )}
      </div>
    </Fragment>
  );
}

/**
 __Filters__ is a reusable component for implementing different types of filter boxes.
 It accepts arrays of different checkbox filter properties
 and dropdown filter properties, together with properties of the filter search.
 */
export default function Filters({
  checkboxFilters,
  dropdownFilters,
  textFilter,
  title,
  disabled,
}) {
  const id = useId();
  const [isMobileFilterExpanded, setIsMobileFilterExpanded] = useState(false);
  const [filterSearchTerm, setFilterSearchTerm] = useDebouncedSearch({
    onFilteredSearchSubmit: textFilter.onChange,
    initialValue: textFilter.initialValue,
  });

  // this effect locks scrolling when filters menu is expanded on mobiles
  useEffect(() => {
    if (isMobileFilterExpanded) {
      document.body.classList.add('locked-scroll');
    } else {
      document.body.classList.remove('locked-scroll');
    }
  }, [isMobileFilterExpanded]);

  const toggleMobileFilterExpanded = () =>
    setIsMobileFilterExpanded((prevState) => !prevState);

  let filterClasses = 'filter';
  filterClasses += isMobileFilterExpanded ? ' mobile-filter-expanded' : '';

  let filterOptionsClasses = 'filter-options';
  filterOptionsClasses += disabled ? ' filter-input-disabled' : '';

  return (
    <div className={filterClasses}>
      <div className="filter-search">
        {textFilter.enabled && (
          <FilteringTextInput
            onChange={(e) => setFilterSearchTerm(e.target.value)}
            value={filterSearchTerm}
            title={textFilter.title}
            placeholder={textFilter.title}
          />
        )}
        <IconButton
          onClick={toggleMobileFilterExpanded}
          disableRipple
          title={title}
          className="filtering-icon"
        >
          {isMobileFilterExpanded ? <CloseIcon /> : <FilterListIcon />}
        </IconButton>
      </div>
      <div className="filter-inner">
        <p className="filter-title">Filters</p>
        {checkboxFilters.map((checkboxFilter, checkboxFilterIdx) =>
          renderCheckboxSection(
            checkboxFilter,
            `${id}-${checkboxFilterIdx}`,
            disabled,
            filterOptionsClasses,
          ),
        )}
        {dropdownFilters.length > 0 && (
          <div className="filter-section dropdowns">
            {dropdownFilters.map((dropdownFilter, dropdownFilterIdx) =>
              renderDropdownSection(
                dropdownFilter,
                `${id}-${dropdownFilterIdx}`,
                disabled,
                filterOptionsClasses,
              ),
            )}
          </div>
        )}
      </div>
    </div>
  );
}

Filters.propTypes = {
  /**
   An array of properties that are used for rendering checkbox filter sections.
   Each element in this array renders a separate section with different onClick function.
  */
  checkboxFilters: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          enabled: PropTypes.bool.isRequired,
          name: PropTypes.string.isRequired,
          id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
            .isRequired,
        }),
      ),
      loading: PropTypes.bool,
      skeletonLength: PropTypes.number,
    }),
  ),
  /**
   An array of properties that are used for rendering dropdown filter sections.
   Each element in this array renders a separate section with different onClick function.
   */
  dropdownFilters: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
      options: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])),
      loading: PropTypes.bool,
      optionLabel: PropTypes.string.isRequired,
      optionKey: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }),
  ),
  /**
   An object containing properties that are used for handling the search input inside the
   filtering component. These properties are passed further to `FilteringTextInput` component.
   */
  textFilter: PropTypes.shape({
    enabled: PropTypes.bool,
    initialValue: PropTypes.string,
    title: PropTypes.string,
    onChange: PropTypes.func,
  }),
  /**
   A title of the filter component - used for accessibility purposes
   */
  title: PropTypes.string,
  /**
   A boolean property indicating whether the component is disabled and should not be clickable.
   */
  disabled: PropTypes.bool,
};

Filters.defaultProps = {
  checkboxFilters: [],
  dropdownFilters: [],
  textFilter: {
    enabled: true,
    initialValue: '',
    title: 'Search',
    onChange: () => {},
  },
  title: 'Filters',
  disabled: false,
};
