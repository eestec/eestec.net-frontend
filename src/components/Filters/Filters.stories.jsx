import { useState } from 'react';
import Filters from 'components/Filters/Filters';

export default {
  title: 'Components/Filters',
  component: Filters,
  tags: ['autodocs'],
  decorators: [
    (Story) => <div className="filter-column-wrapper">{Story()}</div>,
  ],
  argTypes: {
    dropdownFilters: { control: false },
    checkboxFilters: { control: false },
    textFilter: { control: false },
  },
};

function Template({ disabled, loading, textFilterEnabled }) {
  const [chosenOption, setChosenOption] = useState({ value: 'Item One' });
  const [checkboxOptions, setCheckboxOptions] = useState([
    {
      name: 'Checkbox A',
      id: 'checkbox-a',
      enabled: false,
    },
    {
      name: 'Checkbox B',
      id: 'checkbox-b',
      enabled: false,
    },
    {
      name: 'Checkbox C',
      id: 'checkbox-c',
      enabled: false,
    },
  ]);

  return (
    <Filters
      disabled={disabled}
      textFilter={{
        enabled: textFilterEnabled,
        initialValue: '',
        title: 'Search',
        onChange: (val) => console.log(val),
      }}
      checkboxFilters={[
        {
          title: 'Checkboxes',
          onClick: (val) =>
            setCheckboxOptions((prevState) => {
              const newState = [...prevState];
              newState[val].enabled = !newState[val].enabled;
              return newState;
            }),
          options: checkboxOptions,
          loading,
        },
      ]}
      dropdownFilters={[
        {
          title: 'Dropdown',
          onClick: (val) => setChosenOption(val),
          options: [
            {
              id: 'item-one',
              value: 'Item One',
            },
            {
              id: 'item-two',
              value: 'Item Two',
            },
          ],
          optionLabel: 'value',
          optionKey: 'id',
          label: chosenOption.value,
          loading,
        },
      ]}
    />
  );
}

export const Default = Template.bind({});
Default.args = {
  disabled: false,
  loading: false,
  textFilterEnabled: true,
};
