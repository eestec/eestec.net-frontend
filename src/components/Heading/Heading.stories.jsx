import Heading from 'components/Heading/Heading';

export default {
  title: 'Components/Heading',
  component: Heading,
  tags: ['autodocs'],
  argTypes: {
    innerRef: {
      control: false,
    },
  },
};

export const Default = {
  args: {
    variant: 'h1',
    lineAfter: true,
    children: 'Heading',
  },
};
