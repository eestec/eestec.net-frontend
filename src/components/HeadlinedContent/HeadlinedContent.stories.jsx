import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import { headingVariants } from 'utils/text';
import personPlaceholder from 'assets/img/member-placeholder.jpg';
import { loremIpsum } from '../../../.storybook/helpers';

export default {
  title: 'Components/HeadlinedContent',
  component: HeadlinedContent,
  tags: ['autodocs'],
  args: {
    children: <p>{loremIpsum}</p>,
    variant: 'h1',
  },
  decorators: [(Story) => <div className="section">{Story()}</div>],
  parameters: {
    layout: 'fullwidth',
  },
  argTypes: {
    children: { control: false },
    innerRef: {
      control: false,
    },
    variant: {
      options: headingVariants,
    },
    styleVariant: {
      options: headingVariants,
    },
  },
};

export const Left = {
  args: {
    title: 'Left',
  },
};

export const Right = {
  args: {
    title: 'Right',
    splitRight: true,
  },
};

export const FullWidth = {
  args: {
    title: 'Full width',
    fullWidthText: true,
  },
};

export const WithImage = {
  args: {
    title: 'With an image',
    img: {
      imgId: 'headlined-content-image',
      imgSrc: personPlaceholder,
      imgAlt: 'The image of a headlined content',
    },
  },
};
