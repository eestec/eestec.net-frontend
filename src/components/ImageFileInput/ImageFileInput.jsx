import { useContext, useCallback } from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import ToastContext from 'context/ToastContext';
import { niceBytes } from 'utils/misc';
import useChangeWithConfirmation, {
  copyrightsText,
} from 'hooks/useChangeWithConfirmation';

import './ImageFileInput.css';

const MAX_IMG_FILESIZE = 2097152; // in bytes 2097152

/**
 __ImageFileInput__ is a button serving as an input of type `file`.
 It is displayed in a form of an circular icon and accepts PNG, JPG, SVG and GIF
 image formats.

 In the future this component can be extended if needed - to accept other types of files
 depending on the given properties.
 */
function ImageFileInput({
  variant,
  title,
  Icon,
  className,
  disabled,
  onChange,
  accept,
  isLoading,
  maxImgFilesize,
  checkCopyrights,
}) {
  const { setToastContent } = useContext(ToastContext);
  let classes = 'picture-change-button';
  classes += className ? ` ${className}` : '';

  const handleChange = useChangeWithConfirmation({
    enabled: checkCopyrights,
    onConfirm: useCallback(
      (e) => {
        if (e.target.files.length > 0) {
          const newImage = e.target.files[0];

          if (newImage?.size > maxImgFilesize) {
            setToastContent(
              `The image that you're trying to upload exceeds the 2 MB limit (${niceBytes(
                newImage.size,
              )}).`,
              'error',
            );
            return;
          }
          onChange(newImage);
        }
      },
      [setToastContent, maxImgFilesize, onChange],
    ),
    textContent: copyrightsText,
  });

  const resetFileInput = (e) => {
    e.target.value = null;
  };

  if (isLoading) {
    return (
      <div className="image-file-input-loader">
        <CircularProgress size={32} />
      </div>
    );
  }

  if (variant === 'button') {
    return (
      <Button
        className="primary-button"
        variant="contained"
        component="label"
        onClick={resetFileInput}
        style={{ backgroundColor: 'var(--primary-red)' }} // todo: Styled
        disabled={disabled}
        endIcon={<Icon />}
      >
        {title}
        <input onChange={handleChange} type="file" hidden accept={accept} />
      </Button>
    );
  }

  const iconContent = (
    <IconButton
      component="label"
      disabled={disabled}
      className={classes}
      onClick={resetFileInput}
    >
      <Icon />
      <input onChange={handleChange} type="file" hidden accept={accept} />
    </IconButton>
  );

  return title ? (
    <Tooltip title={title} arrow>
      {iconContent}
    </Tooltip>
  ) : (
    iconContent
  );
}

ImageFileInput.propTypes = {
  className: PropTypes.string,
  /**
   Displayed on hover as a tooltip.
  */
  title: PropTypes.string,
  /**
   An imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/).
  */
  Icon: PropTypes.elementType.isRequired,
  disabled: PropTypes.bool,
  /**
   The component passes the chosen file as a parameter of the given function.
  */
  onChange: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  accept: PropTypes.string,
  maxImgFilesize: PropTypes.number,
  checkCopyrights: PropTypes.bool,
  variant: PropTypes.oneOf(['icon', 'button']),
};

ImageFileInput.defaultProps = {
  className: null,
  title: '',
  disabled: false,
  isLoading: false,
  accept: 'image/png, image/jpeg, image/jpg, image/svg, image/gif',
  maxImgFilesize: MAX_IMG_FILESIZE,
  checkCopyrights: false,
  variant: 'icon',
};

export default ImageFileInput;
