import ImageFileInput from 'components/ImageFileInput/ImageFileInput';
import VrpanoIcon from '@mui/icons-material/Vrpano';

export default {
  title: 'Components/ImageFileInput',
  component: ImageFileInput,
  tags: ['autodocs'],
  argTypes: {
    Icon: { control: false },
  },
};

export const Default = {
  args: {
    // eslint-disable-next-line no-console
    onChange: (e) => console.log(e),
    Icon: VrpanoIcon,
  },
};
