import Message from 'components/Message/Message';
import ErrorMessageComponent from 'components/Message/components/ErrorMessage/ErrorMessage';
import SuccessMessageComponent from 'components/Message/components/SuccessMessage/SuccessMessage';
import WarningMessageComponent from 'components/Message/components/WarningMessage/WarningMessage';
import BoltIcon from '@mui/icons-material/Bolt';

export default {
  title: 'Components/Message',
  component: Message,
  tags: ['autodocs'],
  subcomponents: {
    WarningMessage: WarningMessageComponent,
    ErrorMessage: ErrorMessageComponent,
    SuccessMessage: SuccessMessageComponent,
  },
  argTypes: {
    Icon: { control: false },
  },
};

export const Default = {
  args: {
    Icon: BoltIcon,
    children: 'This is a message',
  },
};

export const ErrorMessage = {
  render: (args) => <ErrorMessageComponent {...args} />,
  args: {
    children: 'This is an error message',
  },
};

export const WarningMessage = {
  render: (args) => <WarningMessageComponent {...args} />,
  args: {
    children: 'This is an warning message',
  },
};

export const SuccessMessage = {
  render: (args) => <SuccessMessageComponent {...args} />,
  args: {
    children: 'This is an success message',
  },
};
