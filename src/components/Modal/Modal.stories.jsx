import { useContext } from 'react';
import Modal from 'components/Modal/Modal';
import Button from 'components/Button/Button';
import ModalContext from 'context/ModalContext';
import SingleContactBoard from 'views/public/Contact/components/ContactBoard/SingleContactBoard/SingleContactBoard';
import { ConfirmationModal } from 'hooks/useChangeWithConfirmation/components/ConfirmationModal';
import personPlaceholder from 'assets/img/member-placeholder.jpg';
import ModalContextDecorator from '../../../.storybook/decorators/ModalContextDecorator';

export default {
  title: 'Components/Modal',
  component: Modal,
  tags: ['autodocs'],
  subcomponents: { SingleContactBoard, ConfirmationModal },
  decorators: [ModalContextDecorator],
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    withWrapper: {
      table: {
        disable: true,
      },
    },
    modalContent: {
      defaultValue: 'Hey, I am the content of the modal!',
      description:
        "The node that is to be put in the modal (set by the context's " +
        '`setModalContent` function. Examples of `modalContent` can be' +
        '`ConfirmationModal` or `SingleContactBoard`.',
      table: {
        type: { summary: 'node' },
        defaultValue: { summary: '' },
      },
      control: {
        type: 'text',
      },
    },
  },
};

function Template({ withWrapper, modalContent }) {
  const { setModalContent } = useContext(ModalContext);

  return (
    <Button
      onClick={() =>
        setModalContent(
          withWrapper ? (
            <p
              style={{
                background: 'var(--primary-white)',
                padding: '5rem',
                borderRadius: '2rem',
                color: '#000',
              }}
            >
              {modalContent}
            </p>
          ) : (
            modalContent
          ),
        )
      }
      className="primary-button"
    >
      Open a modal
    </Button>
  );
}

export const Default = {
  render: Template,

  args: {
    withWrapper: true,
  },
};

export const BoardInfo = {
  render: Template,

  args: {
    withWrapper: false,
    modalContent: (
      <SingleContactBoard
        modalData={{
          img: personPlaceholder,
          desc: ['I do stuff', 'And other stuff'],
          name: 'John Doe',
          occupation: 'The ultimate EESTECer',
          alt: 'John Doe',
        }}
      />
    ),
  },

  argTypes: {
    modalContent: { control: false },
  },
};

export const ConfirmationInfo = {
  render: Template,

  args: {
    withWrapper: false,
    modalContent: (
      <ConfirmationModal
        // eslint-disable-next-line no-console
        onConfirm={() => console.log('You did agree!')}
        // eslint-disable-next-line no-console
        onCancel={() => console.log('You did not agree!')}
      >
        <p>Do you want to agree?</p>
      </ConfirmationModal>
    ),
  },

  argTypes: {
    modalContent: { control: false },
  },
};
