import NavLink from 'components/NavLink/NavLink';
import { withRouter } from 'storybook-addon-react-router-v6';
import HeaderNavLinkComponent from 'layouts/MainLayout/components/Header/components/HeaderNavLink/HeaderNavLink';
import FooterNavLinkComponent from 'layouts/MainLayout/components/Footer/components/FooterNavLink/FooterNavLink';

export default {
  title: 'Components/Link variants/NavLink',
  component: NavLink,
  tags: ['autodocs'],
  decorators: [withRouter],
  argTypes: {
    match: {
      control: {
        type: 'boolean',
      },
      table: {
        disable: true,
      },
      defaultValue: false,
    },
    to: { control: false },
  },
};

function Template({ match, to, ...args }) {
  return <NavLink {...args} to={match ? '/' : to} />;
}

export const Default = {
  render: Template,

  args: {
    to: '/path?=/story/components/linkvariants-navlink',
    children: 'Link',
  },
};

export const Active = {
  render: Template,

  args: {
    to: '/path?=/story/components/linkvariants-navlink',
    children: 'Link',
    match: true,
  },
};

export const HeaderNavLink = {
  render: (args) => <HeaderNavLinkComponent {...args} />,
  args: {
    to: '/path?=/story/components/linkvariants-navlink',
    children: 'Header Link',
    // eslint-disable-next-line no-console
    onClickHandler: () => console.log('You clicked the HeaderNavLink!'),
  },
};

function FooterNavLinkFunc(args) {
  return (
    <div className="storybook-footernavlink">
      <FooterNavLinkComponent {...args} />
    </div>
  );
}

export const FooterNavLink = {
  render: FooterNavLinkFunc,
  args: {
    to: '/path?=/story/components/linkvariants-navlink',
    children: 'Footer Link',
    // eslint-disable-next-line no-console
    onClickHandler: () => console.log('You clicked the FooterNavLink!'),
  },
};
