/* eslint-disable no-alert */
import PersonCard from 'components/PersonCard/PersonCard';
import { withRouter } from 'storybook-addon-react-router-v6';
import { splitButtonMockedProps } from '../../../.storybook/helpers';

export default {
  title: 'Components/Cards/PersonCard',
  component: PersonCard,
  tags: ['autodocs'],
  decorators: [withRouter],
  args: {
    item: {
      first_name: 'John Paul',
      last_name: 'Doe',
      slug: 'john-paul-doe',
    },
  },
  argTypes: {
    customCardButton: {
      options: ['None', 'SplitButton'],
      control: { type: 'radio' },
      defaultValue: 'None',
    },
  },
};

function Template({ customCardButton, ...args }) {
  return (
    <PersonCard
      {...args}
      customCardButton={
        customCardButton === 'None' ? null : splitButtonMockedProps
      }
    />
  );
}

export const Default = {
  render: Template,
};

export const WithPosition = {
  render: Template,

  args: {
    item: {
      first_name: 'John Paul',
      last_name: 'Doe',
      slug: 'john-paul-doe',
      position: 'Team X Coordinator for Y',
    },
  },
};
