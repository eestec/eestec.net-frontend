import SlidingElement from 'components/SlidingElement/SlidingElement';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import { loremIpsum } from '../../../.storybook/helpers';

export default {
  title: 'Components/SlidingElement',
  component: SlidingElement,
  tags: ['autodocs'],
  decorators: [
    (Story) => (
      <div style={{ overflow: 'hidden' }} className="section">
        {Story()}
      </div>
    ),
  ],
  parameters: {
    layout: 'fullscreen',
  },
};

export const Left = {
  args: {
    children: [
      <SlidingElement key="Sliding Element - Left">
        <HeadlinedContent
          title="Sliding Element - Left"
          variant="h1"
          fullWidthText
        >
          <p>{loremIpsum}</p>
        </HeadlinedContent>
      </SlidingElement>,
    ],
    slideRight: false,
  },
};

export const Right = {
  args: {
    children: [
      <SlidingElement key="Sliding Element - Right">
        <HeadlinedContent
          title="Sliding Element - Right"
          variant="h1"
          fullWidthText
          splitRight
        >
          <p>{loremIpsum}</p>
        </HeadlinedContent>
      </SlidingElement>,
    ],
    slideRight: true,
  },
};
