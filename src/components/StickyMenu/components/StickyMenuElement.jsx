import scroll from 'utils/scroll';
import LinkButton from 'components/LinkButton/LinkButton';
import { stickyMenuElementPropTypes } from '../utils/prop-types';

/**
 __StickyMenuElement__ is a component rendered by `StickyMenu`, representing a single element (link)
 that scrolls to the `target` and can contain `subElements` that scroll to sub sections of that
 element.
 */
function StickyMenuElement({
  title,
  target,
  subElements,
  depth,
  scrollOffset,
}) {
  const scrollToTarget = () =>
    scroll(target, null, scrollOffset, 1000, 'easeOutBack', 1);
  let classes = 'sticky-menu-element';
  if (depth > 0) {
    classes += ` sticky-menu-element-depth-${depth}`;
  }
  return (
    <div className={classes}>
      <LinkButton
        onClick={scrollToTarget}
        className="sticky-menu-element-title"
      >
        {title}
      </LinkButton>
      <div className="sticky-menu-subelements">
        {subElements.map((subElement) => (
          <StickyMenuElement
            title={subElement.title}
            target={subElement.target}
            key={subElement.title}
            subElements={subElement.subElements}
            depth={depth + 1}
            scrollOffset={scrollOffset}
          />
        ))}
      </div>
    </div>
  );
}

StickyMenuElement.propTypes = stickyMenuElementPropTypes;

StickyMenuElement.defaultProps = {
  subElements: [],
  depth: 0,
  scrollOffset: 0,
};

export default StickyMenuElement;
