import TextInput from 'components/TextInput/TextInput';
import { useArgs } from '@storybook/addons';

export default {
  title: 'Components/TextInput',
  component: TextInput,
  tags: ['autodocs'],
  args: {
    value: '',
  },
  argTypes: {
    ref: {
      control: false,
    },
  },
};

function Template({ value, ...args }) {
  // eslint-disable-next-line no-unused-vars
  const [, updateArgs] = useArgs();
  return (
    <TextInput
      {...args}
      value={value}
      onValueChange={(e) => updateArgs({ value: e.target.value })}
    />
  );
}

export const Default = {
  render: Template,

  args: {
    label: 'Default input',
  },
};

export const Password = {
  render: Template,

  args: {
    label: 'Password input',
    type: 'password',
  },
};

export const WithError = {
  render: Template,

  args: {
    label: 'Input with error',
    errorMessage: 'Something went wrong!',
  },
};
