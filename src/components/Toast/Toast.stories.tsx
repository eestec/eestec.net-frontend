import { Meta, StoryObj } from '@storybook/react';

import Toast from 'components/Toast/Toast';
import ToastContextDecorator from '../../../.storybook/decorators/ToastContextDecorator';

const meta = {
  title: 'Components/Toast',
  component: Toast,
  decorators: [ToastContextDecorator],
  tags: ['autodocs'],
  args: {
    type: 'info',
    content: 'Hey, I am a Toast!',
    autoHideDuration: null,
    vertical: 'bottom',
    horizontal: 'right',
    open: true,
  },
  argTypes: {
    type: {
      options: ['info', 'success', 'warning', 'error'],
      control: { type: 'radio' },
      description: 'Indicates what type of information is being displayed.',
      table: {
        type: { summary: ["'info'", " 'success'", " 'warning'", " 'error'"] },
        defaultValue: { summary: "'info'" },
      },
    },
    content: {
      table: {
        type: { summary: 'string' },
        defaultValue: { summary: '' },
      },
      control: {
        type: 'text',
      },
    },
    autoHideDuration: {
      control: false,
      description:
        'The amount of ms after which the Toast will hide automatically',
      table: {
        defaultValue: { summary: 6000 },
        type: { summary: 'number' },
      },
    },
    vertical: {
      control: false,
      description: 'The absolute vertical positioning of the Toast',
      table: {
        defaultValue: { summary: "'bottom'" },
        type: { summary: ["'bottom'", " 'top'"] },
      },
    },
    horizontal: {
      control: false,
      description: 'The absolute horizontal positioning of the Toast',
      table: {
        defaultValue: { summary: "'right'" },
        type: { summary: ["'right'", " 'left'"] },
      },
    },
    open: {
      control: false,
      description: 'Indicates if the Toast should be visible',
      table: {
        defaultValue: { summary: false },
        type: { summary: 'boolean' },
      },
    },
  },
} satisfies Meta<typeof Toast>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Info: Story = {
  args: {},
};

export const Success: Story = {
  args: {
    type: 'success',
    content: 'Hey, I am a successful Toast!',
  },
};

export const Warning: Story = {
  args: {
    type: 'warning',
    content: 'Hey, I am a warning Toast!',
  },
};

export const Error: Story = {
  args: {
    type: 'error',
    content: 'Hey, I am an error Toast!',
  },
};
