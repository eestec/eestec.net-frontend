import {
  createContext,
  useContext,
  useEffect,
  useMemo,
  useState,
  useCallback,
} from 'react';
import { loadFromLocalStorage, saveToLocalStorage } from 'utils/localStorage';
import PropTypes from 'prop-types';
import AuthContext from './AuthContext';
import logger from 'utils/logger';
import { AdminService } from 'api/service/admin';

const AdminContext = createContext({
  adminData: null,
  isStale: true,
  adminError: null,
});

export function AdminProvider({ children }) {
  const { loggedIn } = useContext(AuthContext);
  const [adminData, setAdminData] = useState(() =>
    loggedIn ? loadFromLocalStorage('adminData') : null,
  );
  const [isStale, setIsStale] = useState(true);
  const [error, setError] = useState(null);

  const refetchAdminData = useCallback(() => {
    async function handleAdminData() {
      try {
        const res = await AdminService.getAdminData();
        const length = Object.values(res.entities).reduce(
          (acc, cur) => acc + (cur?.length || 0),
          0,
        );
        setAdminData({
          ...res,
          length,
        });
        setIsStale(false);
      } catch (err) {
        logger.log(err);
        setError(err?.data?.message || err);
        setIsStale(false);
        setAdminData(null);
      }
    }

    if (loggedIn) handleAdminData();
    if (!loggedIn) {
      setError(null);
      setAdminData(null);
      setIsStale(false);
    }
  }, [loggedIn, setAdminData, setIsStale, setError]);

  useEffect(() => {
    refetchAdminData();
  }, [refetchAdminData]);

  // On every single change of user admin data, update the local storage
  useEffect(() => {
    if (!isStale) {
      saveToLocalStorage('adminData', adminData);
    }
  }, [adminData, isStale]);

  const contextValue = useMemo(
    () => ({
      adminData,
      isStale,
      refetchAdminData,
      adminError: error,
    }),
    [adminData, isStale, error, refetchAdminData],
  );

  return (
    <AdminContext.Provider value={contextValue}>
      {children}
    </AdminContext.Provider>
  );
}

AdminProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AdminContext;
