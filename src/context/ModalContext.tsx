import React, {
  createContext,
  useMemo,
  useState,
  useEffect,
  useCallback,
} from 'react';

export type ModalContextType = {
  modalContent: React.ReactNode;
  modalOptions?: ModalOptions;
  setModalContent: (
    newContent: React.ReactNode,
    options?: ModalOptions,
  ) => void;
};

type ModalProviderProps = {
  children: React.ReactNode;
};

type ModalOptions = {
  dismissable: boolean;
};

const ModalContext = createContext<ModalContextType | null>(null);

/**
 __ModalProvider__ is a React Context provider used for displaying information in modals.
 Since modals can be shown on different pages on the website,
 a global state had to be used for it.

 Another approach to implement modals could be done by using React Portals.
*/
export function ModalProvider({ children }: ModalProviderProps) {
  const [modalContent, _setModalContent] = useState<React.ReactNode>(null);
  const [modalOptions, setModalOptions] = useState<ModalOptions | undefined>(
    undefined,
  );

  const setModalContent = useCallback(
    (newContent: React.ReactNode, options?: ModalOptions) => {
      _setModalContent(newContent);
      setModalOptions(!newContent ? undefined : options);
    },
    [_setModalContent, setModalOptions],
  );

  const modalValue = useMemo(
    () => ({ modalContent, setModalContent, modalOptions }),
    [modalContent, setModalContent, modalOptions],
  );

  useEffect(() => {
    if (modalContent) {
      document.body.classList.add('locked-scroll');
    } else {
      document.body.classList.remove('locked-scroll');
    }
  }, [modalContent]);

  return (
    <ModalContext.Provider value={modalValue}>{children}</ModalContext.Provider>
  );
}

export default ModalContext;
