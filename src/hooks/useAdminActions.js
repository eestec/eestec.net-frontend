import { useCallback, useContext, useMemo } from 'react';
import ToastContext from 'context/ToastContext';
import BranchService from 'api/service/branches';
import { BranchRolesLabelsMap } from 'utils/roles';
import logger from 'utils/logger';

const entityData = {
  cities: { service: BranchService, roles: BranchRolesLabelsMap },
  // teams: TeamService
};
const memberRoleFields = {
  cities: 'branch_role_id',
};

function useAdminActions({ type, name }) {
  const { setToastContent } = useContext(ToastContext);
  const { service, roles } = entityData[type];

  const handleError = useCallback(
    (err) => {
      logger.error(err);
      setToastContent(err?.data?.message || 'Error.', 'error');
    },
    [setToastContent],
  );
  const handleSuccess = useCallback(
    (msg) => setToastContent(msg, 'success'),
    [setToastContent],
  );

  const removeMember = useCallback(
    (slug, { onSuccess, onStart, onFinish, onError } = {}) =>
      async () => {
        onStart?.();
        try {
          await service.removeMember(name, slug);
          handleSuccess(`${slug} has been removed from ${name}.`);
          onSuccess?.({ slug, _action: 'delete' });
        } catch (error) {
          handleError(error);
          onError?.(error);
        } finally {
          onFinish?.();
        }
      },
    [service, name, handleSuccess, handleError],
  );

  const removeMembers = useCallback(
    (slugs, { onSuccess, onStart, onFinish, onError } = {}) =>
      async () => {
        onStart?.();
        try {
          await service.removeMembers(name, slugs);
          handleSuccess(`Selected members have been removed from ${name}.`);
          onSuccess?.();
        } catch (error) {
          handleError(error);
          onError?.();
        } finally {
          onFinish?.();
        }
      },
    [service, name, handleSuccess, handleError],
  );

  const changeMemberRole = useCallback(
    (slug, role, { onSuccess, onStart, onFinish, onError } = {}) =>
      async () => {
        onStart?.();
        try {
          await service.changeMemberRole(name, slug, role);
          handleSuccess(
            `${slug}'s role has been changed to ${roles.get(role)}.`,
          );
          onSuccess?.({
            slug,
            [memberRoleFields[type]]: role,
          });
        } catch (error) {
          handleError(error);
          onError?.(error);
        } finally {
          onFinish?.();
        }
      },
    [service, name, handleSuccess, handleError, roles, type],
  );

  const changeMembersRoles = useCallback(
    (slugs, newRole, { onSuccess, onStart, onFinish, onError } = {}) =>
      async () => {
        onStart?.();
        try {
          await service.changeMembersRoles(name, slugs, newRole);
          handleSuccess(`Roles have been changed to ${roles.get(newRole)}.`);
          onSuccess?.();
        } catch (error) {
          handleError(error);
          onError?.();
        } finally {
          onFinish?.();
        }
      },
    [service, name, handleSuccess, handleError, roles],
  );

  const editData = useCallback(
    async (
      data,
      { presentImageKeys = [] },
      { onSuccess, onStart, onFinish, onError } = {},
    ) => {
      onStart?.();
      try {
        await service.editData(
          name,
          Object.fromEntries(Object.entries(data).filter(([, v]) => v != null)),
          {
            presentImageKeys: presentImageKeys.filter(
              (key) => typeof data[key] === 'object',
            ),
          },
        );
        handleSuccess(`Data of ${name} changed successfully.`);
        onSuccess?.();
      } catch (error) {
        handleError(error);
        onError?.(error);
      } finally {
        onFinish?.();
      }
    },
    [service, name, handleSuccess, handleError],
  );

  const manageMembership = useCallback(
    (
      applicationIds,
      accepted,
      { onSuccess, onStart, onFinish, onError } = {},
    ) =>
      async () => {
        onStart?.();
        try {
          const isMultiple = Array.isArray(applicationIds);
          await service[isMultiple ? 'manageMemberships' : 'manageMembership'](
            name,
            applicationIds,
            accepted,
          );
          const prefix = isMultiple ? 'Applications' : 'Application';
          handleSuccess(
            accepted ? `${prefix} accepted.` : `${prefix} rejected.`,
          );
          onSuccess?.({
            applicationId: applicationIds,
            _action: 'delete',
          });
        } catch (error) {
          handleError(error);
          onError?.();
        } finally {
          onFinish?.();
        }
      },
    [service, name, handleSuccess, handleError],
  );

  return {
    members: useMemo(
      () => ({
        removeMember,
        changeMemberRole,
        removeMembers,
        changeMembersRoles,
      }),
      [removeMember, changeMemberRole, removeMembers, changeMembersRoles],
    ),
    info: useMemo(() => ({ editData }), [editData]),
    applications: useMemo(() => ({ manageMembership }), [manageMembership]),
  };
}

export default useAdminActions;
