import { useQuery } from '@tanstack/react-query';
import BoardService from 'api/service/board';
import type { Mandate } from 'utils/types';

export function useBoard(mandate?: Mandate) {
  return useQuery({
    queryKey: ['board', mandate],
    queryFn: () => BoardService.getBoard({ mandate }),
  });
}
