import PropTypes from 'prop-types';
import Button from 'components/Button/Button';

/**
 __ConfirmationModal__ is an internal component inside the hook
 It is called automatically if the hook specifies that there should be a confirmation before an action is submitted.
*/
export function ConfirmationModal({ onConfirm, onCancel, children }) {
  return (
    <div className="confirmation-modal">
      {children}
      <Button onClick={onCancel} className="secondary-button">
        Cancel
      </Button>
      <Button onClick={onConfirm} className="primary-button">
        Proceed
      </Button>
    </div>
  );
}

ConfirmationModal.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  children: PropTypes.node.isRequired,
};
