export const copyrightsText = (
  <>
    <p>Make sure that you have rights for the picture you are uploading.</p>
    <p>
      By uploading a picture you acknowledge that in case of any legal issues
      which your picture is a subject of, your branch will be the responsible
      entity.
    </p>
  </>
);
