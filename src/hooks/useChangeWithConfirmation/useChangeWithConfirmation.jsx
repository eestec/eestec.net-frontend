import { useContext, useCallback } from 'react';
import ModalContext from 'context/ModalContext';

import { ConfirmationModal } from './components/ConfirmationModal';

export default function useChangeWithConfirmation({
  enabled = true,
  onConfirm = () => {},
  onCancel = () => {},
  textContent,
}) {
  const { setModalContent } = useContext(ModalContext);

  const handleChange = useCallback(
    (arg) => {
      if (!enabled) {
        return onConfirm(arg);
      }

      setModalContent(
        <ConfirmationModal
          onConfirm={() => {
            setModalContent(null);
            onConfirm(arg);
          }}
          onCancel={() => {
            setModalContent(null);
            onCancel(arg);
          }}
        >
          {textContent}
        </ConfirmationModal>,
      );
    },
    [textContent, setModalContent, enabled, onConfirm, onCancel],
  );

  return handleChange;
}
