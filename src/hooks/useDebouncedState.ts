import React, { useState, useEffect } from 'react';

export default function useDebouncedState<T>(
  initialValue: T,
  debounceMs = 500,
) {
  const [value, setValue] = useState(initialValue);
  const [debouncedValue, setDebouncedValue] = useState(initialValue);

  useEffect(() => {
    const timeout = setTimeout(() => setDebouncedValue(value), debounceMs);

    return () => {
      clearTimeout(timeout);
    };
  }, [setDebouncedValue, value, debounceMs]);

  return [value, debouncedValue, setValue] as ReturnType<T>;
}

type ReturnType<T> = [T, T, React.Dispatch<React.SetStateAction<T>>];
