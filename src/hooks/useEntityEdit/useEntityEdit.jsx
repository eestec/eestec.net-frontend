import { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { setAllKeysToValue } from 'utils/misc';
import ToastContext from 'context/ToastContext';
import logger from 'utils/logger';

/**
 __useEntityEdit__ provides an interface for editing entity pages (such as Event, Branch,
 Profile and others in the future). It handles the logic of enabling editing, loading
 and data submission of different entity elements separately.
 The hook stores, updates and returns the form data of the edited entity.
*/
function useEntityEdit({
  apiServiceCall,
  data,
  setData,
  onSuccess,
  isResponseWrappedWithDataKey = false,
}) {
  const { setToastContent } = useContext(ToastContext);

  /*
    Below states will store objects with keys of the entity formData and the
    status of editing and loading. Set to null in the beginning in order
    to make it easy to skip the execution of useEffect of this custom hook on mount.
  */
  const [formData, setFormData] = useState(null);
  const [isBeingEdited, setIsBeingEdited] = useState(null);
  const [isEntityEditLoading, setIsEntityEditLoading] = useState(null);

  // The actual submission of the data
  const onEntityEditSubmit = useCallback(
    ({ keys, pictureValue = null, apiOptions }) => {
      const newStateData = {};
      if (pictureValue) {
        /*
        We edit only one picture at the same time, so we use keys[0]
        to just get the first and only element of the keys array
      */
        newStateData[keys[0]] = pictureValue;
        // Set the loading status of the picture key to show it in the UI
        setIsBeingEdited((prevState) => ({
          ...prevState,
          [keys[0]]: !prevState[keys[0]],
        }));
      } else {
        keys.forEach((key) => {
          // Set to the value from the form and replace multiple newlines with one
          newStateData[key] = formData[key].replace(/\n\s*\n/g, '\n');
        });
      }

      // Set the loading status of the desired keys to show it in the UI
      setIsEntityEditLoading((prevState) => {
        const newEntityEditLoadingState = { ...prevState };
        if (pictureValue) {
          newEntityEditLoadingState[keys[0]] = true;
        } else {
          keys.forEach((key) => {
            // Alter the new state containing loading information to true for edited key
            newEntityEditLoadingState[key] = true;
          });
        }
        return newEntityEditLoadingState;
      });

      // Call the API service
      apiServiceCall(newStateData, apiOptions)
        .then((response) => {
          // Spread old data and then new in order to make sure no data is lost
          const responseData = {
            ...data,
            ...(isResponseWrappedWithDataKey ? response.data : response),
          };
          const toastContent = ['The update was successful.', 'success', 6000];
          if (keys.includes('email') && data.email !== newStateData.email) {
            // Set a longer toast duration if the information is longer
            toastContent[2] = 12000;
            // Set the toast message to include the info about verifying the new email address.
            toastContent[0] += ` A verification link was sent to ${newStateData.email} for you to confirm your new email address. Until you do it, your account will be considered unverified.`;
          }
          setData(responseData);
          setFormData(responseData);
          setToastContent(...toastContent);
          // Execute onSuccess if such was provided
          if (onSuccess) {
            onSuccess(responseData);
          }
        })
        .catch((err) => {
          logger.log(err);
          let errorMessage = err?.data?.message;
          if (!errorMessage) {
            errorMessage =
              'Unfortunately, we encountered an unidentified error while updating your data.';
            if (pictureValue) {
              errorMessage +=
                '  Try reducing the filesize of the image you were trying to upload.';
            }
          }
          setToastContent(errorMessage, 'error');
          // Set form data to the original one if there was a failure
          setFormData(data);
        })
        .finally(() => {
          // Whatever happens, set edit and loading statuses back to false
          setIsEntityEditLoading((prevState) => {
            const newState = { ...prevState };
            keys.forEach((key) => {
              newState[key] = false;
            });
            return newState;
          });
          setIsBeingEdited((prevState) => {
            const newState = { ...prevState };
            keys.forEach((key) => {
              newState[key] = false;
            });
            return newState;
          });
        });
    },
    [
      apiServiceCall,
      data,
      setData,
      setIsEntityEditLoading,
      setIsBeingEdited,
      setFormData,
      formData,
      setToastContent,
      onSuccess,
      isResponseWrappedWithDataKey,
    ],
  );

  /*
    When the data property of the custom hook is not empty set the initial values
    of the hook to reflect the proper keys. This cannot be done on mount since sometimes
    the data property is null initially and stops being null after being fetched from BE.

    This may not be the best solution to this situation as it causes a redundant re-render
    but I couldn't find anything better that would be both simple and working as expected.
  */
  useEffect(() => {
    if (data && !isBeingEdited && !isEntityEditLoading) {
      setFormData(data);
      setIsBeingEdited(setAllKeysToValue(data, false));
      setIsEntityEditLoading(setAllKeysToValue(data, false));
    }
  }, [
    data,
    isBeingEdited,
    isEntityEditLoading,
    setFormData,
    setIsBeingEdited,
    setIsEntityEditLoading,
  ]);

  // A boolean value that indicates whether editing is disabled due to loading
  const isEditingDisabled = useMemo(
    () =>
      isEntityEditLoading !== null &&
      Object.values(isEntityEditLoading).some((value) => value),
    [isEntityEditLoading],
  );

  // A boolean value that indicates whether any of the fields is in an editing state
  const isAnyBeingEdited = useMemo(
    () =>
      isBeingEdited !== null &&
      Object.values(isBeingEdited).some((value) => value),
    [isBeingEdited],
  );

  const resetFormData = () => {
    setFormData(data);
    setIsBeingEdited(setAllKeysToValue(data, false));
    setIsEntityEditLoading(setAllKeysToValue(data, false));
  };

  return {
    // Simplify setting the form data to update only the keys we want
    setEntityEditFormData: (key, value) =>
      setFormData((prevState) => ({
        ...prevState,
        [key]: value,
      })),
    // Simplify setting the isBeingEdited data to update only the keys we want
    toggleIsBeingEdited: (key) =>
      setIsBeingEdited((prevState) => ({
        ...prevState,
        [key]: !prevState[key],
      })),
    // Submit the alteration to BE
    onEntityEditSubmit,
    // Object containing formData keys and boolean values of loading
    isEntityEditLoading,
    // Object containing formData keys and boolean values of edit status
    isBeingEdited,
    // The current altered data, not yet submitted
    entityEditFormData: formData,
    // Indicates whether editing is disabled due to loading
    isEditingDisabled,
    // Indicates whether any of the fields is in an editing state
    isAnyBeingEdited,
    // Reset all to initial values
    resetEntityEditFormData: resetFormData,
  };
}

export default useEntityEdit;
