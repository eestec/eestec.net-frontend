import { useMemo } from 'react';
import { useInfiniteQuery } from '@tanstack/react-query';
import EventService from 'api/service/events';
import type { Event } from 'utils/types';

export default function useEvents<K extends 'past' | 'active'>({
  entityKey,
  entityValue,
  kind,
  filters,
  enabled = true,
}: UseEventsParams & { kind: K }) {
  const result = useInfiniteQuery<EventsData<K>>({
    queryKey: ['events', entityKey, entityValue, kind, filters],
    queryFn: ({ pageParam }) =>
      EventService.getEventsNew({
        pageParam,
        entityKey,
        entityValue,
        kind,
        filters,
      }),
    initialPageParam: null,
    getNextPageParam: (lastPage) => lastPage.links?.next || null,
    enabled,
  });

  const combinedData = useMemo(() => {
    if (kind === 'active') {
      const upcoming: Array<unknown> = [];
      const inProgress: Array<unknown> = [];
      const applications: Array<unknown> = [];
      result.data?.pages.forEach((event) => {
        const eventData = event.data as ActiveEventsData;
        upcoming.push(...eventData.upcoming);
        inProgress.push(...eventData.inProgress);
        applications.push(...eventData.applications);
      });

      return {
        upcoming,
        inProgress,
        applications,
      };
    }

    const combinedData: Array<unknown> = [];
    result.data?.pages.forEach((event) => {
      combinedData.push(...(event.data as PastEventsData));
    });

    return combinedData;
  }, [result.data, kind]);

  return {
    ...result,
    combinedData: combinedData as CombinedData<K>,
  };
}

type UseEventsParams = {
  entityKey?: string;
  entityValue?: string;
  kind?: EventKind;
  filters?: string;
  enabled?: boolean;
};

type Links = {
  first: string;
  last: string;
  prev: string | null;
  next: string | null;
};

type EventKind = 'past' | 'active';

type ActiveEventsData = {
  upcoming: Array<Event>;
  inProgress: Array<Event>;
  applications: Array<Event>;
};

type PastEventsData = Array<Event>;

type CombinedData<K extends EventKind> = K extends 'active'
  ? ActiveEventsData
  : PastEventsData;

type EventsData<K extends EventKind> = {
  data: CombinedData<K>;
  links: Links;
};
