import { useCallback, useState, useMemo, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import useNavigationListener from 'hooks/useNavigationListener';

/**
 __useFilterActions__ abstracts away the whole logic behind filter actions, such as converting
 filter objects into filter strings, handling search params based on filter clicks and based on
 user browser button navigations and much more. It returns the state with filters and a setter
 function for filters which also triggers the search params modifications.
 */
function useFilterActions({
  onFilterChange,
  navigationListener = {
    enable: true,
    retainableProperties: [],
  },
  mapFiltersToSearchParams,
  mapSearchParamsToFilters,
  filterPopulation,
  defaultParamsString = '',
}) {
  const [filters, setFilters] = useState();
  const [searchParams, setSearchParams] = useSearchParams();
  const [filterPopulationData, setFilterPopulationData] = useState(
    filterPopulation.type === 'array' ? [] : null,
  );

  const isFilterPopulationDataEmpty = useMemo(
    () =>
      filterPopulation.type === 'array'
        ? !filterPopulationData.length
        : !filterPopulationData,
    [filterPopulation.type, filterPopulationData],
  );

  const navigationListenerCallback = useCallback(
    ({ search }) => {
      if (navigationListener.enable && !isFilterPopulationDataEmpty) {
        // set filters to match the new search params whenever browser buttons are used
        setFilters((prevState) => {
          const newState = mapSearchParamsToFilters(
            filterPopulationData,
            new URLSearchParams(search),
          );
          navigationListener.retainableProperties.forEach((propertyName) => {
            newState[propertyName] = prevState[propertyName];
          });
          const newFilterString = mapFiltersToSearchParams(newState);
          onFilterChange(newFilterString.api.toString());
          return newState;
        });
      }
    },
    [
      filterPopulationData,
      setFilters,
      isFilterPopulationDataEmpty,
      mapSearchParamsToFilters,
      mapFiltersToSearchParams,
      navigationListener.retainableProperties,
      navigationListener.enable,
      onFilterChange,
    ],
  );
  useNavigationListener(navigationListenerCallback);

  useEffect(() => {
    if (isFilterPopulationDataEmpty) {
      const serviceFunctionReference = filterPopulation.serviceFunction;
      serviceFunctionReference()
        .then(({ data }) => {
          const newFilters = mapSearchParamsToFilters(data, searchParams);
          const newFilterString = mapFiltersToSearchParams(newFilters);
          setFilters(mapSearchParamsToFilters(data, searchParams));
          setFilterPopulationData(data);
          onFilterChange(newFilterString.api.toString());
        })
        .catch(filterPopulation.onError);
    }
  }, [
    filterPopulation.serviceFunction,
    filterPopulation.onError,
    isFilterPopulationDataEmpty,
    mapSearchParamsToFilters,
    mapFiltersToSearchParams,
    searchParams,
    setFilters,
    onFilterChange,
  ]);

  const setFiltersCallback = useCallback(
    (newFilters) => {
      setFilters(newFilters);
      const filterString = mapFiltersToSearchParams(newFilters);
      const currentParamsString = searchParams.toString();
      const newParamsString = filterString.searchParams.toString();
      const callback =
        newParamsString !== currentParamsString &&
        !(newParamsString === defaultParamsString && currentParamsString === '')
          ? () => setSearchParams(filterString.searchParams)
          : undefined;
      onFilterChange(filterString.api.toString(), callback);
    },
    [
      setFilters,
      onFilterChange,
      searchParams,
      mapFiltersToSearchParams,
      setSearchParams,
      defaultParamsString,
    ],
  );

  return [filters, setFiltersCallback];
}

export default useFilterActions;
