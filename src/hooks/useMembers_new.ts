import { useInfiniteQuery } from '@tanstack/react-query';
import MemberService, { type MemberEntityParams } from 'api/service/members';
import { getNextPageParam } from 'utils/api';
import type { BranchMemberKind, ApiQueryParams } from 'utils/types';

// FUTURE: This is a new implementation in place, not used yet. In the future replace useMembers with this
export default function useMembers({
  entityParams,
  kind,
  filters,
  sorting,
  enabled = true,
}: UseMembersParams) {
  return useInfiniteQuery({
    queryKey: ['members', entityParams, kind, filters, sorting],
    queryFn: ({ pageParam }) =>
      MemberService.getMembers(entityParams, {
        page: pageParam as number | undefined,
        sorting,
        kind,
        filters,
      }),
    initialPageParam: null,
    getNextPageParam,
    enabled,
  });
}

type UseMembersParams = {
  entityParams: MemberEntityParams;
  kind?: BranchMemberKind;
  filters?: ApiQueryParams['filters'];
  sorting?: ApiQueryParams['sorting'];
  enabled?: boolean;
};
