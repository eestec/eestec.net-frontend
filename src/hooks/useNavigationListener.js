import { useEffect } from 'react';

/**
 __useNavigationListener__ executes a function given as its argument whenever a user
 uses the browser's arrows to navigate between pages in the routing history.
 The function gets executed with a history location object.
*/
function useNavigationListener(callback) {
  useEffect(() => {
    window.onpopstate = (e) => {
      callback(e.target.window.location);
    };
  }, [callback]);
}

export default useNavigationListener;
