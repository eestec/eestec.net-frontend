import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import StyledEngineProvider from '@mui/material/StyledEngineProvider';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import reportWebVitals from './reportWebVitals';
import Routing from 'routes/RouteProvider';
import { AuthProvider } from 'context/AuthContext';
import { ToastProvider } from 'context/ToastContext';
import { AdminProvider } from 'context/AdminContext';

import 'utils/fonts';
import '@csstools/normalize.css';
import './index.css';

const queryClient = new QueryClient();

createRoot(document.getElementById('root')!).render(
  <StrictMode>
    <AuthProvider>
      <AdminProvider>
        <StyledEngineProvider injectFirst>
          <ToastProvider>
            <QueryClientProvider client={queryClient}>
              <Routing />
            </QueryClientProvider>
          </ToastProvider>
        </StyledEngineProvider>
      </AdminProvider>
    </AuthProvider>
  </StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
