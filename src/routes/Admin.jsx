import { useContext, useCallback, useState } from 'react';
import AdminContext from 'context/AdminContext';
import AuthContext from 'context/AuthContext';
import ToastContext from 'context/ToastContext';
import PropTypes from 'prop-types';
import { Navigate, useParams } from 'react-router-dom';
import AdminPanelLoader from 'views/admin/partials/AdminPanelLoader';

const loader = (
  <AdminPanelLoader>Loading the administration data...</AdminPanelLoader>
);

const availableEntities = ['cities', 'events']; // , 'teams']

export default function Admin({ Component }) {
  const { loggedIn } = useContext(AuthContext);
  const {
    isStale: isLoading,
    adminData,
    adminError,
    refetchAdminData,
  } = useContext(AdminContext);
  const { setToastContent } = useContext(ToastContext);
  const { entityName, entityType } = useParams();
  const [navigationProps, setNavigationProps] = useState();

  const navigateAndToast = useCallback(
    (error) => {
      if (!navigationProps) {
        setToastContent(error, 'error');
        setNavigationProps({ to: '/', replace: true });
      }
      return loader;
    },
    [setNavigationProps, navigationProps, setToastContent],
  );

  if (!loggedIn || (entityType && !availableEntities.includes(entityType))) {
    return <Navigate to="/404" replace />;
  }

  if (navigationProps) {
    return <Navigate {...navigationProps} />;
  }

  if (adminError) {
    return navigateAndToast(adminError);
  }
  if (isLoading) {
    return loader;
  }
  if (!adminData?.length) {
    return navigateAndToast('You are not an administrator.');
  }

  if (entityName && entityType) {
    const identifier = adminDataMapping[entityType];
    const foundEntity = adminData.entities[entityType].find(
      (elem) => elem[identifier] === entityName,
    );
    if (!foundEntity && !adminData.isSuper) {
      return navigateAndToast('You are not an administrator of this entity.');
    }
  }

  return (
    <Component adminData={adminData} refetchAdminData={refetchAdminData} />
  );
}

Admin.propTypes = {
  /**
   An element type being the view to be presented on the given route.
  */
  Component: PropTypes.elementType.isRequired,
};

const adminDataMapping = {
  cities: 'id',
};
