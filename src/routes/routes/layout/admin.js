import { lazy } from 'react';

const AdminPanel = lazy(() => import('views/admin/AdminPanel'));
const EntityAdminPanel = lazy(() => import('views/admin/EntityAdminPanel'));

export default [
  {
    path: '/admin',
    exact: true,
    Element: AdminPanel,
  },
  {
    path: '/:entityType/:entityName/settings',
    Element: EntityAdminPanel,
  },
];
