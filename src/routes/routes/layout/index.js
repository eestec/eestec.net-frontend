import common from './common';
import _private from './private';
import admin from './admin';
import MainLayout from 'layouts/MainLayout/MainLayout';

// export all the routes of the main layout (header+footer)
export default {
  root: {
    path: '/',
    Element: MainLayout,
  },
  routes: {
    common,
    private: _private,
    admin,
    public: [],
  },
};
