/*
 This file contains helper functions for the API services
 defined in `/api` project directory.
*/

import logger from './logger';

// eslint-disable-next-line import/prefer-default-export
export function getFormData(object) {
  const formData = new FormData();
  // Convert object for formData
  Object.keys(object).forEach((key) => formData.append(key, object[key]));
  return formData;
}

// applicationResponseMapper is the special mapper of the outcome of application endpoint
export const applicationResponseMapper = (data) =>
  data.map((applicationData) => ({
    ...applicationData.user,
    created_at: applicationData.created_at,
    applicationId: applicationData.id,
  }));

export const branchMembersResponseMapper = ({ data: response }) =>
  response.map((memberData) => {
    const { user, ...data } = memberData;
    return {
      ...user,
      pivot: data,
    };
  });

export const getNextPageParam = (lastPage) => {
  const nextPage = lastPage.links.next;
  if (!nextPage) {
    return undefined;
  }

  try {
    const url = new URL(nextPage);
    const searchParams = new URLSearchParams(url.search);
    const page = searchParams.get('page');

    return page ? parseInt(page, 10) : undefined;
  } catch (e) {
    logger.log(e);
    return undefined;
  }
};
