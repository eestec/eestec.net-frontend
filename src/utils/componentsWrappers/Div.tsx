import React, { forwardRef } from 'react';

const Div = forwardRef(function Div(props, ref: React.Ref<HTMLDivElement>) {
  return <div {...props} ref={ref} />;
});

export default Div;
