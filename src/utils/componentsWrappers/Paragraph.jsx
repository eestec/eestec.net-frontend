import { forwardRef } from 'react';

const Paragraph = forwardRef(function Paragraph(props, ref) {
  return <p {...props} ref={ref} />;
});

export default Paragraph;
