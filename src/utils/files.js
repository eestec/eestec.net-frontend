/*
 This file contains utility functions which handle files
*/

// Imports all files from r - useful for mass import of e.g. images
export function importAll(r) {
  return r.keys().map(r);
}

export function getFileName(path) {
  const pathArray = path.split('/');
  const actualName = pathArray[pathArray.length - 1].split('.')[0];
  return actualName.replaceAll('_', ' ');
}
