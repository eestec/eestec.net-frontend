import PropTypes from 'prop-types';

const gridType = PropTypes.oneOf([
  'applications',
  'cities',
  'citiesList',
  'board',
  'teams',
  'events',
  'all',
]);
const entities = PropTypes.shape({
  events: PropTypes.arrayOf(PropTypes.object),
  cities: PropTypes.arrayOf(PropTypes.object),
  teams: PropTypes.arrayOf(PropTypes.object),
});

export const admin = {
  entities,
  adminData: PropTypes.shape({
    isSuper: PropTypes.bool,
    entities,
    length: PropTypes.number,
  }),
  entityType: PropTypes.oneOf(['cities', 'board', 'teams', 'events', 'all'])
    .isRequired,
  gridType,
  adminActions: {
    board: PropTypes.shape({
      addBoardPosition: PropTypes.func,
      updateBoardPosition: PropTypes.func,
    }),
    cities: PropTypes.shape({
      addBranch: PropTypes.func,
      updateBranch: PropTypes.func,
    }),
  },
  toolbarActions: {
    callbacks: PropTypes.shape({
      onSuccess: PropTypes.func,
      onStart: PropTypes.func,
      onFinish: PropTypes.func,
      onError: PropTypes.func,
    }),
    selectedRows: PropTypes.arrayOf(PropTypes.object),
    entityName: PropTypes.string,
    gridType,
  },
};
