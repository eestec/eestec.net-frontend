import smooth from 'utils/scroll/smooth';

function animateScroll(
  scrollFunction: (initialPosition: number, endPosition: number) => boolean,
  targetPosition: number,
  initialPosition: number,
  duration: number,
  easing: string,
  ...params: number[]
) {
  let start: number;
  let position: number;
  let animationFrame: number;

  const amountOfPixelsToScroll = initialPosition - targetPosition;

  function step(timestamp: number) {
    if (start === undefined) {
      start = timestamp;
    }

    const elapsed = timestamp - start;

    // this just gives us a number between 0 (start) and 1 (end)
    const relativeProgress = elapsed / duration;

    const easedProgress = smooth[easing](relativeProgress, ...params);

    // calculate new position for every tick of the requestAnimationFrame
    position = initialPosition - amountOfPixelsToScroll * easedProgress;

    // set the scrollbar position
    const shouldStop = scrollFunction(0, position);

    // Stop when relative progress is done or when the scrollFunction says to
    if (shouldStop || relativeProgress >= 1) {
      window.cancelAnimationFrame(animationFrame);
      return;
    }

    // repeat until the end is reached
    if (elapsed < duration) {
      animationFrame = window.requestAnimationFrame(step);
    }
  }

  animationFrame = window.requestAnimationFrame(step);
}

/**
 * __animateDraggable__ is used for animation of the draggable interface (`Draggable` or `Carousel`)
 * @param targetPosition - the final position of the scroll
 * @param duration - the duration of the scroll
 * @param containerRef - a reference to the container that should be scrolled
 * @param conditionRef - if the value of this reference is true, the animation should stop
 * @param easing - the animation easing function to use
 */
export function animateDraggable(
  targetPosition: number,
  duration: number,
  containerRef: React.RefObject<HTMLElement>,
  conditionRef: React.MutableRefObject<boolean>,
  easing: string = 'easeInOutQuad',
) {
  animateScroll(
    (initialPosition: number, endPosition: number): boolean => {
      // do not animate scroll if ref not yet initialized, as it will result in a crash
      if (conditionRef.current || !containerRef.current) {
        return true;
      }
      // each animation frame should set scrollLeft to endPosition of a frame
      // eslint-disable-next-line no-param-reassign
      containerRef.current.scrollLeft = endPosition;
      /*
        Animating stops when if scrollFunction returns true.
        Return true if the conditionRef value is true (usually meaning that the user started
        scrolling on their own so the animation should stop.
      */
      return conditionRef.current;
    },
    targetPosition,
    containerRef.current?.scrollLeft || 0,
    duration,
    easing,
  );
}

export default animateScroll;
