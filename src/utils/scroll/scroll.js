import animateScroll from 'utils/scroll/animateScroll';

export function getElementPosition(element) {
  let elem = element;
  let location = 0;
  if (elem.offsetParent) {
    do {
      location += elem.offsetTop;
      elem = elem.offsetParent;
    } while (elem);
  }
  return location >= 0 ? location : 0;
}

/* Creates a smooth scrolling experience on click
 * The easing can be picked from the list at smooth.js
 */

function scroll(
  ref,
  inputInitialPosition,
  offset,
  duration = 1500,
  easing = 'linear',
  ...params
) {
  const initialPosition = inputInitialPosition || window.scrollY;
  const element = ref.current;
  if (!element) {
    return;
  }

  animateScroll(
    window.scrollTo,
    getElementPosition(element) - offset,
    initialPosition,
    duration,
    easing,
    ...params,
  );
}
export default scroll;
