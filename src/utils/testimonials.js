/*
 This file contains hardcoded testimonials information
 presented at https://eestec.net/about-us

 At some point it maybe (?) good to put that data in BE and fetch it.
*/

import testimonialImgAlex from 'assets/img/testimonials/alex.jpg';
import testimonialImgAndrijana from 'assets/img/testimonials/andrijana.jpg';
import testimonialImgDiana from 'assets/img/testimonials/diana.jpg';
import testimonialImgIgor from 'assets/img/testimonials/igor.jpg';
import testimonialImgOlga from 'assets/img/testimonials/olga.jpg';

const testimonials = [
  {
    id: 1,
    name: 'Alexander Geiger',
    position: 'ex VC-AA - LC Aachen',
    text:
      'EESTEC provided the opportunity for me to collaborate with ' +
      'many diverse individuals in different constellations. ' +
      'Those interactions are valuable reference points now in my ' +
      'personal and professional life and the friends I made along ' +
      'the way are very close to me. I can recommend joining EESTEC ' +
      'to every student in the fields in and around Electrical Engineering ' +
      'Whether you are interested in leadership, soft skills,' +
      'and Computer Science. new technologies or travel, you will find ' +
      'ways to follow your passions. Just reach out to any experienced ' +
      'EESTECer and they will guide you to the right opportunity for you. ',
    img: testimonialImgAlex,
  },
  {
    id: 2,
    name: 'Igor Zec',
    position: "SCOC'22 Head Organiser - LC Belgrade",
    text:
      'Without a shadow of a doubt, I can say that EESTEC changed my life. ' +
      "I've met lifelong friends, explored new cultures, and learned so much. " +
      "I've even changed career paths! The skills and experience I've " +
      'gained are priceless. There was always someone eager to share their ' +
      'knowledge with me and I will forever be grateful for all that EESTEC' +
      'has given me. Regardless, I still believe that my favourite thing about ' +
      'EESTEC is the fact that even when I found myself in a completely ' +
      'unfamiliar place and surrounded by people I met only yesterday, ' +
      "if I had EESTECers with me, I've always felt like I was home. ",
    img: testimonialImgIgor,
  },
  {
    id: 3,
    name: 'Andrijana Nedeljković',
    position: 'ex Training Team Leader -  LC Novi Sad',
    text:
      'Joining EESTEC in 2016 transformed my life. From local roles to ' +
      'international positions, I grew immensely. As a Soft Skills Trainer, ' +
      'I traveled Europe, led sessions, and designed events. This experience ' +
      'helped me discover my strengths, weaknesses, and aspirations. As ' +
      'Training Team Leader, I managed a team of over 150 trainers and ' +
      "contributed to EESTEC's learning needs. EESTEC not only fueled my " +
      'personal and professional growth but also connected me with a lifelong ' +
      'community of like-minded individuals from across Europe. My EESTEC ' +
      'experience positively impacted my career as an IT project manager, ' +
      'allowing me to integrate my engineering knowledge with ' +
      'the interpersonal skills I gained from the Association. I highly ' +
      'recommend EESTEC to anyone seeking to learn, grow, connect, and ' +
      "explore. I'm confident it will be transformative for you too.",
    img: testimonialImgAndrijana,
  },
  {
    id: 4,
    name: 'Olga Paraskevopoulou',
    position: 'BATCAM -  LC Patras',
    text:
      'Since I started my EESTEC journey a little bit over a year ago, ' +
      'I have had many special experiences that I cherish. I have had the ' +
      'opportunity to engage with different kinds of subjects, discover my ' +
      'talents and weaknesses and work on improving my skills in a very ' +
      'supportive and friendly environment. From both the local and ' +
      'international level that I have been active to, everyone has ' +
      'something that can inspire me and the more I am open to being ' +
      'inspired the more I find myself wanting to experience and give ' +
      'back to the organization. Besides EESTEC being the opportunity ' +
      'for your self-development, connections, valuable friendships and ' +
      'travels, EESTEC is what you want it to be and has a special place ' +
      'in my heart for everything that has taught me and for every emotion ' +
      'that has made me feel. ',
    img: testimonialImgOlga,
  },
  {
    id: 5,
    name: 'Diana Radoi',
    position: 'Supervisory Board Member - LC Bucharest',
    text:
      'Being part of EESTEC was an incredible student experience. It allowed me ' +
      'to explore diverse areas, acquire knowledge, develop skills, and contribute ' +
      'to the community. EESTEC is a multicultural environment where connections with ' +
      'members from different countries thrive. Over six years, I held 30 responsibilities, ' +
      'both local and international. This safe space enabled me to try new things, ' +
      'seize opportunities, and discover my passions. EESTEC bridges student life and ' +
      "professional work, and the term “volunteer” doesn't fully capture the dedication " +
      'required. Last year, as my studies concluded, I have stepped up as a Supervisory Board ' +
      "member, overseeing the Association's activities and collaborating with the Board.",
    img: testimonialImgDiana,
  },
];

export default testimonials;
