export interface BoardMember {
  id: number;
  order: number;
  mandate_start: string;
  mandate_end: string;
  mandate: Mandate;
  position: BoardPosition;
  user: User;
}

export interface BoardPosition {
  id: number;
  name: string;
  description: string;
  created_at: string;
  updated_at: string;
}

export interface BranchMember {
  branch_id: number;
  user_id: number;
  branch_role_id: number;
  created_at: string;
  updated_at: string;
  user: User;
}

export interface BranchMemberApplication {
  id: number;
  user_id: number;
  created_at: string;
  user: User;
}

export interface User {
  first_name: string;
  last_name: string;
  slug: string;
  profile_photo_path: string | null;
}

export interface DetailedUser extends User {
  description: string;
  banner_path: string | null;
  email_verified_at?: string;
  branch_verified_at?: string;
  branches: Branch[];
}

export interface Branch {
  id: number;
  name: string;
  slug: string;
  email: string;
  founded_in: number;
  website: string;
  country: string;
  region: number;
  facebook: string;
  instagram: string | null;
  linkedin: string | null;
  address: string;
  description: string;
  profile_picture_path: string;
  large_profile_picture_path: string;
  branch_type: BranchType;
}

export interface Event {
  name: string;
  start_date: string;
  end_date: string;
  slug: string;
  application_deadline: string;
  max_participants: number;
  participation_fee: number;
  location: string | null;
  description: string;
  profile_picture: string | null;
  banner_path: string | null;
  event_type: EventType;
  branches: Branch[];
  entities: InternationalEntity[];
}

export type Mandate = `${number}/${number}`;

export type EntityParams<T extends string = string> = {
  key: T;
  value?: string;
};

export type BranchMemberKind = 'active' | 'alumni' | 'board';

export type ApiQueryParams<T = unknown> = {
  page?: number;
  filters?: {
    quickFilterValues: string[];
    items: {
      field: string;
      operator: string;
      value: string | undefined;
    }[];
  };
  sorting?: {
    field: string;
    sort: 'asc' | 'desc';
  }[];
} & T;

export type DataResponse<R> = {
  data: R;
};

export type PaginatedResponse<R> = DataResponse<R> & {
  links: PaginationLinks;
  meta: PaginationMeta;
};

export type EventType = EntityType;
export type BranchType = EntityType;

interface EntityType {
  id: number;
  name: string;
}

interface InternationalEntity {}

interface PaginationLinks {
  first: string;
  last: string;
  prev: string | null;
  next: string | null;
}

interface PaginationMeta {
  current_page: number;
  from: number;
  last_page: number;
  links: {
    url?: string;
    label: string;
    active: boolean;
  }[];
  path: string;
  per_page: number;
  to: number;
  total: number;
}
