import { lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { Navigate } from 'react-router-dom';
import useDocumentTitle from 'hooks/useDocumentTitle';
import Heading from 'components/Heading/Heading';
import { admin as adminPropTypes } from 'utils/prop-types';
import AdminPanelError from '../partials/AdminPanelError';
import AdminPanelLoader from '../partials/AdminPanelLoader';
const AdminEntitySelector = lazy(
  () => import('../partials/AdminEntitySelector'),
);
const SuperAdminDashboard = lazy(
  () => import('../partials/SuperAdminDashboard'),
);

import './AdminPanel.css';

function AdminPanel({ adminData, refetchAdminData }) {
  useDocumentTitle('Administration');

  // render SuperAdminDashboard if its the head administrator (so devs, SB or BoA)
  if (adminData.isSuper) {
    return (
      <Suspense fallback={<AdminPanelLoader />}>
        <SuperAdminDashboard
          entities={adminData.entities}
          refetchAdminData={refetchAdminData}
        />
      </Suspense>
    );
  }

  // if a person is an admin of only one entity, redirect them there
  if (adminData.length === 1) {
    for (const [key, value] of Object.entries(adminData.entities)) {
      if (value?.length) {
        return <Navigate replace to={`/${key}/${value[0].id}/settings`} />;
      }
    }
    return (
      <AdminPanelError>
        An internal error happened. Try refreshing.
      </AdminPanelError>
    );
  }

  // if there are more entities but a person is not a super admin, let them choose what to manage
  return (
    <>
      <Heading variant="h1" lineAfter>
        Administration Dashboard
      </Heading>
      <Suspense fallback={<AdminPanelLoader />}>
        <AdminEntitySelector entities={adminData.entities} />
      </Suspense>
    </>
  );
}

export default function WrappedAdminPanel(props) {
  return (
    <section className="admin-panel-wrapped">
      <AdminPanel {...props} />
    </section>
  );
}

AdminPanel.propTypes = {
  adminData: adminPropTypes.adminData,
  refetchAdminData: PropTypes.func,
};
