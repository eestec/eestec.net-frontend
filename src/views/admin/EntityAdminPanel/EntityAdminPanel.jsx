import { useCallback } from 'react';
import { Link, useLocation, useParams } from 'react-router-dom';
import Button from '@mui/material/Button';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Heading from 'components/Heading/Heading';
import useAdminActions from 'hooks/useAdminActions';
import { capitalizeFirstLetter } from 'utils/misc';
import { admin as adminPropTypes } from 'utils/prop-types';
import useDocumentTitle from 'hooks/useDocumentTitle';
import AdminDataGrid from '../partials/AdminDataGrid';
import AdminInfoEditor from '../partials/AdminInfoEditor';

import './EntityAdminPanel.css';

export default function EntityAdminPanel({ adminData }) {
  const { state } = useLocation();
  const shouldGoBack = state?.from === 'adminPanel';

  const { length } = adminData;
  const { entityType, entityName } = useParams();
  useDocumentTitle(
    `Administration - ${capitalizeFirstLetter(entityName.replaceAll('-', ' '))}`,
  );
  const { members, info, applications } = useAdminActions({
    type: entityType,
    name: entityName,
  });
  const changeMemberRole = members.changeMemberRole;

  const onInlineMemberStatusChange = useCallback(
    ({ data, identifier }) =>
      changeMemberRole(identifier, data?.branch_role_id, {
        onError: (err) => {
          throw err;
        },
      })(),
    [changeMemberRole],
  );

  const applicationsUpdateEvent = `${entityType}ApplicationsUpdated`;

  return (
    <section className="admin-panel">
      {length > 1 && (
        <Button
          to={shouldGoBack ? -1 : '/admin'}
          component={Link}
          startIcon={<ArrowBackIcon />}
          variant="text"
          disableRipple
          className="admin-panel-back-link"
        >
          Go to the admin dashboard
        </Button>
      )}
      <Heading variant="h1" lineAfter>
        Administration Panel -{' '}
        <span className="capitalized">{entityName.replaceAll('-', ' ')}</span>
      </Heading>
      <Heading variant="h2">Manage information</Heading>
      <AdminInfoEditor
        entityType={entityType}
        entityName={entityName}
        adminActions={info}
      />
      <Heading variant="h2">Manage applications</Heading>
      <AdminDataGrid
        gridType="applications"
        entitySettingsKey="applications"
        entityType={entityType}
        entityName={entityName}
        adminActions={applications}
        onUpdateTriggerEventName={applicationsUpdateEvent}
      />
      <Heading variant="h2">Manage members</Heading>
      <AdminDataGrid
        entityType={entityType}
        entityName={entityName}
        adminActions={members}
        onUpdateListenerEventName={applicationsUpdateEvent}
        onInlineEdit={onInlineMemberStatusChange}
      />
    </section>
  );
}
EntityAdminPanel.propTypes = {
  adminData: adminPropTypes.adminData,
};
