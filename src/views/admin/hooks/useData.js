import {
  useState,
  useEffect,
  useCallback,
  useRef,
  useMemo,
  useContext,
} from 'react';
import { useGridApiRef } from '@mui/x-data-grid';
import ToastContext from 'context/ToastContext';
import logger from 'utils/logger';
import { getObjectDelta } from 'utils/misc';
import { getEntityServiceSettings } from '../utils/services';
import Pagination from '../partials/Pagination';

const PAGE_SIZE = 12;
const INITIAL_FILTER_MODEL = {
  items: [],
  logicOperator: 'and',
  quickFilterLogicOperator: 'and',
  quickFilterValues: [],
};
const DEFAULT_EMPTY_PROPS_STATE = {};

function usePagination(enabled) {
  const [pagination, setPagination] = useState({
    page: 0,
    pageSize: PAGE_SIZE,
  });
  const [totalRowCount, setTotalRowCount] = useState(0);

  const paginationProps = enabled
    ? {
        keepNonExistentRowsSelected: true,
        rowCount: totalRowCount,
        onPaginationModelChange: setPagination,
        paginationModel: pagination,
        pageSizeOptions: [PAGE_SIZE],
        paginationMode: 'server',
      }
    : DEFAULT_EMPTY_PROPS_STATE;

  return {
    page: pagination.page,
    paginationProps,
    setTotalRowCount,
    setPagination,
  };
}

function useSorting(enabled) {
  const [sorting, setSorting] = useState([]);

  const sortingProps = enabled
    ? {
        sortingMode: 'server',
        sortModel: sorting,
        onSortModelChange: setSorting,
      }
    : DEFAULT_EMPTY_PROPS_STATE;

  return {
    sorting,
    sortingProps,
  };
}

function useFiltering(enabled) {
  const [gridFilters, setGridFilters] = useState(INITIAL_FILTER_MODEL);

  const filterProps = enabled
    ? {
        filterMode: 'server',
        filterModel: gridFilters,
      }
    : DEFAULT_EMPTY_PROPS_STATE;

  return {
    filterProps,
    filters: gridFilters,
    setFilters: setGridFilters,
  };
}

export default function useData({
  type,
  name,
  serviceKey,
  rowIdentifier,
  onInlineEdit,
  formatRow,
  serverSideDataHandling,
  allowSelection,
  onUpdateListenerEventName,
  onUpdateTriggerEventName,
  refetchRowsOnInlineEditSuccessKeys,
  refetchAllOnRowEdit,
  shouldProceedWithEdit,
  extraParams,
}) {
  const { setToastContent } = useContext(ToastContext);
  const { serviceFn, formatFn } = getEntityServiceSettings(type, serviceKey);
  const [rows, setRows] = useState([]);
  const [currentlyEditedCell, setCurrentlyEditedCell] = useState();
  const [rowMap, setRowMap] = useState({});
  const [selectedRowMap, setSelectedRowMap] = useState({});
  const [loading, setLoading] = useState(false);

  const [error, setError] = useState();
  const [rowSelectionModel, setRowSelectionModel] = useState([]);
  const apiRef = useGridApiRef();
  const {
    stopCellEditMode,
    updateRows,
    getSelectedRows,
    setRowSelectionModel: apiSetRowSelectionModel,
  } = apiRef.current || {};

  const abortController = useRef(new AbortController());

  const { sorting, sortingProps } = useSorting(serverSideDataHandling);
  const { page, paginationProps, setTotalRowCount, setPagination } =
    usePagination(serverSideDataHandling);
  const { filters, filterProps, setFilters } = useFiltering(
    serverSideDataHandling,
  );

  const onFilterModelChange = useCallback(
    (newFilters) => {
      setFilters(newFilters);
      setPagination((oldPagination) =>
        oldPagination.page === 0
          ? oldPagination
          : { ...oldPagination, page: 0 },
      );
    },
    [setFilters, setPagination],
  );

  const handleSelection = useCallback(
    // TODO: Maybe this robust function and everything regarding rowSelectionModel could be greatly reduced if we take advantage more of the apiRef
    (newIds) => {
      if (!newIds.length) {
        setSelectedRowMap({});
        setRowSelectionModel([]);
        return;
      }

      let newRowsForSelectedRowMap = {};
      const newRowSelectionModel = newIds.reduce((acc, id, curIdx) => {
        const rowMapValue = rowMap[id];
        const selectedRowMapValue = selectedRowMap[id];
        if (rowMapValue && !selectedRowMapValue) {
          newRowsForSelectedRowMap[id] = rowMapValue;
        }
        acc[curIdx] = rowMapValue || selectedRowMapValue;
        return acc;
      }, Array(newIds.length));

      setRowSelectionModel(newRowSelectionModel);
      setSelectedRowMap((prevState) => {
        const prevStateKeys = Object.keys(prevState);
        const newSelectedKeys = Object.keys(newRowsForSelectedRowMap);

        const shouldRemoveKeys = newIds.length < prevStateKeys.length;
        const shouldAddKeys = !!newSelectedKeys.length;
        if (!shouldAddKeys && !shouldRemoveKeys) {
          return prevState;
        }

        let newState = { ...prevState };

        if (shouldRemoveKeys) {
          const idsSet = new Set(newIds);
          newState = prevStateKeys.reduce((modifiedState, curPrevKey) => {
            if (idsSet.has(curPrevKey)) {
              modifiedState[curPrevKey] = prevState[curPrevKey];
            }
            return modifiedState;
          }, {});
        }

        if (shouldAddKeys) {
          newSelectedKeys.forEach((newKey) => {
            newState[newKey] = newRowsForSelectedRowMap[newKey];
          });
        }

        return newState;
      });
    },
    [rowMap, selectedRowMap, setSelectedRowMap, setRowSelectionModel],
  );

  const fetchRows = useCallback(async () => {
    abortController.current.abort();
    abortController.current = new AbortController();
    setLoading(true);

    try {
      const { data: _res, meta } = await serviceFn(
        type,
        name,
        {
          page,
          filters,
          sorting,
          ...extraParams,
        },
        abortController.current.signal,
      );
      const _totalRowCount = meta?.total;

      const res = formatFn ? formatFn(_res) : _res;
      const [newRows, newRowMap] = res.reduce(
        (acc, row, idx) => {
          const formattedRow = formatRow(row);
          acc[0][idx] = formattedRow;
          acc[1][formattedRow[rowIdentifier]] = formattedRow;
          return acc;
        },
        [Array(res.length), {}],
      );

      setRows(newRows);
      setRowMap(newRowMap);
      setError(null);
      setTotalRowCount(_totalRowCount);
    } catch (err) {
      if (err?.message === 'canceled') return;
      logger.log(err);
      setError(err);
    } finally {
      setLoading(false);
    }
  }, [
    filters,
    sorting,
    page,
    formatFn,
    serviceFn,
    extraParams,
    formatRow,
    name,
    rowIdentifier,
    setTotalRowCount,
    type,
  ]);

  useEffect(() => {
    fetchRows();
    return () => {
      abortController.current.abort();
    };
  }, [fetchRows]);

  useEffect(() => {
    if (onUpdateListenerEventName) {
      document.addEventListener(onUpdateListenerEventName, fetchRows, {
        passive: true,
      });
    }
    return () => {
      if (onUpdateListenerEventName) {
        document.removeEventListener(onUpdateListenerEventName, fetchRows);
      }
    };
  }, [fetchRows, onUpdateListenerEventName]);

  const refetchAllRowEditCallback = refetchAllOnRowEdit ? fetchRows : undefined;

  const actionCallbacks = useMemo(() => {
    return {
      onSuccess: (modifiedRow) => {
        if (refetchAllRowEditCallback) {
          refetchAllRowEditCallback();
        } else {
          updateRows(Array.isArray(modifiedRow) ? modifiedRow : [modifiedRow]);
          if (modifiedRow?._action === 'delete') {
            const selectedRows = getSelectedRows();
            if (selectedRows.size > 0) {
              apiSetRowSelectionModel(
                Array.from(selectedRows.entries())
                  .filter(([, value]) => value !== undefined)
                  .map(([key]) => key),
              );
            }
            setTotalRowCount((prevState) => prevState - 1);
          }
        }
        if (onUpdateTriggerEventName) {
          document.dispatchEvent(new CustomEvent(onUpdateTriggerEventName));
        }
      },
      onStart: () => setLoading(true),
      onFinish: () => setLoading(false),
    };
  }, [
    updateRows,
    setLoading,
    apiSetRowSelectionModel,
    refetchAllRowEditCallback,
    setTotalRowCount,
    getSelectedRows,
    onUpdateTriggerEventName,
  ]);

  const batchActionCallbacks = useMemo(
    () => ({
      onSuccess: () => {
        apiSetRowSelectionModel([]);
        fetchRows();
        if (onUpdateTriggerEventName) {
          document.dispatchEvent(new CustomEvent(onUpdateTriggerEventName));
        }
      },
      onStart: () => setLoading(true),
      onFinish: () => setLoading(false),
    }),
    [setLoading, fetchRows, apiSetRowSelectionModel, onUpdateTriggerEventName],
  );

  const onInlineEditCallback =
    refetchRowsOnInlineEditSuccessKeys?.length > 0 ? fetchRows : undefined;

  const processRowUpdate = useCallback(
    async (updatedData, originalData) => {
      const delta = getObjectDelta(originalData, updatedData);
      if (
        !Object.keys(delta).length ||
        shouldProceedWithEdit?.(delta) === false
      ) {
        return originalData;
      }
      setLoading(true);
      const identifier = updatedData[rowIdentifier];
      await onInlineEdit?.({ identifier, data: delta });
      setLoading(false);
      setCurrentlyEditedCell(null);
      if (!Array.isArray(refetchRowsOnInlineEditSuccessKeys)) {
        return updatedData;
      }

      const deltaKeys = Object.keys(delta);
      if (
        refetchRowsOnInlineEditSuccessKeys.some((key) =>
          deltaKeys.includes(key),
        )
      ) {
        onInlineEditCallback?.();
        // return originalData as it will be uploaded by the fetch - this may be confusing but is quite a clever way to give server time
        // to process more complicated changes for the whole table after some unusual data change and prevent table from flickering with various data
        return originalData;
      }
      return updatedData;
    },
    [
      onInlineEdit,
      onInlineEditCallback,
      refetchRowsOnInlineEditSuccessKeys,
      rowIdentifier,
      shouldProceedWithEdit,
    ],
  );

  const onProcessRowUpdateError = useCallback(
    (err) => {
      setLoading(false);
      setToastContent(err?.data?.message || err?.message || err, 'error');
      stopCellEditMode({ ...currentlyEditedCell, ignoreModifications: true });
      setCurrentlyEditedCell(null);
    },
    [setToastContent, stopCellEditMode, currentlyEditedCell],
  );

  const onCellEditStart = useCallback(
    ({ id, field }) => setCurrentlyEditedCell({ id, field }),
    [setCurrentlyEditedCell],
  );

  return {
    slots: serverSideDataHandling
      ? {
          pagination: Pagination,
        }
      : {},
    error,
    refetch: fetchRows,
    rowSelectionModel,
    dataUpdateRef: apiRef,
    actionCallbacks,
    batchActionCallbacks,
    dataProps: {
      onRowSelectionModelChange: allowSelection ? handleSelection : undefined,
      rows,
      loading,
      processRowUpdate,
      onProcessRowUpdateError,
      onCellEditStart,
      ...paginationProps,
      ...filterProps,
      onFilterModelChange,
      ...sortingProps,
    },
  };
}
