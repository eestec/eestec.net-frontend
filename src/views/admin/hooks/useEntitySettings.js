import { useMemo } from 'react';
import {
  formatRow,
  rowIdMapping,
  getBaseColumns,
  dataGridProps,
} from '../utils/datagrid';
import { getEntityActions } from '../utils/actions';

function useEntitySettings(gridType) {
  return useMemo(() => {
    const rowIdentifier = rowIdMapping[gridType];

    return {
      rowIdentifier,
      formatRow: formatRow[gridType] ?? ((v) => v),
      getEntityActions: getEntityActions[gridType],
      getRowId: (row) => row[rowIdentifier],
      getBaseColumns: getBaseColumns[gridType],
      dataGridProps: dataGridProps[gridType] ?? {},
    };
  }, [gridType]);
}

export default useEntitySettings;
