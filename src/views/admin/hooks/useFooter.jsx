function useFooter(enabled) {
  return {
    slots: !enabled ? 'div' : undefined,
  };
}

export default useFooter;
