import { useContext, useCallback, useMemo } from 'react';
import ToastContext from 'context/ToastContext';
import BranchService from 'api/service/branches';
import BoardService from 'api/service/board';
import logger from 'utils/logger';

function useBoardSuperAdminActions({ handleError, handleSuccess }) {
  const addBoardPosition = useCallback(
    async ({ position }, { onSuccess, onStart, onFinish, onError } = {}) => {
      onStart?.();
      try {
        await BoardService.addBoardPosition(position);
        handleSuccess(`${position} has been added.`);
        onSuccess?.({ position });
      } catch (error) {
        handleError(error);
        onError?.(error);
      } finally {
        onFinish?.();
      }
    },
    [handleSuccess, handleError],
  );

  const removeBoardMember = useCallback(
    (position, { onSuccess, onStart, onFinish, onError } = {}) =>
      async () => {
        if (position < 0) {
          return;
        }
        onStart?.();
        try {
          await BoardService.removeBoardMember(position);
          handleSuccess('Board member has been removed');
          onSuccess?.({ position, _action: 'delete' });
        } catch (error) {
          handleError(error);
          onError?.(error);
        } finally {
          onFinish?.();
        }
      },
    [handleSuccess, handleError],
  );

  const updateBoardPosition = useCallback(
    async (
      position,
      data,
      { chosenMandate },
      { onSuccess, onStart, onFinish, onError } = {},
    ) => {
      onStart?.();
      try {
        if ('name' in data) {
          const userId = data.name.id;
          const name = `${data.name.first_name} ${data.name.last_name}`;
          if (position < 0) {
            await BoardService.addBoardMember({
              mandate: chosenMandate?.value,
              boardPositionId: data.name?._row?.position?.id,
              userId,
            });
          } else {
            await BoardService.updateBoardMember(position, {
              userId,
            });
          }
          handleSuccess(`${name}'s position has been updated.`);
          onSuccess?.({ position, ...data });
        }
        if ('description' in data) {
          await BoardService.updateBoardPosition(data.description.id, {
            description: data.description.description,
          });
          handleSuccess(
            `${data.description.name}'s description has been updated.`,
          );
          onSuccess?.({ position, ...data });
        }
      } catch (error) {
        handleError(error);
        onError?.(error);
      } finally {
        onFinish?.();
      }
    },
    [handleSuccess, handleError],
  );

  return useMemo(
    () => ({
      updateBoardPosition,
      removeBoardMember,
      addBoardPosition,
    }),
    [updateBoardPosition, removeBoardMember, addBoardPosition],
  );
}

function useBranchesSuperAdminActions({ handleError, handleSuccess }) {
  const addBranch = useCallback(
    async (data, { onSuccess, onStart, onFinish, onError } = {}) => {
      onStart?.();
      try {
        const slug = await BranchService.addBranch(data);
        handleSuccess(`${data.name} has been added.`);
        onSuccess?.({ ...data, slug });
      } catch (error) {
        handleError(error);
        onError?.(error);
      } finally {
        onFinish?.();
      }
    },
    [handleSuccess, handleError],
  );

  const updateBranch = useCallback(
    async (
      slug,
      data,
      { presentImageKeys = [] } = {},
      { onSuccess, onStart, onFinish, onError } = {},
    ) => {
      onStart?.();
      try {
        const payload = Object.fromEntries(
          Object.entries(data).filter(([, v]) => v != null),
        );
        if (payload.branch_type) {
          await BranchService.changeBranchType(slug, payload.branch_type.name);
        } else {
          await BranchService.editData(slug, payload, {
            presentImageKeys: presentImageKeys.filter(
              (key) => typeof data[key] === 'object',
            ),
          });
        }
        handleSuccess(`${slug} has been updated.`);
        onSuccess?.({ slug, ...data });
      } catch (error) {
        handleError(error);
        onError?.(error);
      } finally {
        onFinish?.();
      }
    },
    [handleSuccess, handleError],
  );

  return useMemo(
    () => ({
      addBranch,
      updateBranch,
    }),
    [addBranch, updateBranch],
  );
}

export default function useSuperAdminActions() {
  const { setToastContent } = useContext(ToastContext);

  const handleError = useCallback(
    (err) => {
      logger.error(err);
      setToastContent(err?.data?.message || 'Error.', 'error');
    },
    [setToastContent],
  );
  const handleSuccess = useCallback(
    (msg) => setToastContent(msg, 'success'),
    [setToastContent],
  );

  return {
    board: useBoardSuperAdminActions({ handleSuccess, handleError }),
    branches: useBranchesSuperAdminActions({ handleSuccess, handleError }),
  };
}
