import { useMemo } from 'react';
import { getToolbar } from '../utils/toolbars';

function useToolbar({ quickFilterContainerRef, gridType, adminActions }) {
  return useMemo(
    () =>
      getToolbar({
        gridType,
        adminActions,
        quickFilterContainerRef,
      }) || {},
    [quickFilterContainerRef, gridType, adminActions],
  );
}

export default useToolbar;
