import { useMemo, useRef } from 'react';
import { PropTypes } from 'prop-types';
import LinearProgress from '@mui/material/LinearProgress';
import { DataGrid } from '@mui/x-data-grid'; // check if this messes up bundle size
import AdminPanelError from '../AdminPanelError';
import { useEntitySettings, useData, useToolbar, useFooter } from '../../hooks';
import { allowEntersForMultilineEditCells } from '../../utils/settings';
import { admin as adminPropTypes } from 'utils/prop-types';

import './AdminDataGrid.css';

export default function AdminDataGrid({
  entityType,
  entityName,
  gridType: _gridType,
  adminActions,
  onUpdateListenerEventName,
  onInlineEdit,
  shouldProceedWithEdit,
  extraParams,
  refetchAllOnRowEdit,
  onUpdateTriggerEventName,
  refetchRowsOnInlineEditSuccessKeys,
  entitySettingsKey,
  ...props
}) {
  const gridType = _gridType || entityType;
  const quickFilterContainerRef = useRef();

  const {
    rowIdentifier,
    formatRow,
    getBaseColumns,
    getRowId,
    dataGridProps,
    getEntityActions,
  } = useEntitySettings(gridType);
  const {
    allowSelection,
    allowActions,
    actionsWidth,
    datagridClassname,
    displayFooter,
    serverSideDataHandling,
  } = dataGridProps;

  const { Toolbar, toolbarProps, quickFilterContainer } = useToolbar({
    quickFilterContainerRef,
    gridType,
    adminActions,
  });

  const {
    error,
    slots: dataSlots,
    rowSelectionModel,
    batchActionCallbacks,
    dataProps,
    actionCallbacks,
    dataUpdateRef,
  } = useData({
    type: entityType,
    name: entityName,
    serviceKey: entitySettingsKey,
    rowIdentifier,
    shouldProceedWithEdit,
    formatRow,
    serverSideDataHandling,
    allowSelection,
    extraParams,
    onUpdateListenerEventName,
    refetchAllOnRowEdit,
    refetchRowsOnInlineEditSuccessKeys,
    onUpdateTriggerEventName,
    onInlineEdit,
  });

  const footer = useFooter(displayFooter);

  const columns = useMemo(() => {
    const baseColumns = getBaseColumns(adminActions, actionCallbacks);
    if (!allowActions) return baseColumns;
    const actions = {
      field: 'actions',
      type: 'actions',
      width: actionsWidth || 30,
      getActions: (params) =>
        getEntityActions(adminActions, entityName, params, actionCallbacks),
    };

    return [...baseColumns, actions];
  }, [
    adminActions,
    entityName,
    getEntityActions,
    actionsWidth,
    getBaseColumns,
    allowActions,
    actionCallbacks,
  ]);

  if (error) {
    return (
      <AdminPanelError>{error?.data?.message || 'Error.'}</AdminPanelError>
    );
  }

  // FUTURE: Styling with Styled components
  return (
    <div className={`admin-panel-datagrid-wrapper ${datagridClassname ?? ''}`}>
      {quickFilterContainer}
      <DataGrid
        apiRef={dataUpdateRef}
        className="admin-panel-datagrid"
        sx={sx}
        autoHeight
        getRowId={getRowId}
        checkboxSelection={allowSelection}
        columns={columns}
        slots={{
          loadingOverlay:
            dataProps.rows.length > 0 ? LinearProgress : undefined,
          toolbar: Toolbar,
          footer: footer.slots,
          ...dataSlots,
        }}
        slotProps={{
          toolbar: {
            settings: toolbarProps,
            batchActions: {
              adminActions,
              callbacks: batchActionCallbacks,
              selectedRows: rowSelectionModel,
              entityName,
              gridType,
            },
          },
        }}
        {...props}
        onCellEditStop={allowEntersForMultilineEditCells}
        {...dataProps}
      />
    </div>
  );
}
AdminDataGrid.propTypes = {
  entityType: adminPropTypes.entityType,
  gridType: adminPropTypes.gridType,
  entityName: PropTypes.string,
  extraParams: PropTypes.object,
  shouldProceedWithEdit: PropTypes.func,
  entitySettingsKey: PropTypes.oneOf([
    'members',
    'applications',
    'list',
    'info',
  ]),
  onInlineEdit: PropTypes.func,
  onUpdateTriggerEventName: PropTypes.string,
  onUpdateListenerEventName: PropTypes.string,
  refetchRowsOnInlineEditSuccessKeys: PropTypes.arrayOf(PropTypes.string),
  adminActions: PropTypes.object,
  refetchAllOnRowEdit: PropTypes.bool,
};
AdminDataGrid.defaultProps = {
  entitySettingsKey: 'members',
};

const sx = {
  boxShadow: 2,
  border: 2,
  backgroundColor: '#1b1b1b',
  borderColor: 'var(--background-gray)',
  '& .MuiDataGrid-cell:focus, & .MuiDataGrid-cell:focus-within': {
    outline: 'solid var(--primary-red) 1px',
  },
  '& .MuiDataGrid-iconSeparator': {
    fill: '#828282',
  },
  '& .MuiDataGrid-row': {
    borderColor: '#383838',
    '> div': {
      borderColor: '#383838',
    },
  },
  '& .MuiDataGrid-columnHeaders': {
    borderColor: '#383838',
  },
  '& .MuiDataGrid-columnHeader:focus-within': {
    outline: 'solid var(--primary-red) 1px',
  },
  '& .MuiDataGrid-row.Mui-selected': {
    backgroundColor: 'rgba(210, 25, 25, 0.04)',
    '&:hover': {
      backgroundColor: 'rgba(210, 25, 25, 0.08)',
    },
  },
};
