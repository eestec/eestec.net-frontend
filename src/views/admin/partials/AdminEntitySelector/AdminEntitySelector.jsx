import { admin as adminPropTypes } from 'utils/prop-types';
import SingleEntitySelector from '../SingleEntitySelector';

import './AdminEntitySelector.css';

export default function AdminEntitySelector({ entities }) {
  return (
    <div className="admin-entity-selector">
      {Object.entries(entities)
        .filter(([, value]) => value?.length)
        .map(([key, value]) => (
          <SingleEntitySelector elements={value} entityType={key} key={key} />
        ))}
    </div>
  );
}
AdminEntitySelector.propTypes = {
  entities: adminPropTypes.entities,
};
AdminEntitySelector.defaultProps = {
  entities: [],
};
