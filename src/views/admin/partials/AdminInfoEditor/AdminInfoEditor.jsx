import { useState, useEffect, useCallback, useRef, useMemo } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button';
import logger from 'utils/logger';
import useForm from 'hooks/useForm';
import {
  formKeys as _formKeys,
  getValidators,
  renderAdminInputs,
} from '../../utils/inputs';
import AdminPanelLoader from '../AdminPanelLoader';
import AdminPanelError from '../AdminPanelError';
import { getEntityServiceSettings } from '../../utils/services';

import './AdminInfoEditor.css';

export default function AdminInfoEditor({
  entityType,
  entityName,
  adminActions,
}) {
  const { formKeysNames, formKeys, imageKeys } = useMemo(() => {
    const formKeys = _formKeys[entityType].info;
    return {
      formKeys,
      formKeysNames: formKeys.map(({ name }) => name),
      imageKeys: formKeys
        .filter(({ type }) => type === 'image')
        .map(({ name }) => name),
    };
  }, [entityType]);
  const { editData } = adminActions;
  const abortController = useRef(new AbortController());

  const fetchEntityData = useCallback(
    (shouldSetLoading = true, dataInitializer) => {
      if (shouldSetLoading) {
        setLoading(true);
      }
      abortController.current.abort();
      abortController.current = new AbortController();

      const { serviceFn, formatFn } = getEntityServiceSettings(
        entityType,
        'info',
      );

      // Get entity information
      serviceFn(entityName, abortController.signal)
        .then((response) => {
          const formattedResponse = formatFn(response);
          const filteredResponse = formKeysNames.reduce((acc, cur) => {
            acc[cur] = formattedResponse[cur];
            return acc;
          }, {});
          dataInitializer?.(filteredResponse);
          setGeneralError(null);
        })
        .catch((err) => {
          logger.log(err);
          setGeneralError(err);
        })
        .finally(() => setLoading(false));
    },
    [entityName, entityType, formKeysNames],
  );

  const {
    initData,
    handleSubmit,
    handleInputChange,
    handleRawInputChange,
    handleReset,
    isFormLoading,
    setFormLoading,
    formData,
    errors,
    setErrors,
  } = useForm({
    formKeys: formKeysNames,
    validators: getValidators(formKeys),
    onSubmit: (formData) =>
      editData(
        formData,
        { presentImageKeys: imageKeys },
        {
          onError: (err) =>
            setErrors(
              formKeysNames.reduce((errObj, curKey) => {
                errObj[curKey] = err?.data?.errors?.[curKey]?.[0];
                return errObj;
              }, {}),
            ),
          onFinish: () => setFormLoading(false),
          onSuccess: () => fetchEntityData(false, initData),
        },
      ),
  });
  const [generalError, setGeneralError] = useState();
  const [loading, setLoading] = useState(false);

  // this effect retrieves data about the entity
  useEffect(() => {
    fetchEntityData(true, initData);

    // during unmount of the component abort the ongoing requests to avoid memory leak
    return () => abortController.current.abort();
  }, [fetchEntityData, initData]);

  if (loading) {
    return <AdminPanelLoader />;
  }
  if (generalError) {
    return (
      <AdminPanelError>
        {generalError?.data?.message || 'Error.'}
      </AdminPanelError>
    );
  }

  return (
    <form>
      {renderAdminInputs({
        onChange: handleInputChange,
        onImageChange: handleRawInputChange,
        keys: formKeys,
        errors,
        formData,
      })}
      <div className="admin-info-editor-buttons">
        <Button
          type="submit"
          className="primary-button"
          isLoading={isFormLoading}
          onClick={handleSubmit}
        >
          Submit
        </Button>
        <Button className="secondary-button" type="reset" onClick={handleReset}>
          Reset
        </Button>
      </div>
    </form>
  );
}
AdminInfoEditor.propTypes = {
  entityType: PropTypes.oneOf(['cities', 'teams', 'events', 'all']).isRequired,
  entityName: PropTypes.string,
  adminActions: PropTypes.shape({
    editData: PropTypes.func,
  }),
};
