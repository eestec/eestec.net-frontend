import PropTypes from 'prop-types';
import ErrorOutline from '@mui/icons-material/ErrorOutline';
import AdminPanelSegment from './AdminPanelSegment';

function AdminPanelError({ children }) {
  return (
    <AdminPanelSegment>
      <ErrorOutline fontSize="large" />
      {children}
    </AdminPanelSegment>
  );
}
AdminPanelError.propTypes = {
  children: PropTypes.string,
};

export default AdminPanelError;
