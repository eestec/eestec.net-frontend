import PropTypes from 'prop-types';
import CircularProgress from '@mui/material/CircularProgress';
import AdminPanelSegment from './AdminPanelSegment';

function AdminPanelLoader({ children }) {
  return (
    <AdminPanelSegment>
      <CircularProgress size="5rem" />
      {children}
    </AdminPanelSegment>
  );
}
AdminPanelLoader.propTypes = {
  children: PropTypes.string,
};

export default AdminPanelLoader;
