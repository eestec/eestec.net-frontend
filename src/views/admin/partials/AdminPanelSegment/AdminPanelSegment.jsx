import PropTypes from 'prop-types';
import './AdminPanelSegment.css';

export default function AdminPanelSegment({ children }) {
  return <div className="admin-panel-segment">{children}</div>;
}
AdminPanelSegment.propTypes = {
  children: PropTypes.node.isRequired,
};
