import { memo, useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { admin as adminPropTypes } from 'utils/prop-types';
import { getEntityBatchActions } from '../../utils/actions';

import './AdminToolbarActions.css';

function AdminToolbarActions({
  selectedRows,
  gridType,
  entityName,
  callbacks,
  adminActions,
}) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="manage-selected-button"
        aria-controls={open ? 'manage-selected-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        variant="text"
        disableElevation
        onClick={handleClick}
        startIcon={<KeyboardArrowDownIcon />}
        className="admin-data-grid-toolbar-actions"
        disabled={!selectedRows?.length}
      >
        Manage selected
      </Button>
      <Menu
        id="manage-selected-menu"
        MenuListProps={{
          'aria-labelledby': 'manage-selected-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
      >
        {getEntityBatchActions[gridType](
          adminActions,
          entityName,
          selectedRows,
          callbacks,
          handleClose,
        )}
      </Menu>
    </div>
  );
}
AdminToolbarActions.propTypes = {
  ...adminPropTypes.toolbarActions,
  adminActions: PropTypes.object,
};

const MemoizedAdminToolbarActions = memo(AdminToolbarActions);
export default MemoizedAdminToolbarActions;
