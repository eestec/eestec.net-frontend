import PropTypes from 'prop-types';
import EditIcon from '@mui/icons-material/Edit';
import Avatar from '@mui/material/Avatar';
import ImageFileInput from 'components/ImageFileInput/ImageFileInput';

import './DataGridAvatar.css';

export default function DataGridAvatar({
  editable,
  onChange,
  fieldName,
  id,
  ...props
}) {
  const handleChange = (imageData) => {
    document.activeElement.blur();
    onChange(id, { [fieldName]: imageData });
  };
  return (
    <div className="data-grid-avatar">
      <Avatar {...props} />
      {editable && (
        <ImageFileInput
          className="data-grid-avatar-image-file-input"
          onChange={handleChange}
          Icon={EditIcon}
          accept="image/png, image/jpeg, image/jpg"
        />
      )}
    </div>
  );
}
DataGridAvatar.propTypes = {
  editable: PropTypes.bool,
  onChange: PropTypes.func,
  fieldName: PropTypes.string,
  id: PropTypes.number,
};
