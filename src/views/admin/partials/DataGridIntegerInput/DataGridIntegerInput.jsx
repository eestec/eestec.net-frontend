import { useLayoutEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useGridApiContext } from '@mui/x-data-grid';

import './DataGridIntegerInput.css';

const isNullOrEmptyString = (val) => val === '' || val == null;

export default function DataGridIntegerInput({
  id,
  value,
  field,
  hasFocus,
  min,
  max,
}) {
  const apiRef = useGridApiContext();
  const ref = useRef();

  const _max = max === 'rowCount' ? apiRef.current.getRowsCount() : max;

  useLayoutEffect(() => {
    if (hasFocus) {
      ref.current.focus();
    }
  }, [hasFocus]);

  const setValue = (newValue) =>
    apiRef.current.setEditCellValue({
      id,
      field,
      value: newValue,
    });

  const handleValueChange = (event) => {
    const newValue = event.target.value; // The new value entered by the user

    if (isNullOrEmptyString(newValue)) {
      return setValue('');
    }

    if (!/^[-+]?[0-9]*$/.test(newValue)) {
      return;
    }

    const parsed = parseInt(newValue, 10);
    if (parsed > _max) return setValue(_max);
    if (parsed < min) return setValue(min);

    setValue(parsed);
  };

  return (
    <input
      ref={ref}
      type="number"
      min={min}
      max={_max}
      step={1}
      value={isNullOrEmptyString(value) ? '' : value}
      onChange={handleValueChange}
      className="data-grid-integer-input"
    />
  );
}
DataGridIntegerInput.propTypes = {
  id: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  field: PropTypes.string,
  hasFocus: PropTypes.bool,
  min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};
