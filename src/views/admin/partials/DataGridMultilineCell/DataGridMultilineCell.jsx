import PropTypes from 'prop-types';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import InputBase from '@mui/material/InputBase';
import { useGridApiContext } from '@mui/x-data-grid';
import { useState, useLayoutEffect, useCallback } from 'react';

import './DataGridMultilineCell.css';

const hintMessage = '<Enter> for newline, <Ctrl>+<Enter> for Save';

export default function DataGridMultilineCell(props) {
  const { id, field, value: _value, colDef, hasFocus, valueKey } = props;
  const isValueWithKey = !!(typeof _value === 'object' && valueKey);
  const value = isValueWithKey ? _value[valueKey] : _value;
  const [valueState, setValueState] = useState(value);
  const [anchorEl, setAnchorEl] = useState();
  const [inputRef, setInputRef] = useState(null);
  const apiRef = useGridApiContext();

  useLayoutEffect(() => {
    if (hasFocus && inputRef) {
      inputRef.focus();
    }
  }, [hasFocus, inputRef]);

  const handleRef = useCallback((el) => {
    setAnchorEl(el);
  }, []);

  const handleChange = useCallback(
    (event) => {
      const newValue = event.target.value;
      setValueState(newValue);
      apiRef.current.setEditCellValue(
        {
          id,
          field,
          value: isValueWithKey
            ? { ..._value, [valueKey]: newValue }
            : newValue,
          debounceMs: 400,
        },
        event,
      );
    },
    [apiRef, field, id, isValueWithKey, valueKey, _value],
  );

  return (
    <div className="data-grid-multiline-cell">
      {/* TODO: Styled */}
      <div
        ref={handleRef}
        style={{
          ...sx.topLine,
          width: colDef.computedWidth,
        }}
      />
      {anchorEl && (
        <Popper open anchorEl={anchorEl} placement="bottom-start">
          <Paper
            elevation={1}
            sx={{
              ...sx.paper,
              minWidth: colDef.computedWidth,
            }}
          >
            <InputBase
              multiline
              rows={4}
              value={valueState}
              sx={sx.inputBase}
              onChange={handleChange}
              inputRef={(ref) => setInputRef(ref)}
            />
            <div className="data-grid-multiline-cell-hint">{hintMessage}</div>
          </Paper>
        </Popper>
      )}
    </div>
  );
}
DataGridMultilineCell.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  field: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  colDef: PropTypes.object,
  hasFocus: PropTypes.bool,
  valueKey: PropTypes.string,
};

const sx = {
  inputBase: {
    textarea: { resize: 'vertical', minHeight: '50px' },
    width: '100%',
    color: 'white',
    backgroundColor: 'black',
  },
  topLine: {
    height: 1,
    display: 'block',
    position: 'absolute',
    top: 0,
  },
  paper: {
    p: 1,
    backgroundColor: 'black',
    position: 'relative',
  },
};
