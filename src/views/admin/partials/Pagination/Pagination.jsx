import PropTypes from 'prop-types';
import { GridPagination as MuiGridPagination } from '@mui/x-data-grid';
import MuiPagination from '@mui/material/Pagination';

function Pagination({ page, onPageChange, count, rowsPerPage, className }) {
  return (
    <MuiPagination
      color="primary"
      className={`admin-datagrid-pagination ${className ?? ''}`}
      count={Math.ceil(count / rowsPerPage)}
      page={page + 1}
      onChange={(event, newPage) => {
        onPageChange(event, newPage - 1);
      }}
    />
  );
}
Pagination.propTypes = {
  page: PropTypes.number,
  onPageChange: PropTypes.func,
  count: PropTypes.number,
  rowsPerPage: PropTypes.number,
  className: PropTypes.string,
};

export default function GridPagination(props) {
  return <MuiGridPagination ActionsComponent={Pagination} {...props} />;
}
