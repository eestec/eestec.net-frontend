import { useState } from 'react';
import PropTypes from 'prop-types';
import Autocomplete from 'components/Autocomplete/Autocomplete';
import ButtonLink from 'components/ButtonLink/ButtonLink';
import ArrowForward from '@mui/icons-material/ArrowForward';
import { admin as adminPropTypes } from 'utils/prop-types';

import './SingleEntitySelector.css';

const entityData = {
  cities: { title: 'Manage branch details', label: 'Choose a branch' },
  teams: { title: 'Manage teams', label: 'Choose a team' },
  events: { title: 'Manage events', label: 'Choose an event' },
};

// this is for entities as a whole
const idMapping = {
  cities: 'id',
  // teams: ...
};

export default function SingleEntitySelector({ entityType, elements }) {
  const isSingleElement = elements.length === 1;
  const formatLink = (elem) =>
    `/${entityType}/${elem[idMapping[entityType]]}/settings`;
  const { title, label } = entityData[entityType];

  const [chosenEntityLink, setChosenEntityLink] = useState(
    isSingleElement ? formatLink(elements[0]) : '',
  );

  const handleChange = (elem) => setChosenEntityLink(formatLink(elem));

  return (
    <div className="single-entity-selector">
      <h2>{title}</h2>
      <div className="single-entity-selector-content">
        {!isSingleElement ? (
          <Autocomplete
            label={label}
            optionLabel="name"
            options={elements}
            onChange={handleChange}
          />
        ) : null}
        <ButtonLink
          to={chosenEntityLink}
          state={{ from: 'adminPanel' }}
          className="primary-button"
          icon={ArrowForward}
          disabled={!chosenEntityLink}
        >
          {!isSingleElement ? 'Go' : `Manage ${elements[0].name}`}
        </ButtonLink>
      </div>
    </div>
  );
}
SingleEntitySelector.propTypes = {
  elements: PropTypes.array,
  entityType: adminPropTypes.entityType,
};
SingleEntitySelector.defaultProps = {
  elements: [],
};
