import {
  useCallback,
  useContext,
  useMemo,
  useEffect,
  useRef,
  useState,
} from 'react';
import PropTypes from 'prop-types';
import Heading from 'components/Heading/Heading';
import ModalContext from 'context/ModalContext';
import Button from 'components/Button/Button';
import useForm from 'hooks/useForm';
import { admin as adminPropTypes } from 'utils/prop-types';
import AdminEntitySelector from '../AdminEntitySelector';
import AdminDataGrid from '../AdminDataGrid';
import useSuperAdminActions from '../../hooks/useSuperAdminActions';
import {
  formKeys as _formKeys,
  getMandates,
  getValidators,
  renderAdminInputs,
} from '../../utils/inputs';

import './SuperAdminDashboard.css';
import Dropdown from 'components/Dropdown/Dropdown';

const boardRefetchRowsOnInlineEditSuccessKeys = [
  'order',
  'name',
  'description',
];
const boardInitialState = {
  sorting: {
    sortModel: [{ field: 'order', sort: 'asc' }],
  },
};

const useInlineEdit = (onInlineEdit, extraParams = undefined) =>
  useCallback(
    ({ data, identifier }) =>
      onInlineEdit(identifier, data, extraParams, {
        onError: (err) => {
          throw err;
        },
      }),
    [onInlineEdit, extraParams],
  );

function NewElementForm({ onSubmit, entityType, heading, callbacks }) {
  const { formKeysNames, formKeys } = useMemo(() => {
    const formKeys = _formKeys[entityType].new;
    return {
      formKeys,
      formKeysNames: formKeys.map(({ name }) => name),
    };
  }, [entityType]);
  const { setModalContent } = useContext(ModalContext);

  const {
    handleSubmit,
    handleInputChange,
    isFormLoading,
    setFormLoading,
    formData,
    errors,
    setErrors,
  } = useForm({
    formKeys: formKeysNames,
    validators: getValidators(formKeys),
    onSubmit: (formData) =>
      onSubmit(formData, {
        onError: (err) => {
          setErrors(
            formKeysNames.reduce((errObj, curKey) => {
              errObj[curKey] = err?.data?.errors?.[curKey]?.[0];
              return errObj;
            }, {}),
          );
          callbacks.onError?.();
        },
        onStart: () => {
          setFormLoading(true);
          callbacks.onStart?.();
        },
        onFinish: () => {
          setFormLoading(false);
          callbacks.onFinish?.();
        },
        onSuccess: () => {
          setModalContent(null);
          callbacks.onSuccess?.();
        },
      }),
  });

  return (
    <form
      style={{
        padding: '2rem',
        background: 'var(--background-gray)',
        borderRadius: '1rem',
      }}
    >
      <h2
        style={{
          marginTop: 0,
          fontSize: '1.2rem',
          color: 'white',
        }}
      >
        {heading}
      </h2>
      {renderAdminInputs({
        onChange: handleInputChange,
        keys: formKeys,
        textInputsColumns: 1,
        errors,
        formData,
      })}
      <div
        style={{
          display: 'flex',
          gap: '1rem',
          marginTop: '1rem',
          width: '100%',
          justifyContent: 'center',
        }}
      >
        <Button
          className="secondary-button"
          onClick={() => setModalContent(null)}
        >
          Cancel
        </Button>
        <Button
          type="submit"
          className="primary-button"
          isLoading={isFormLoading}
          onClick={handleSubmit}
        >
          Add
        </Button>
      </div>
    </form>
  );
}

const useNewElementFormDialog = ({ onSubmit, entityType, heading }) => {
  const { setModalContent } = useContext(ModalContext);

  return useCallback(
    (callbacks) => {
      setModalContent(
        <NewElementForm
          onSubmit={onSubmit}
          entityType={entityType}
          heading={heading}
          callbacks={callbacks}
        />,
        { dismissable: false },
      );
    },
    [onSubmit, heading, entityType, setModalContent],
  );
};

function BoardDataGrid({ adminActions }) {
  const mandateRef = useRef(getMandates());
  const [chosenMandate, setChosenMandate] = useState(
    mandateRef.current.current,
  );

  const onInlineEdit = useInlineEdit(adminActions.updateBoardPosition, {
    chosenMandate,
  });
  const shouldProceedWithBoardEdit = useCallback((delta) => {
    if (!delta) {
      return false;
    }

    if ('name' in delta) {
      if (!delta.name?.slug) {
        return false;
      }
    }

    return true;
  }, []);
  const addBoardPosition = useNewElementFormDialog({
    entityType: 'board',
    onSubmit: adminActions.addBoardPosition,
    heading: 'Add a new Board position',
  });
  const _adminActions = useMemo(
    () => ({ ...adminActions, addBoardPosition }),
    [addBoardPosition, adminActions],
  );

  const extraParams = useMemo(
    () => ({ mandate: chosenMandate.value }),
    [chosenMandate],
  );

  return (
    <div className="board-data-grid-container">
      <Dropdown
        label={chosenMandate.label}
        onSelectClick={setChosenMandate}
        optionLabel="label"
        optionKey="value"
        options={mandateRef.current.mandates}
        className="board-data-grid-dropdown"
      />
      <AdminDataGrid
        extraParams={extraParams}
        entityType="board"
        refetchRowsOnInlineEditSuccessKeys={
          boardRefetchRowsOnInlineEditSuccessKeys
        }
        refetchAllOnRowEdit
        shouldProceedWithEdit={shouldProceedWithBoardEdit}
        adminActions={_adminActions}
        onInlineEdit={onInlineEdit}
        initialState={boardInitialState}
      />
    </div>
  );
}

function BranchesDataGrid({ adminActions }) {
  const onInlineEdit = useInlineEdit(adminActions.updateBranch);
  const addBranch = useNewElementFormDialog({
    entityType: 'cities',
    onSubmit: adminActions.addBranch,
    heading: 'Add a new branch',
  });
  const _adminActions = useMemo(
    () => ({ ...adminActions, addBranch }),
    [addBranch, adminActions],
  );

  return (
    <AdminDataGrid
      gridType="citiesList"
      entityType="cities"
      entitySettingsKey="list"
      adminActions={_adminActions}
      onInlineEdit={onInlineEdit}
      onUpdateTriggerEventName="branchAdded"
    />
  );
}

export default function SuperAdminDashboard({ entities, refetchAdminData }) {
  const { board, branches } = useSuperAdminActions();

  useEffect(() => {
    document.addEventListener('branchAdded', refetchAdminData, {
      passive: true,
    });
    return () => {
      document.removeEventListener('branchAdded', refetchAdminData);
    };
  }, [refetchAdminData]);

  return (
    <div>
      <Heading variant="h1" lineAfter>
        Administration Dashboard
      </Heading>
      <div className="super-admin-grids">
        <div>
          <Heading variant="h2">Manage entities</Heading>
          <AdminEntitySelector entities={entities} />
        </div>
        <div>
          <Heading variant="h2">Manage the Board</Heading>
          <BoardDataGrid adminActions={board} />
        </div>
        <div>
          <Heading variant="h2">Manage branches</Heading>
          <BranchesDataGrid adminActions={branches} />
        </div>
      </div>
    </div>
  );
}

BoardDataGrid.propTypes = {
  adminActions: adminPropTypes.adminActions.board,
};
BranchesDataGrid.propTypes = {
  adminActions: adminPropTypes.adminActions.cities,
};
NewElementForm.propTypes = {
  onSubmit: PropTypes.func,
  entityType: PropTypes.string,
  heading: PropTypes.string,
  callbacks: adminPropTypes.toolbarActions.callbacks,
};
NewElementForm.defaultProps = {
  callbacks: {},
};
SuperAdminDashboard.propTypes = {
  entities: PropTypes.shape({
    events: PropTypes.arrayOf(PropTypes.object),
    cities: PropTypes.arrayOf(PropTypes.object),
    teams: PropTypes.arrayOf(PropTypes.object),
  }),
  refetchAdminData: PropTypes.func,
};
