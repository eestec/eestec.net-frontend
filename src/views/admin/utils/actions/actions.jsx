import { Link } from 'react-router-dom';

import Divider from '@mui/material/Divider';
import AlumniIcon from '@mui/icons-material/Elderly';
import ActiveIcon from '@mui/icons-material/SportsMartialArts';
import RemovePersonIcon from '@mui/icons-material/PersonRemove';
import PersonIcon from '@mui/icons-material/Person';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import DeleteIcon from '@mui/icons-material/Delete';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import Edit from '@mui/icons-material/Edit';

import { BranchRoles } from 'utils/roles';
import { capitalizeFirstLetter, pushToMapArray } from 'utils/misc';
import { rowIdMapping } from '../datagrid';
import { SingleActionItem, BatchActionItem } from './components';

const getAction = (
  { label, lineBefore, openInNewTab, ...props },
  Element = SingleActionItem,
) => (
  <Element
    key={`actionItem--${label}`}
    label={label}
    lineBefore={lineBefore}
    {...(openInNewTab ? { target: '_blank', rel: 'noopener noreferrer' } : {})}
    {...props}
  />
);
const getBatchAction = (props) => getAction(props, BatchActionItem);

export const getEntityBatchActions = {
  cities: (
    { changeMembersRoles, removeMembers },
    entityName,
    rows = [],
    callbacks,
    handleClose,
  ) => {
    const groupedRows = rows.reduce(
      (acc, row) => {
        pushToMapArray(acc, row.branch_role_id, row[rowIdMapping.cities]);
        return acc;
      },
      new Map([
        [BranchRoles.BOARD, []],
        [BranchRoles.ACTIVE_MEMBER, []],
        [BranchRoles.ALUMNI, []],
      ]),
    );
    const isAnyBoard = !!groupedRows.get(BranchRoles.BOARD)?.length;
    const isAnyActive = !!groupedRows.get(BranchRoles.ACTIVE_MEMBER)?.length;
    const isAnyAlumni = !!groupedRows.get(BranchRoles.ALUMNI)?.length;

    const result = [];
    if (isAnyBoard) {
      result.push(
        getBatchAction({
          icon: <GroupRemoveIcon />,
          onClick: changeMembersRoles(
            groupedRows.get(BranchRoles.BOARD),
            BranchRoles.ACTIVE_MEMBER,
            callbacks,
          ),
          handleClose,
          label: 'Remove all from the board',
        }),
      );
    }
    if (isAnyActive || isAnyAlumni) {
      result.push(
        getBatchAction({
          icon: <GroupAddIcon />,
          onClick: changeMembersRoles(
            [
              ...groupedRows.get(BranchRoles.ACTIVE_MEMBER),
              ...groupedRows.get(BranchRoles.ALUMNI),
            ],
            BranchRoles.BOARD,
            callbacks,
          ),
          handleClose,
          label: 'Add all to the board',
        }),
      );
    }
    result.push(<Divider sx={{ my: 0.5 }} key="after-board-divider" />);
    if (isAnyAlumni) {
      result.push(
        getBatchAction({
          icon: <ActiveIcon />,
          onClick: changeMembersRoles(
            groupedRows.get(BranchRoles.ALUMNI),
            BranchRoles.ACTIVE_MEMBER,
            callbacks,
          ),
          handleClose,
          label: 'Make all active',
          lineBefore: true,
        }),
      );
    }
    if (isAnyActive || isAnyBoard) {
      result.push(
        getBatchAction({
          icon: <AlumniIcon />,
          onClick: changeMembersRoles(
            [
              ...groupedRows.get(BranchRoles.ACTIVE_MEMBER),
              ...groupedRows.get(BranchRoles.BOARD),
            ],
            BranchRoles.ALUMNI,
            callbacks,
          ),
          handleClose,
          label: 'Make all alumni',
        }),
      );
    }
    result.push(
      getBatchAction({
        icon: <RemovePersonIcon />,
        onClick: removeMembers(
          rows.map((row) => row[rowIdMapping.cities]),
          callbacks,
        ),
        handleClose,
        label: `Remove all from ${capitalizeFirstLetter(entityName)}`,
      }),
    );

    return result;
  },
  board: () => {},
  teams: () => {},
  applications: (
    { manageMembership },
    entityName,
    rows = [],
    callbacks,
    handleClose,
  ) => [
    getBatchAction({
      icon: <CheckIcon />,
      onClick: manageMembership(
        rows.map((row) => row[rowIdMapping.applications]),
        true,
        callbacks,
      ),
      handleClose,
      label: `Accept all to ${capitalizeFirstLetter(entityName)}`,
    }),
    getBatchAction({
      icon: <CloseIcon />,
      onClick: manageMembership(
        rows.map((row) => row[rowIdMapping.applications]),
        false,
        callbacks,
      ),
      handleClose,
      label: `Reject all from ${capitalizeFirstLetter(entityName)}`,
    }),
  ],
};

export const getEntityActions = {
  cities: (
    { changeMemberRole, removeMember },
    entityName,
    { row },
    callbacks,
  ) =>
    [
      row.branch_role_id !== BranchRoles.BOARD
        ? getAction({
            icon: <GroupAddIcon />,
            onClick: changeMemberRole(
              row[rowIdMapping.cities],
              BranchRoles.BOARD,
              callbacks,
            ),
            label: 'Add to the board',
            showInMenu: true,
          })
        : getAction({
            icon: <GroupRemoveIcon />,
            onClick: changeMemberRole(
              row[rowIdMapping.cities],
              BranchRoles.ACTIVE_MEMBER,
              callbacks,
            ),
            label: 'Remove from the board',
            showInMenu: true,
          }),
      row.branch_role_id === BranchRoles.ALUMNI
        ? getAction({
            icon: <ActiveIcon />,
            onClick: changeMemberRole(
              row[rowIdMapping.cities],
              BranchRoles.ACTIVE_MEMBER,
              callbacks,
            ),
            label: 'Make active',
            lineBefore: true,
            showInMenu: true,
          })
        : getAction({
            icon: <AlumniIcon />,
            onClick: changeMemberRole(
              row[rowIdMapping.cities],
              BranchRoles.ALUMNI,
              callbacks,
            ),
            label: 'Make alumni',
            lineBefore: true,
            showInMenu: true,
          }),
      getAction({
        icon: <RemovePersonIcon />,
        onClick: removeMember(row[rowIdMapping.cities], callbacks),
        label: `Remove from ${capitalizeFirstLetter(entityName)}`,
        showInMenu: true,
      }),
      getAction({
        icon: <PersonIcon />,
        component: Link,
        lineBefore: true,
        label: 'View profile',
        openInNewTab: true,
        to: `/users/${row[rowIdMapping.cities]}`,
        showInMenu: true,
      }),
    ].filter(Boolean),
  teams: () => [],
  board: ({ removeBoardMember }, _entityName, { row }, callbacks) => [
    getAction({
      label: 'Remove',
      icon: <DeleteIcon />,
      onClick: removeBoardMember(row[rowIdMapping.board], callbacks),
      showInMenu: false,
    }),
  ],
  applications: ({ manageMembership }, _entityName, { row }, callbacks) => [
    getAction({
      label: 'Accept',
      icon: <CheckIcon />,
      onClick: manageMembership(
        row[rowIdMapping.applications],
        true,
        callbacks,
      ),
      showInMenu: false,
    }),
    getAction({
      label: 'Reject',
      icon: <CloseIcon />,
      onClick: manageMembership(
        row[rowIdMapping.applications],
        false,
        callbacks,
      ),
      showInMenu: false,
    }),
  ],
  citiesList: (_actions, _entityName, { row }) => [
    getAction({
      label: 'Edit other details',
      icon: <Edit />,
      component: Link,
      to: `/cities/${row[rowIdMapping.citiesList]}/settings`,
      state: { from: 'adminPanel' },
      showInMenu: true,
    }),
    getAction({
      label: 'View branch page',
      icon: <OpenInNewIcon />,
      component: Link,
      to: `/cities/${row[rowIdMapping.citiesList]}`,
      openInNewTab: true,
      showInMenu: true,
    }),
  ],
};
