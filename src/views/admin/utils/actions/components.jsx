import PropTypes from 'prop-types';
import { GridActionsCellItem } from '@mui/x-data-grid';
import MenuItem from '@mui/material/MenuItem';

export const SingleActionItem = ({ lineBefore, ...props }) => {
  let classes = '';
  if (props.showInMenu) {
    classes += ' admin-panel-grid-actions-menu-cell';
  }
  if (lineBefore) {
    classes += ' admin-panel-grid-actions-cell-line-before';
  }
  return <GridActionsCellItem className={classes} {...props} />;
};
SingleActionItem.propTypes = {
  lineBefore: PropTypes.bool,
  showInMenu: PropTypes.bool,
};
export const BatchActionItem = ({
  onClick,
  icon,
  label,
  lineBefore,
  handleClose,
  ...props
}) => (
  <>
    <MenuItem
      onClick={() => {
        onClick();
        handleClose?.();
      }}
      disableRipple
      {...props}
    >
      {icon}
      {label}
    </MenuItem>
  </>
);
BatchActionItem.propTypes = {
  onClick: PropTypes.func,
  handleClose: PropTypes.func,
  icon: PropTypes.node,
  label: PropTypes.string,
  lineBefore: PropTypes.bool,
};
