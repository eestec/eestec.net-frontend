import { Fragment, useState, useEffect, useCallback } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import {
  type GridRenderEditCellParams,
  useGridApiContext,
} from '@mui/x-data-grid';
import useDebouncedState from 'hooks/useDebouncedState';

import type { PaginatedResponse } from 'utils/types';

function AsynchronousInputCell<TData = unknown>(
  props: AsynchronousInputCellProps<TData>,
) {
  const {
    getOptions,
    getOptionLabel,
    isOptionEqualToValue,
    inputLabel,
    params,
  } = props;
  const row = params.row;

  const apiRef = useGridApiContext();
  const [open, setOpen] = useState(false);
  const [input, debouncedInput, setInput] = useDebouncedState('');
  const [loading, setLoading] = useState(false);
  const [options, setOptions] = useState<readonly TData[]>([]);

  useEffect(() => {
    if (debouncedInput.length >= 3) {
      setLoading(true);
      const controller = new AbortController();

      (async () => {
        const result = await getOptions(debouncedInput, {
          signal: controller.signal,
        });
        setLoading(false);
        setOptions(result.data);
      })();

      return () => {
        controller.abort();
      };
    } else {
      setLoading(false);
      setOptions([]);
    }
  }, [getOptions, debouncedInput]);

  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  const handleInputChange = useCallback(
    (_event: React.SyntheticEvent, value: string) => setInput(value),
    [setInput],
  );
  const handleOpen = useCallback(() => {
    setOpen(true);
    setInput('');
  }, [setInput]);
  const handleClose = useCallback(() => {
    setOpen(false);
    setOptions([]);
    setLoading(false);
  }, []);
  const getValue = useCallback(() => {
    if (params.value) return params.value;

    return null;
  }, [params.value]);

  const handleChange = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (event: React.SyntheticEvent<Element, Event>, newValue: any) => {
      event.stopPropagation();
      apiRef.current.setEditCellValue({
        id: params.id,
        field: params.field,
        value: {
          ...newValue,
          _row: row,
        },
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [params.id, params.field, row],
  );

  return (
    <Autocomplete
      value={getValue()}
      onChange={handleChange}
      open={open}
      onOpen={handleOpen}
      onClose={handleClose}
      fullWidth
      isOptionEqualToValue={isOptionEqualToValue}
      getOptionLabel={getOptionLabel}
      options={options}
      loading={loading}
      autoHighlight
      filterOptions={(x) => x}
      onInputChange={handleInputChange}
      inputValue={input}
      renderInput={(params) => (
        <TextField
          {...params}
          label={inputLabel}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </Fragment>
            ),
          }}
        />
      )}
    />
  );
}

function SynchronousInputCell(props: SynchronousInputCellProps) {
  const {
    params,
    options,
    isOptionEqualToValue,
    freeSolo,
    getOptionLabel,
    multiple,
    inputLabel,
  } = props;
  const apiRef = useGridApiContext();

  const handleChange = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (event: React.SyntheticEvent<Element, Event>, newValue: any) => {
      event.stopPropagation();
      apiRef.current.setEditCellValue({
        id: params.id,
        field: params.field,
        value: newValue,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [params.id, params.field],
  );

  const getValue = useCallback(() => {
    if (params.value) return params.value;

    if (multiple) return [];

    return null;
  }, [params.value, multiple]);

  return (
    <Autocomplete
      value={getValue()}
      onChange={handleChange}
      onInputChange={(event, value) =>
        freeSolo && !multiple && event && handleChange(event, value)
      }
      fullWidth
      multiple={multiple}
      isOptionEqualToValue={isOptionEqualToValue}
      options={options ?? []}
      freeSolo={freeSolo}
      autoHighlight
      getOptionLabel={getOptionLabel}
      renderInput={(inputParams) => (
        <TextField {...inputParams} label={inputLabel} error={params.error} />
      )}
    />
  );
}

export default function AutocompleteEditInputCell<TData = unknown>(
  props: AutocompleteEditInputCellProps<TData>,
) {
  const { asynchronous, ...rest } = props;

  if (asynchronous) {
    const asyncProps = rest as AsynchronousInputCellProps<TData>;
    return <AsynchronousInputCell {...asyncProps} />;
  }

  const syncProps = rest as SynchronousInputCellProps;
  return <SynchronousInputCell {...syncProps} />;
}

type SynchronousInputCellProps = {
  params: GridRenderEditCellParams;
  options: unknown[] | undefined;
  freeSolo?: boolean;
  inputLabel: string;
  multiple?: boolean;
  isOptionEqualToValue: (option: unknown) => boolean;
  getOptionLabel?: (option: unknown) => string;
};

type AsynchronousInputCellProps<TData = unknown> = {
  params: GridRenderEditCellParams;
  getOptions: (
    input: string,
    extraOptions: { signal: AbortSignal },
  ) => Promise<PaginatedResponse<TData[]>>;
  isOptionEqualToValue: (option: unknown) => boolean;
  inputLabel: string;
  getOptionLabel?: (option: unknown) => string;
};

type AutocompleteEditInputCellProps<TData = unknown> =
  | (SynchronousInputCellProps & { asynchronous: false })
  | (AsynchronousInputCellProps<TData> & { asynchronous: true });
