import React from 'react';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

// Styles in this component are temporary because it is a visual fix and soon everything will be in Styled Components anyway
const SingleSelectDropdownCell: React.FC<{ value: string }> = ({ value }) => (
  <div
    style={{
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
    }}
  >
    <div style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}>{value}</div>
    <ArrowDropDownIcon
      style={{ color: 'var(--dark-gray)', opacity: 0.6, marginRight: '-2px' }}
    />
  </div>
);

export default SingleSelectDropdownCell;
