import format from 'date-fns/format';
import { BranchRolesLabels } from 'utils/roles';
import { BranchTypes, BranchLabelsMap } from 'utils/branches';
import {
  getAutocompleteColumn,
  getAvatarColumn,
  getMultilineColumn,
  getSingleSelectColumn,
} from './render';
import DataGridIntegerInput from '../partials/DataGridIntegerInput';
import MemberService from 'api/service/members';

const formatDate = ({ value }) =>
  value && format(new Date(value), 'dd.MM.yyyy kk:mm');

const dataGridProps = {
  cities: {
    allowSelection: true,
    allowActions: true,
    displayFooter: true,
    serverSideDataHandling: true,
  },
  teams: {
    allowSelection: true,
    allowActions: true,
    displayFooter: true,
    serverSideDataHandling: true,
  },
  board: {
    allowSelection: false,
    allowActions: true,
    displayFooter: false,
    serverSideDataHandling: false,
  },
  citiesList: {
    allowSelection: false,
    allowActions: true,
    displayFooter: true,
    serverSideDataHandling: true,
  },
  applications: {
    allowSelection: true,
    allowActions: true,
    actionsWidth: 80,
    datagridClassname: 'datagrid-applications',
    displayFooter: true,
    serverSideDataHandling: true,
  },
};

// this is for members
const rowIdMapping = {
  cities: 'slug',
  applications: 'applicationId',
  teams: '',
  board: 'id',
  citiesList: 'slug',
};

const formatRow = {
  cities: (row) => {
    const { branch_id, user_id, user, ..._row } = row;
    const { profile_photo_path, ..._user } = user;
    return {
      ..._row,
      ..._user,
    };
  },
  teams: () => {},
};

const getBaseColumns = {
  applications: () => [
    {
      field: 'first_name',
      headerName: 'First name',
      flex: 1,
    },
    {
      field: 'last_name',
      headerName: 'Last name',
      flex: 1,
    },
    {
      field: 'created_at',
      headerName: 'Application date',
      type: 'dateTime',
      flex: 1,
      valueFormatter: formatDate,
    },
  ],
  cities: () => [
    {
      field: 'first_name',
      headerName: 'First name',
      flex: 1,
    },
    {
      field: 'last_name',
      headerName: 'Last name',
      flex: 1,
    },
    { field: 'slug', headerName: 'Identifier', flex: 1 },
    getSingleSelectColumn({
      field: 'branch_role_id',
      headerName: 'Status',
      flex: 1,
      valueOptions: BranchRolesLabels,
      editable: true,
    }),
    {
      field: 'created_at',
      headerName: 'Registration date',
      type: 'dateTime',
      flex: 1,
      valueFormatter: formatDate,
    },
    {
      field: 'updated_at',
      headerName: 'Last updated',
      type: 'dateTime',
      flex: 1,
      valueFormatter: formatDate,
    },
  ],
  teams: () => [],
  board: () => [
    getAvatarColumn({
      imageFieldName: 'user',
      nameFieldName: 'name',
      identifierFieldName: rowIdMapping.board,
      imageSecondaryFieldName: 'profile_photo_path',
      editable: false,
    }),
    getAutocompleteColumn(
      'name',
      {
        headerName: 'Name',
        sortable: false,
        editable: true,
        flex: 3,
        valueGetter: ({ row }) => row.user,
        valueFormatter: ({ value }) =>
          value.first_name || value.last_name
            ? `${value.first_name} ${value.last_name}`
            : '',
      },
      {
        freeSolo: false,
        multiple: false,
        isOptionEqualToValue: (option, value) => option.slug === value.slug,
        getOptionLabel: (value) =>
          value.first_name || value.last_name
            ? `${value.first_name} ${value.last_name}`
            : '',
        asynchronous: true,
        getOptions: getUsersForInput,
        inputLabel: 'Choose an EESTECer',
      },
    ),
    {
      field: 'position',
      headerName: 'Position',
      sortable: false,
      width: 550,
      flex: 3,
      valueGetter: ({ value }) => value.name,
    },
    getMultilineColumn(
      'description',
      {
        flex: 3,
        sortable: false,
        headerName: 'Description',
        valueGetter: ({ row }) => row.position,
        valueFormatter: ({ value }) => value.description,
      },
      {
        valueKey: 'description',
      },
    ),
    // {
    //   field: 'order',
    //   headerName: 'Order',
    //   sortable: false,
    //   editable: true,
    //   type: 'number',
    //   renderEditCell: (params) => (
    //     <DataGridIntegerInput {...params} min={1} max="rowCount" />
    //   ),
    // },
  ],
  citiesList: () => [
    {
      field: 'name',
      headerName: 'Name',
      editable: true,
      flex: 1,
    },
    {
      field: 'slug',
      headerName: 'Identifier',
      flex: 1,
    },
    getSingleSelectColumn({
      field: 'branch_type',
      headerName: 'Type',
      valueGetter: ({ value }) => value.id,
      valueFormatter: ({ value }) => BranchLabelsMap.get(value),
      editable: true,
      flex: 1,
      valueOptions: BranchTypes,
      getOptionValue: (option) => option?.id || option,
      getOptionLabel: (option) => option?.name || option,
      valueSetter: ({ value, row }) => ({
        ...row,
        branch_type: BranchTypes.find(({ id }) => id === value),
      }),
    }),
    {
      field: 'country',
      headerName: 'Country',
      editable: true,
      flex: 1,
    },
    {
      field: 'region',
      headerName: 'Region',
      editable: true,
      type: 'number',
      renderEditCell: (params) => <DataGridIntegerInput {...params} min={1} />,
      valueSetter: ({ value, row }) => ({
        ...row,
        region: value === '' || value == null ? 1 : value,
      }),
    },
  ],
};

const getUsersForInput = (input, { signal } = {}) =>
  MemberService.getMembers(
    { key: 'all' },
    { filters: { items: [], quickFilterValues: input.trim().split(' ') } },
    signal,
  );

export { rowIdMapping, formatRow, getBaseColumns, dataGridProps };
