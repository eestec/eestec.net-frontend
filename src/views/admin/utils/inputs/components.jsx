import PropTypes from 'prop-types';
import ImageFileInput from 'components/ImageFileInput/ImageFileInput';
import TextInput from 'components/TextInput/TextInput';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import Grid from '@mui/material/Grid';
import { capitalizeFirstLetter } from 'utils/misc';

export const AdminTextInput = ({
  multiline,
  label,
  onChange,
  error,
  ...props
}) => (
  <Grid item xs={2} sm={multiline ? 2 : 1}>
    <TextInput
      {...props}
      label={label ?? capitalizeFirstLetter(name)}
      onValueChange={onChange}
      errorMessage={error}
      multiline={multiline}
    />
  </Grid>
);
AdminTextInput.propTypes = {
  multiline: PropTypes.bool,
  label: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export const AdminImageInput = ({
  value,
  fallbackUrl,
  label,
  aspectRatio,
  ...props
}) => {
  const src = value
    ? typeof value === 'string'
      ? value
      : URL.createObjectURL(value)
    : '';

  return (
    <div
      style={{
        textAlign: 'center',
        paddingTop: '1.5rem',
      }}
    >
      <p
        style={{
          marginTop: 0,
          fontSize: '1.2rem',
          fontWeight: 'bold',
          marginBottom: '1rem',
          color: 'white',
          textAlign: 'left',
        }}
      >
        {label}
      </p>
      <img
        src={src || fallbackUrl}
        alt={label}
        style={{
          display: 'block',
          marginBottom: '1rem',
          width: '100%',
          objectFit: 'cover',
          aspectRatio: aspectRatio,
          borderRadius: '0.5rem',
        }}
      />
      <ImageFileInput {...props} variant="button" Icon={CloudUploadIcon} />
    </div>
  );
};
AdminImageInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  fallbackUrl: PropTypes.string,
  label: PropTypes.string,
  aspectRatio: PropTypes.string,
};
