import Grid from '@mui/material/Grid';
import { capitalizeFirstLetter } from 'utils/misc';
import {
  validateWebsiteURL,
  validatePhysicalAddress,
  validateNotEmpty,
  validateEmail,
} from 'utils/validators';

import { AdminImageInput, AdminTextInput } from './components';

import branchPlaceholder from 'assets/img/entity-banner-placeholder.jpg';

// FUTURE: For now there was no need for other non-image inputs than TextInput, This here should be easily adjustable though in case there is a need for more input components
const formKeys = {
  cities: {
    info: [
      { name: 'founded_in', label: 'Founded in', type: 'number' },
      { name: 'website', validators: [validateWebsiteURL] },
      { name: 'facebook', validators: [validateWebsiteURL] },
      { name: 'instagram', validators: [validateWebsiteURL] },
      { name: 'linkedin', validators: [validateWebsiteURL] },
      {
        name: 'address',
        validators: [validatePhysicalAddress],
        multiline: true,
        hint: 'Where is your local EESTEC office? Where can people find you?',
        style: { minHeight: '80px' },
      },
      { name: 'description', multiline: true },
      {
        name: 'profile_picture_path',
        type: 'image',
        aspectRatio: '490/243',
        label: 'Branch picture',
        title: 'Upload new branch picture',
        fallbackUrl: branchPlaceholder,
        checkCopyrights: true,
      },
    ],
    new: [
      { name: 'name', label: 'City name', validators: [validateNotEmpty] },
      { name: 'country', validators: [validateNotEmpty] },
      { name: 'region', type: 'number', validators: [validateNotEmpty] },
      {
        name: 'email',
        type: 'email',
        validators: [validateNotEmpty, validateEmail],
      },
    ],
  },
  board: {
    new: [
      {
        name: 'position',
        label: 'Position name',
        validators: [validateNotEmpty],
      },
    ],
  },
};

const getValidators = (keys) =>
  keys.reduce((acc, cur) => {
    acc[cur.name] = cur.validators;
    return acc;
  }, {});

const getComponent = (type) => {
  if (type === 'image') return AdminImageInput;
  return AdminTextInput;
};

const renderAdminInputs = ({
  onChange,
  onImageChange,
  textInputsColumns = 2,
  keys,
  errors,
  formData,
}) => {
  const [textInputs, imageInputs] = keys.reduce(
    (acc, curProps) => {
      const { name, type, label } = curProps;
      const Component = getComponent(type);
      const accIdx = type === 'image' ? 1 : 0;
      const dynamicProps = {
        label: label ?? capitalizeFirstLetter(name),
        onChange: type === 'image' ? onImageChange(name) : onChange(name),
        error: errors?.[name],
        value: formData[name],
      };

      acc[accIdx].push(
        <Component key={name} {...curProps} {...dynamicProps} />,
      );
      return acc;
    },
    [[], []],
  );

  return (
    <div
      style={{
        display: 'flex',
        columnGap: '4.5rem',
        justifyContent: 'flex-start',
        flexWrap: 'wrap',
      }}
    >
      {textInputs.length > 0 ? (
        <Grid
          container
          columns={textInputsColumns}
          columnSpacing="1.5rem"
          className="form-container-wide"
        >
          {textInputs}
        </Grid>
      ) : null}
      {imageInputs.length > 0 ? (
        <div
          style={{
            maxWidth: 'min(100%, 400px)',
            minWidth: 'min(100%, 300px)',
            flex: '1 1',
          }}
        >
          {imageInputs}
        </div>
      ) : null}
    </div>
  );
};

const FIRST_MANDATE_YEAR = 2023;

const getMandates = () => {
  const today = new Date();
  const currentMonth = today.getUTCMonth();
  const currentYear = today.getUTCFullYear();

  const shouldDisplayElect = currentMonth >= 3;

  const result = [];

  for (
    let i = FIRST_MANDATE_YEAR;
    i <= (shouldDisplayElect ? currentYear : currentYear - 1);
    ++i
  ) {
    const lastTwo = parseInt(`${i}`.slice(2), 10);
    result.push({
      value: `${i}/${lastTwo + 1}`,
      label: `${lastTwo}/${lastTwo + 1}`,
    });
  }

  return {
    mandates: result,
    current: result.at(currentMonth >= 7 ? -1 : -2),
  };
};

export { formKeys, renderAdminInputs, getValidators, getMandates };
