import DataGridAvatar from '../partials/DataGridAvatar';
import DataGridMultilineCell from '../partials/DataGridMultilineCell';
import AutocompleteEditInputCell from './cells/AutocompleteEditInputCell';
import SingleSelectDropdownCell from './cells/SingleSelectDropdownCell';

const _renderMultilineEditCell = (params, componentProps) => (
  <DataGridMultilineCell {...params} {...componentProps} />
);
const getMultilineColumn = (fieldName, props, componentProps) => {
  const renderMultilineEditCell = (params) =>
    _renderMultilineEditCell(params, componentProps);

  return {
    field: fieldName,
    type: 'string',
    editable: true,
    headerName: '',
    ...(props || {}),
    renderEditCell: renderMultilineEditCell,
  };
};

function getAvatarColumn({
  imageFieldName,
  imageSecondaryFieldName,
  nameFieldName,
  identifierFieldName,
  editable,
  onChange,
}) {
  return {
    field: imageFieldName,
    type: 'string',
    editable: false,
    width: 60,
    sortable: false,
    filterable: false,
    headerName: '',
    renderCell: ({ row }) => {
      let value = row[imageFieldName] ?? '';
      if (value && typeof value === 'object' && imageSecondaryFieldName) {
        value = value[imageSecondaryFieldName];
      }
      const src = value
        ? typeof value === 'string'
          ? value
          : URL.createObjectURL(value)
        : '';

      return (
        <DataGridAvatar
          onChange={onChange}
          alt={row[nameFieldName]}
          src={src}
          fieldName={imageFieldName}
          id={row[identifierFieldName]}
          editable={editable}
        />
      );
    },
  };
}

function getSingleSelectColumn({ valueFormatter, ...props }) {
  return {
    ...props,
    valueFormatter,
    type: 'singleSelect',
    renderCell: ({ value }) => (
      <SingleSelectDropdownCell
        value={valueFormatter ? valueFormatter({ value }) : value}
      />
    ),
  };
}

function getAutocompleteColumn(
  fieldName,
  props,
  {
    options,
    freeSolo,
    multiple,
    getOptionLabel,
    isOptionEqualToValue,
    getOptions,
    inputLabel,
    asynchronous,
  },
) {
  return {
    field: fieldName,
    type: 'string',
    editable: true,
    headerName: '',
    ...(props || {}),
    renderEditCell: (params) => (
      <AutocompleteEditInputCell
        params={params}
        options={options}
        freeSolo={freeSolo}
        multiple={multiple}
        isOptionEqualToValue={isOptionEqualToValue}
        getOptionLabel={getOptionLabel}
        getOptions={getOptions}
        inputLabel={inputLabel}
        asynchronous={asynchronous}
      />
    ),
  };
}

export {
  getAvatarColumn,
  getMultilineColumn,
  getAutocompleteColumn,
  getSingleSelectColumn,
};
