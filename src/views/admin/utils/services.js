import BranchService from 'api/service/branches';
import EventService from 'api/service/events';
import MemberService from 'api/service/members';
import BoardService from 'api/service/board';
import { applicationResponseMapper } from 'utils/api';

const entityServices = {
  cities: {
    members: (_type, slug, params, abortSignal) =>
      MemberService.getMembers(
        { key: 'branchMembers', value: slug },
        params,
        abortSignal,
      ),
    info: BranchService.getBranch,
    list: (_type, _slug, params, abortSignal) =>
      BranchService.getBranches(params, abortSignal),
    applications: (_type, slug, params, abortSignal) =>
      MemberService.getMembers(
        { key: 'branchApplications', value: slug },
        params,
        abortSignal,
      ),
  },
  events: {
    list: EventService.getEvents,
    info: EventService.getEvent,
  },
  teams: {},
  board: {
    members: async (_type, _name, params, abortSignal) => {
      const [boardResult, boardPositionsResult] = await Promise.all([
        BoardService.getBoard(params, abortSignal),
        BoardService.getBoardPositions(abortSignal),
      ]);
      if (boardResult.length === boardPositionsResult.length) {
        return {
          data: boardResult,
          meta: {
            total: boardResult.length,
          },
        };
      }

      const result = [...boardResult];
      const orders = new Set(boardResult.map(({ order }) => order));

      boardPositionsResult.forEach((boardPosition, idx) => {
        if (
          boardResult.some(
            (boardMember) => boardMember.position.id === boardPosition.id,
          )
        ) {
          return;
        }

        let order = !orders.has(boardPosition.id)
          ? boardPosition.id
          : undefined;

        if (!order) {
          let i = 1;
          while (orders.has(i)) {
            ++i;
          }
          orders.add(i);
          order = i;
        } else {
          orders.add(order);
        }

        result.push({
          id: 0 - idx - 1,
          position: boardPosition,
          user: {
            first_name: '',
            last_name: '',
            profile_photo_path: '',
            slug: '',
            id: 0 - idx - 1,
          },
          order,
        });
      });

      return {
        data: result.toSorted((a, b) => a.order - b.order),
        meta: {
          total: result.length,
        },
      };
    },
  },
};
const responseFormatters = {
  cities: {
    info: ({ branch }) => branch,
    applications: applicationResponseMapper,
  },
  events: {},
  teams: {},
  board: {},
};

function getEntityServiceSettings(entityType, key) {
  return {
    serviceFn: entityServices[entityType]?.[key],
    formatFn: responseFormatters[entityType]?.[key],
  };
}

export { getEntityServiceSettings };
