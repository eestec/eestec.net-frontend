import { GridCellEditStopReasons } from '@mui/x-data-grid';
import { isKeyboardEvent } from 'utils/misc';

export const allowEntersForMultilineEditCells = (params, event) => {
  if (params.reason !== GridCellEditStopReasons.enterKeyDown) {
    return;
  }
  if (params.colDef?.renderEditCell?.name !== 'renderMultilineEditCell') {
    return;
  }
  if (isKeyboardEvent(event) && !event.ctrlKey && !event.metaKey) {
    event.defaultMuiPrevented = true;
  }
};
