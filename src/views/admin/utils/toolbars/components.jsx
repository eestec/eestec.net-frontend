import { forwardRef } from 'react';
import PropTypes from 'prop-types';
import {
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarExport,
  GridToolbarDensitySelector,
  GridToolbarQuickFilter,
} from '@mui/x-data-grid';
import Portal from '@mui/material/Portal';
import AdminToolbarActions from '../../partials/AdminToolbarActions';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import { admin as adminPropTypes } from 'utils/prop-types';

export function BranchesToolbar({ batchActions, settings }) {
  const { withQuickFilter, quickFilterContainerRef } = settings;
  // The prop-types warning below is false positive
  // eslint-disable-next-line react/prop-types
  const { adminActions, callbacks } = batchActions;

  return (
    <>
      {renderQuickFilterContainerInPortal(
        withQuickFilter,
        quickFilterContainerRef.current,
      )}
      <GridToolbarContainer>
        <Button
          startIcon={<AddIcon />}
          variant="text"
          onClick={() => adminActions.addBranch(callbacks)}
        >
          Add a new branch
        </Button>
        <EditingHint />
      </GridToolbarContainer>
    </>
  );
}

// export function BoardToolbar({ batchActions })
export function BoardToolbar() {
  // The prop-types warning below is false positive
  // eslint-disable-next-line react/prop-types
  // const { adminActions, callbacks } = batchActions;

  return (
    <GridToolbarContainer>
      {/* <Button
        startIcon={<AddIcon />}
        variant="text"
        onClick={() => adminActions.addBoardPosition(callbacks)}
      >
        Add a new position
      </Button> */}
      <EditingHint />
    </GridToolbarContainer>
  );
}

export function MembersToolbar({ settings, batchActions }) {
  const { withQuickFilter, quickFilterContainerRef, withNativeToolbarActions } =
    settings;

  return (
    <>
      {renderQuickFilterContainerInPortal(
        withQuickFilter,
        quickFilterContainerRef.current,
      )}
      <GridToolbarContainer className="admin-data-grid-toolbar-container">
        {renderNativeToolbarActions(withNativeToolbarActions)}
        <AdminToolbarActions {...batchActions} />
      </GridToolbarContainer>
    </>
  );
}

export const QuickFilterContainer = forwardRef(
  function QuickFilterContainer(_props, ref) {
    return <div className="admin-data-grid-quick-filter-container" ref={ref} />;
  },
);

function EditingHint() {
  return (
    <div
      style={{
        marginLeft: 'auto',
        fontSize: '0.65rem',
        color: 'gray',
        alignSelf: 'flex-start',
        padding: '4px',
        cursor: 'default',
      }}
    >
      Double-click on a cell to edit it
    </div>
  );
}

const renderQuickFilterContainerInPortal = (enabled, container) =>
  enabled && container ? (
    <Portal container={container}>
      <GridToolbarQuickFilter className="admin-data-grid-quick-filter" />
    </Portal>
  ) : null;

const renderNativeToolbarActions = (enabled) =>
  enabled ? (
    <div>
      <GridToolbarColumnsButton />
      <GridToolbarDensitySelector />
      <GridToolbarExport />
    </div>
  ) : null;

const toolbarSettingsPropTypes = PropTypes.shape({
  withQuickFilter: PropTypes.bool,
  quickFilterContainerRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]),
  withNativeToolbarActions: PropTypes.bool,
});
MembersToolbar.propTypes = {
  settings: toolbarSettingsPropTypes,
  batchActions: PropTypes.shape({
    ...adminPropTypes.toolbarActions,
    adminActions: PropTypes.object,
  }),
};
BranchesToolbar.propTypes = {
  settings: toolbarSettingsPropTypes,
  batchActions: PropTypes.shape({
    ...adminPropTypes.toolbarActions,
    adminActions: adminPropTypes.adminActions.cities,
  }),
};
BoardToolbar.propTypes = {
  settings: toolbarSettingsPropTypes,
  batchActions: PropTypes.shape({
    ...adminPropTypes.toolbarActions,
    adminActions: adminPropTypes.adminActions.board,
  }),
};
