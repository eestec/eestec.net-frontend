import {
  MembersToolbar,
  BranchesToolbar,
  BoardToolbar,
  QuickFilterContainer,
} from './components';

const toolbarSettings = {
  cities: {
    props: {
      withQuickFilter: true,
      withNativeToolbarActions: true,
    },
    Toolbar: MembersToolbar,
  },
  applications: {
    props: {
      withQuickFilter: true,
      withNativeToolbarActions: true,
    },
    Toolbar: MembersToolbar,
  },
  teams: {},
  board: { Toolbar: BoardToolbar, props: {} },
  citiesList: {
    props: {
      withQuickFilter: true,
    },
    Toolbar: BranchesToolbar,
  },
};

export function getToolbar({ gridType, quickFilterContainerRef }) {
  let quickFilterContainer = null;
  const { Toolbar, props } = toolbarSettings[gridType] || {};
  if (props.withQuickFilter) {
    quickFilterContainer = (
      <QuickFilterContainer ref={quickFilterContainerRef} />
    );
    props.quickFilterContainerRef = quickFilterContainerRef;
  }

  return {
    Toolbar,
    toolbarProps: props,
    quickFilterContainer,
  };
}
