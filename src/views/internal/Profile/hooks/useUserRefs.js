import { useRef } from 'react';

function useUserRefs() {
  /*
    TODO: This is disgusting. A single useRef can serve many references if you use useRef([])
    See here: https://stackoverflow.com/questions/54633690/how-can-i-use-multiple-refs-for-an-array-of-elements-with-hooks
  */
  const events = useRef(null);
  const attendedEvents = useRef(null);
  const organizedEvents = useRef(null);
  const attendedPast = useRef(null);
  const attendedUpcoming = useRef(null);
  const attendedInProgress = useRef(null);
  const attendedApplication = useRef(null);
  const organizedPast = useRef(null);
  const organizedUpcoming = useRef(null);
  const organizedInProgress = useRef(null);
  const organizedApplication = useRef(null);

  return {
    events: {
      all: events,
      organized: {
        all: organizedEvents,
        past: organizedPast,
        upcoming: organizedUpcoming,
        inProgress: organizedInProgress,
        application: organizedApplication,
      },
      attended: {
        all: attendedEvents,
        past: attendedPast,
        upcoming: attendedUpcoming,
        inProgress: attendedInProgress,
        application: attendedApplication,
      },
    },
  };
}

export default useUserRefs;
