import { useRef } from 'react';
import { Link } from 'react-router-dom';

import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import SlidingElement from 'components/SlidingElement/SlidingElement';
import SeeMoreButton from 'components/SeeMoreButton/SeeMoreButton';
import useDocumentTitle from 'hooks/useDocumentTitle';
import Testimonials from 'views/public/AboutUs/components/Testimonials/Testimonials';
import History from 'views/public/AboutUs/components/History/History';
import historyData from 'utils/history';
import scroll from 'utils/scroll';
import mockTestimonials from 'utils/testimonials';

import 'views/public/AboutUs/AboutUs.css';

/**
 __AboutUs__ is a view used to display information in the About Us section
 */
function AboutUs() {
  const aboutUsRef = useRef(null);
  const ourHistoryRef = useRef(null);
  const testimonialsRef = useRef(null);
  const scrollToAboutUsSection = () =>
    scroll(aboutUsRef, null, 0, 1000, 'easeOutBack', 1);

  useDocumentTitle('About us');

  return (
    <>
      <div className="top-tabs-wrapper">
        <div className="about-us-top-tabs">
          <div>
            <Link to="#about-us" onClick={scrollToAboutUsSection}>
              <h1>
                <span className="light">About</span>
                <span>EESTEC</span>
              </h1>
            </Link>
          </div>
          <div>
            <Link to="/join-us">
              <span>Join us</span>
            </Link>
          </div>
        </div>
      </div>
      <div className="section" id="about-us" ref={aboutUsRef}>
        <div>
          <SlidingElement>
            <HeadlinedContent
              title="Who are we?"
              variant="h2"
              fullWidthText
              splitRight
            >
              <p>
                EESTEC, Electrical Engineering STudents&#39; European
                assoCiation, brings together over 5000 Electrical Engineering
                and Computer Science (EECS) students from more than 20 different
                European countries. With 40+ branches across Europe, we are here
                to Power Your Future!
              </p>
            </HeadlinedContent>
          </SlidingElement>
          <SlidingElement slideRight>
            <HeadlinedContent title="EESTEC Values" variant="h2" fullWidthText>
              <p>
                Our values are the core principles that guide our actions and
                behaviours at EESTEC.
              </p>
              <p>
                <b>INSPIRING BELONGING: </b>
                We create an atmosphere where everyone is accepted. We create a
                friendly and safe environment where people feel that they belong
                to a community no matter their differences.
              </p>
              <p>
                <b>INVOKING PASSION: </b>
                We work in a creative atmosphere that enables our working
                processes to be fulfilling and exciting. We appreciate being
                part of a team that is making a difference, and we enjoy every
                moment of it.
              </p>
              <p>
                <b>ENCOURAGING KNOWLEDGE SHARING: </b>
                Sharing is caring. We create a learning environment where there
                is room for mistakes to ensure an easier learning curve and
                inspire the exchange and transfer of knowledge, which ensures
                the development and continuity of our Association.
              </p>
              <p>
                <b>ACTIVATING MOBILITY: </b>
                We provide opportunities for travelling and discovering
                different cultures throughout our network. We strive to make
                travelling a lot easier and more possible for ourselves.
              </p>
              <p>
                <b>PURSUING DEVELOPMENT: </b>
                We are committed to continuous personal, academic and
                professional development, and we embrace getting out of our
                comfort zone.
              </p>
              <p>
                <b>EMPOWERING COLLABORATION: </b>
                We empower and encourage open communication and collaboration
                within our network. That is how great things are done!
              </p>
            </HeadlinedContent>
          </SlidingElement>
          <SlidingElement>
            <HeadlinedContent
              title="Our Mission"
              variant="h2"
              fullWidthText
              splitRight
            >
              <p>
                Connecting EECS students and supporting them in their
                professional, academic, and personal growth by creating an
                international network.
              </p>
            </HeadlinedContent>
          </SlidingElement>
          <SlidingElement slideRight>
            <HeadlinedContent title="Our Vision" variant="h2" fullWidthText>
              <p>
                Empowering EECS students across Europe to reach their full
                potential in their academic, professional, and social lives in a
                strong and recognizable network.
              </p>
            </HeadlinedContent>
          </SlidingElement>
        </div>
        <SeeMoreButton target={ourHistoryRef} label="Our history" />
      </div>
      <div className="section" id="our-history" ref={ourHistoryRef}>
        <History items={historyData} scrollButtonTarget={testimonialsRef} />
      </div>
      <div className="section" id="testimonials" ref={testimonialsRef}>
        <Testimonials items={mockTestimonials} />
      </div>
    </>
  );
}

export default AboutUs;
