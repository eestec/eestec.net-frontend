import { useRef } from 'react';

function useBranchRefs() {
  /*
    TODO: This is disgusting. A single useRef can serve many references if you use useRef([])
    See here: https://stackoverflow.com/questions/54633690/how-can-i-use-multiple-refs-for-an-array-of-elements-with-hooks
  */
  const description = useRef(null);
  const board = useRef(null);
  const events = useRef(null);
  const past = useRef(null);
  const upcoming = useRef(null);
  const inProgress = useRef(null);
  const openForApplication = useRef(null);
  const members = useRef(null);
  const active = useRef(null);
  const alumni = useRef(null);
  const membershipApplications = useRef(null);

  return {
    description,
    board,
    events: {
      all: events,
      past,
      inProgress,
      upcoming,
      openForApplication,
    },
    members: {
      all: members,
      active,
      alumni,
      membershipApplications,
    },
  };
}

export default useBranchRefs;
