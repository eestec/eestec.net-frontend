import { useContext, useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';

import ModalContext from 'context/ModalContext';
import Heading from 'components/Heading/Heading';
import useDocumentTitle from 'hooks/useDocumentTitle';
import { useBoard } from 'hooks/useBoard';
import ToastContext from 'context/ToastContext';
import ContactBoard from './components/ContactBoard/ContactBoard';
import ContactForm from './components/ContactForm/ContactForm';

import './Contact.css';

/**
 __Contact__ is a view used to display the Contact page
 */
function Contact() {
  useDocumentTitle('Contact');
  const { data, isLoading, error } = useBoard();
  const { modalContent } = useContext(ModalContext);
  const { setToastContent } = useContext(ToastContext);

  useEffect(() => {
    if (error) {
      setToastContent(error.data?.message);
    }
  }, [error, setToastContent]);

  return (
    <section className={`contact ${modalContent ? 'blurred' : ''}`}>
      <Heading
        variant="h3"
        lineAfter
        styleVariant="h1"
        className="contact-board-title"
      >
        The Board
      </Heading>
      <div className="contact-board-container">
        {isLoading && (
          <div className="board-loader">
            <CircularProgress size={70} />
          </div>
        )}
        {data?.map(({ position, user }) => (
          <ContactBoard
            key={position.id}
            name={`${user.first_name} ${user.last_name}`}
            desc={position.description}
            occupation={position.name}
            img={user.profile_photo_path}
          />
        ))}
      </div>
      <Heading
        variant="h3"
        lineAfter
        styleVariant="h2"
        className="contact-contact-title"
      >
        CONTACT
      </Heading>
      <ContactForm />
    </section>
  );
}

export default Contact;
