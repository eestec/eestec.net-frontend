import PropTypes from 'prop-types';
import 'views/public/Contact/components/ContactBoard/SingleContactBoard/SingleContactBoard.css';

/**
 __SingleContactBoard__ is a modal element presenting detailed information about a board member
 */
function SingleContactBoard({ modalData }) {
  const { img, desc, name, occupation, alt } = modalData;
  return (
    <div className="single-contact-board">
      <div className="single-board-photo-container">
        <img src={img} alt={alt} className="single-board-photo" />
      </div>
      <div className="single-board-member-info">
        <div className="single-board-header">
          <h3>{name}</h3>
          <p>{occupation}</p>
        </div>
        <div className="single-board-desc">{formatDescription(desc)}</div>
      </div>
    </div>
  );
}

function formatDescription(input) {
  const lines = input.split('\n');
  let elements = [];
  let listItems = [];

  lines.forEach((line, index) => {
    const trimmedLine = line.trim();

    if (trimmedLine.startsWith('- ')) {
      listItems.push(
        <li key={`li-${index}`}>{trimmedLine.substring(2).trim()}</li>,
      );
    } else {
      if (listItems.length > 0) {
        elements.push(<ul key={`ul-${index}`}>{listItems}</ul>);
        listItems = [];
      }
      elements.push(<p key={`p-${index}`}>{trimmedLine}</p>);
    }
  });

  // If there are any remaining list items, push the final <ul>
  if (listItems.length > 0) {
    elements.push(<ul key={`ul-final`}>{listItems}</ul>);
  }

  return elements;
}

SingleContactBoard.propTypes = {
  modalData: PropTypes.shape({
    img: PropTypes.string,
    desc: PropTypes.string,
    name: PropTypes.string,
    occupation: PropTypes.string,
    alt: PropTypes.string,
  }).isRequired,
};
export default SingleContactBoard;
