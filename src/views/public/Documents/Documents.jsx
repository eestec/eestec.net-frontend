import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import useDocumentTitle from 'hooks/useDocumentTitle';

/**
 __Documents__ is the view used to display the Official Documents page
 */
function Documents() {
  useDocumentTitle('Official Documents');

  return (
    <div className="info-page">
      <h1 className="info-title">Official Documents</h1>
      <div className="section">
        <HeadlinedContent title="Statutes" variant="h1" fullWidthText>
          <p>
            The Statutes is the document which defines the most fundamental
            aspects of EESTEC, such as its aim. The seat of the Αssociation is
            located in Zurich (Switzerland).
          </p>
          <p>Current Statutes:</p>
          <p>
            <a href="https://drive.google.com/file/d/1nneLDMcM04bcYeOg1C97cyqogjCC-u3q/view">
              Statutes (December 2024)
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="General Rules of Procedure"
          variant="h1"
          fullWidthText
        >
          <p>
            The General Rules of Procedure (GRoP) are made to guide the bodies
            of EESTEC in their work. They are a supplement to the Statutes and a
            summary of what has shown to be the most appropriate measure, in
            order to achieve the aim of EESTEC.
          </p>
          <p>Current General Rules of Procedure:</p>
          <p>
            <a href="https://drive.google.com/file/d/1NbACECVr-174MYByTypRwVdOMIghIai6/view">
              General Rules of Procedure (December 2024)
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Strategic Plan" variant="h1" fullWidthText>
          <p>
            The Strategic Plan is a formalized road-map that describes where
            EESTEC is going over the next few years and how it is going to get
            there. The Strategic Plan brings focus and direction to our
            organization and sets clear guidelines for our future actions.
          </p>
          <p>
            <a href="https://drive.google.com/file/d/181j61qmbwWLRlMTNIN0ToUdpQTxbNi68/view?usp=drive_link">
              Strategic plan 2023-2026
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Event Regulations" variant="h1" fullWidthText>
          <p>
            The Event Regulations (ER) is a document issued by the Board
            defining internal policies regarding organising EESTEC events.
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1Cu40wOK2DKMnTt4qBodwwKvfTcm8vVhX/view">
              Event Regulations (February 2025)
            </a>
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1OayIlHNMA5rNL_1apApYwnxxeJf1DH3B/view">
              Guide - How to announce your event and choose participants
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Extended Rules of Procedure"
          variant="h1"
          fullWidthText
        >
          <p>
            The Extended Rules of Procedure (ERoP) is a document issued by the
            Board defining internal policies regarding specific topics.
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1Xess-ejXlcIqQ_LTVf8jRHTg-NqvL0gb/view">
              Extended Rules of Procedures (December 2024)
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Swiss Civil Law" variant="h1" fullWidthText>
          <p>
            Our Association is registered in Zurich, therefore, we have to
            follow the Swiss Civil Law which is the foundation of our legal
            documents. Those must be always followed and complied with Swiss
            Civil Code.
          </p>
          <p>
            <a href="https://www.fedlex.admin.ch/eli/cc/24/233_245_233/en">
              Swiss Civil Code
            </a>
          </p>
        </HeadlinedContent>
      </div>
    </div>
  );
}

export default Documents;
