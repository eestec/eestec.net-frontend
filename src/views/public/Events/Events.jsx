import { useContext, useCallback, useState } from 'react';
import EventService from 'api/service/events';
import CardList from 'components/CardList/CardList';
import Heading from 'components/Heading/Heading';
import useEvents from 'hooks/useEvents';
import useDocumentTitle from 'hooks/useDocumentTitle';
import EventCard from 'components/EventCard/EventCard';
import {
  mapFiltersToSearchParams,
  mapSearchParamsToFilters,
} from 'views/public/Events/utils/filters';
import ToastContext from 'context/ToastContext';
import { eventWithFiltersGridSettings } from 'utils/grid';
import logger from 'utils/logger';
import useFilterActions from 'hooks/useFilterActions';
import EventFilters from 'views/public/Events/components/EventFilters/EventFilters';

import 'views/public/Events/Events.css';

/**
 __Events__ is a view used to display the Events page
 */
function Events() {
  useDocumentTitle('Events');
  const { setToastContent } = useContext(ToastContext);
  const [filterString, setFilterString] = useState('');

  const onEventTypesLoadError = useCallback(
    (err) => {
      logger.log(err);
      setToastContent(
        'Something went wrong with gathering the event types. Please reload the page.',
        'error',
      );
    },
    [setToastContent],
  );

  const [filters, setFilters] = useFilterActions({
    onFilterChange: (_filterString, setSearchParamsCallback) => {
      setFilterString(_filterString);
      setSearchParamsCallback?.();
    },
    navigationListener: {
      enable: true,
      retainableProperties: ['name'],
    },
    mapFiltersToSearchParams,
    mapSearchParamsToFilters,
    filterPopulation: {
      serviceFunction: EventService.getEventTypes,
      onError: onEventTypesLoadError,
      type: 'array',
    },
    defaultParamsString: 'sort=-start_date',
  });

  const pastEvents = useEvents({
    kind: 'past',
    filters: filterString,
    enabled: !!filterString,
  });
  const activeEvents = useEvents({
    kind: 'active',
    filters: filterString,
    enabled: !!filterString,
  });

  return (
    <div id="events" className="filter-column-wrapper section">
      <div className="filter-wrapper">
        <EventFilters
          filters={filters}
          setFilters={setFilters}
          disabled={activeEvents.isLoading || pastEvents.isLoading}
        />
      </div>
      <div className="events-wrapper">
        {!!filterString &&
          !pastEvents.isLoading &&
          !activeEvents.isLoading &&
          !activeEvents.combinedData.length &&
          !pastEvents.combinedData.length && (
            <p className="no-cards dimmed">
              There are no events matching your criteria.
            </p>
          )}
        {(activeEvents.isLoading ||
          !!activeEvents.combinedData.applications.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>
              Open for application
            </Heading>
            <CardList
              items={activeEvents.combinedData.applications}
              emptyMessage="There are no events open for application"
              loading={activeEvents.isLoading}
              cardButtonsText="Apply now"
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
            />
          </div>
        )}
        {(activeEvents.isLoading ||
          !!activeEvents.combinedData.upcoming.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>
              Upcoming
            </Heading>
            <CardList
              items={activeEvents.combinedData.upcoming}
              emptyMessage="There are no upcoming events"
              loading={activeEvents.isLoading}
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
            />
          </div>
        )}
        {(activeEvents.isLoading ||
          !!activeEvents.combinedData.inProgress.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>
              In progress
            </Heading>
            <CardList
              items={activeEvents.combinedData.inProgress}
              emptyMessage="There are no events in progress"
              loading={activeEvents.isLoading}
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
            />
          </div>
        )}
        {(pastEvents.isLoading || !!pastEvents.combinedData.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>
              Past events
            </Heading>
            <CardList
              items={pastEvents.combinedData}
              emptyMessage="There are no past events"
              /* only display CardList loading when all lists are being reloaded */
              loading={pastEvents.isLoading}
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
              showMoreButton={{
                visibilityCondition: pastEvents.hasNextPage,
                onClick: pastEvents.fetchNextPage,
                isLoading: pastEvents.isFetchingNextPage,
                text: 'Show more past events',
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default Events;
