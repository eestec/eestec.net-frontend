import Filter from 'components/Filters/Filters';
import PropTypes from 'prop-types';

/**
 __EventFilters__ is the filtering box visible on the Events page. It accepts filters
 and filter setting function, together with information whether filtering should be disabled,
 e.g. during an ongoing BE call which includes filters.

 This data is then formatted and passed to the `Filters` component.
 */
function EventFilters({ filters, setFilters, disabled }) {
  // onTypeClick handles the click event on the checkboxes with event types
  const onTypeClick = (idx) => {
    const newType = [...filters.type];
    newType[idx] = {
      ...newType[idx],
      enabled: !newType[idx].enabled,
    };
    const newState = {
      ...filters,
      type: newType,
    };
    setFilters(newState);
  };

  // onTypeClick handles the click event on the dropdown with sorting methods
  const onDropdownClick = (chosenOption) => {
    const sort = {
      chosenOption,
      options: filters.sort.options,
    };
    setFilters({ ...filters, sort });
  };

  return (
    <Filter
      title="Filter events"
      checkboxFilters={[
        {
          title: 'Type',
          onClick: onTypeClick,
          options: filters?.type || [],
          loading: !filters,
          skeletonLength: 10,
        },
      ]}
      dropdownFilters={[
        {
          title: 'Sort by',
          onClick: onDropdownClick,
          options: filters?.sort?.options || [],
          optionLabel: 'displayName',
          optionKey: 'identifier',
          label: filters?.sort?.chosenOption?.displayName || '',
          loading: !filters,
        },
      ]}
      textFilter={{
        enabled: true,
        initialValue: filters?.name,
        title: 'Search events',
        onChange: (searchTerm) => {
          const newState = { ...filters, name: searchTerm };
          setFilters(newState);
        },
      }}
      disabled={disabled}
    />
  );
}

EventFilters.propTypes = {
  filters: PropTypes.shape({
    type: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
      }),
    ),
    sort: PropTypes.shape({
      chosenOption: PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired,
      }).isRequired,
      options: PropTypes.arrayOf(
        PropTypes.shape({
          displayName: PropTypes.string.isRequired,
          identifier: PropTypes.string.isRequired,
        }),
      ).isRequired,
    }),
    name: PropTypes.string,
  }),
  setFilters: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

EventFilters.defaultProps = {
  filters: null,
};

export default EventFilters;
