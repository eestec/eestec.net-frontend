import { useState } from 'react';
import PropTypes from 'prop-types';

const VIDEO_LINK =
  'https://www.youtube-nocookie.com/embed/8r3V1sAcRFg?autoplay=1&mute=1&loop=1&playlist=8r3V1sAcRFg';

export default function EESTECVideo({ fallback }) {
  const [isLoaded, setIsLoaded] = useState(false);

  return (
    <>
      {isLoaded ? null : fallback}
      <iframe
        className="homepage-video"
        style={!isLoaded ? { visibility: 'hidden' } : undefined}
        onLoad={() => setIsLoaded(true)}
        src={VIDEO_LINK}
        title="What is EESTEC - Video"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    </>
  );
}

EESTECVideo.propTypes = {
  fallback: PropTypes.node,
};
EESTECVideo.defaultProps = {
  fallback: null,
};
