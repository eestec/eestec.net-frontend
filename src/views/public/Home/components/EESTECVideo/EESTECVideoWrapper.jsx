import {
  useState,
  useEffect,
  useRef,
  useCallback,
  lazy,
  Suspense,
} from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@mui/material/CircularProgress';

import './EESTECVideo.css';

const EESTECVideo = lazy(() => import('./EESTECVideo'));

function VideoFallback() {
  return (
    <div className="homepage-video fallback">
      <CircularProgress size={70} />
    </div>
  );
}

function LazyEESTECVideo({ fallback }) {
  return (
    <Suspense fallback={fallback}>
      <EESTECVideo fallback={fallback} />
    </Suspense>
  );
}
LazyEESTECVideo.propTypes = {
  fallback: PropTypes.node,
};
LazyEESTECVideo.defaultProps = {
  fallback: null,
};

/**
 __EESTECVideo__ is a component presenting the promotional video of EESTEC
 */
function EESTECVideoWrapper() {
  const [isVideoReadyToLoad, setIsVideoReadyToLoad] = useState(false);
  const fallback = useRef(<VideoFallback />);
  const setVideoReady = useCallback(
    () => setIsVideoReadyToLoad(true),
    [setIsVideoReadyToLoad],
  );

  useEffect(() => {
    document.addEventListener('scroll', setVideoReady, { passive: true });
    return () => {
      document.removeEventListener('scroll', setVideoReady);
    };
  }, [setVideoReady]);

  useEffect(() => {
    if (isVideoReadyToLoad) {
      document.removeEventListener('scroll', setVideoReady);
    }
  }, [isVideoReadyToLoad, setVideoReady]);

  return (
    <div className="homepage-video-wrapper">
      {isVideoReadyToLoad ? (
        <LazyEESTECVideo fallback={fallback.current} />
      ) : (
        fallback.current
      )}
    </div>
  );
}

export default EESTECVideoWrapper;
