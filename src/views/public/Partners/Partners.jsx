import { useContext } from 'react';
import ModalContext from 'context/ModalContext';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import PartnersSlider from 'components/PartnersSlider/PartnersSlider';
import useDocumentTitle from 'hooks/useDocumentTitle';

import 'views/public/Partners/Partners.css';

/**
 __Partners__ is a view used to display the Partners page of the website
 */
function Partners() {
  const { modalContent } = useContext(ModalContext);
  useDocumentTitle('Partners');

  return (
    <div id="partners" className={modalContent ? 'blurred' : ''}>
      <PartnersSlider />
      <div className="section partners-top-message">
        <p>
          Our members are the
          <b> next generation of professionals</b>
          . With their skills and knowledge, they will commit to achieving the
          vision and goals of the companies they will work for.
          <br />
          Moreover, the members of our Association are open-minded, highly
          motivated students from top European universities.
          <br />
          Would you like to come in contact with our strong and recognizable
          network of passionate, skilled, and highly motivated students?
          <br />
          Contact us and become our Partner!
        </p>
      </div>
      <div className="section become-partner">
        <HeadlinedContent title="Become our Annual Partner" variant="h1">
          <p>
            As our Annual Partner you can establish multifaceted interaction
            with our audience throughout the year, not only via external
            channels (e.g., Facebook and Instagram Page), but also through
            internal ones, such as our internal mailing list. Moreover, this
            package will give you access to exclusive services like the
            opportunity to organise webinars in collaboration with us, and to
            join our Spring and Autumn Congresses, during which you will have a
            chance to present your company directly to the representatives of
            our branches, and answer their questions!
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Organize a Webinar with us!"
          variant="h1"
          splitRight
        >
          <p>
            As our Partner, you will have the chance to discuss practices and
            technologies related to your company’s field, and promote your
            company’s mission and opportunities even further.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Promote your company through our network"
          variant="h1"
        >
          <p>
            If you choose one of our promotional packages, you will have the
            opportunity to take the best out of our social media and internal
            communication channels to promote your brand and your initiatives.
            This will give you the chance of increasing your resonance within a
            specific target audience, composed of students from all over Europe.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Get involved in our Projects"
          variant="h1"
          splitRight
        >
          <p>
            <b>
              <a href="https://ssa.eestec.net" target="_blank" rel="noreferrer">
                Soft Skills Academy
              </a>
            </b>
            <br />
            Soft Skills Academy is a project that strives for the development of
            students in the form of interpersonal skills that are necessary for
            employment and a successful career, but also for the challenges of
            modern life that the traditional way of studying cannot provide.
            Each year it provides hundreds of students in different European
            with the opportunity to participate in soft skill trainings.
            Sessions are organised in cooperation with EESTEC trainers,
            companies and individual professionals, who wish to share their
            knowledge with students and empower them by giving them the chance
            to acquire and improve fundamental skills for their careers.
          </p>
          <p>
            <b>
              <a
                href="https://eestechchallenge.eestec.net"
                target="_blank"
                rel="noreferrer"
              >
                EESTech Challenge
              </a>
            </b>
            <br />
            EESTech Challenge is our annual competition, in which each year
            approximately 300 students take part. These students compete in
            small teams firstly in some of our local committees, a task relevant
            to the annual topic of the competition. Afterwards, the winners of
            these Local Rounds gather together to compete in the Final Round
            organized by one of our branches. Companies involved in the project
            have the chance to get in touch with young, talented engineers and
            to share their expertise as mentors or during a presentation or
            lecture. The topic of the EESTech Challenge is chosen every year to
            fit the current trends in technology, while being always related to
            the EECS field (Electrical Engineering Computer Science).
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Donations" variant="h1">
          <p>
            Our Association&apos;s main purpose is to give our members the tools
            to best shape their future careers in an incredibly dynamic and
            evolving society. With this objective clear in our minds, the&nbsp;
            Electrical Engineering STudents&apos; European assoCiation (EESTEC)
            organises events to improve both their soft and hard skills. Your
            donation can help us to cover the expenses related to these events,
            allow us to fulfill our ambitious goals and improve ourselves in
            every aspect.
          </p>
        </HeadlinedContent>
      </div>
      {/*      <div className="section contact-partners">
        <div className="contact-us">
          <Heading variant="h1">Are you interested? Contact us</Heading>
          <EmailForm onSubmit={handleSubmit} baseComponentClass="partners" />
        </div>
      </div> */}
    </div>
  );
}

export default Partners;
