/*eslint-env node*/
import { defineConfig, loadEnv } from 'vite';
import { plugins, alias } from './.vite';

const config = ({ mode }: { mode: string }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  return defineConfig({
    plugins,
    build: {
      outDir: 'build',
    },
    resolve: {
      alias,
    },
    server: {
      port: 3000,
    },
  });
};

export default config;
